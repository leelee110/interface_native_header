/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef QOS_H
#define QOS_H
/**
 * @addtogroup QoS
 * @{
 *
 * @brief Provides QoS interfaces for setting, canceling, and querying QoS levels.
 *
 * @since 12
 */

/**
 * @file qos.h
 *
 * @brief Declares the QoS interfaces in C.
 *
 * @syscap SystemCapability.Resourceschedule.QoS.Core
 *
 * @since 12
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the QoS levels.
 *
 * @since 12
 */
typedef enum QoS_Level {
    /**
     * @brief QoS level for user invisible tasks.
     */
    QOS_BACKGROUND = 0,

    /**
     * @brief QoS level for background critical tasks.
     */
    QOS_UTILITY,

    /**
     * @brief Default QoS level.
     */
    QOS_DEFAULT,

    /**
     * @brief QoS level for user triggered tasks.
     */
    QOS_USER_INITIATED,

    /**
     * @brief QoS level for time limited tasks.
     */
    QOS_DEADLINE_REQUEST,

    /**
     * @brief QoS level for user interactive tasks.
     */
    QOS_USER_INTERACTIVE,
} QoS_Level;

/**
 * @brief Sets the QoS level for the calling thread.
 *
 * @param level QoS level. For details, see {@link QoS_Level}.
 * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
 * @see QoS_Level
 * @since 12
 */
int OH_QoS_SetThreadQoS(QoS_Level level);

/**
 * @brief Cancels the QoS level of the calling thread.
 *
 * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
 * @see QoS_Level
 * @since 12
 */
int OH_QoS_ResetThreadQoS();

/**
 * @brief Obtains the QoS level of the calling thread.
 *
 * @param level QoS level of the thread, in the format of {@link QoS_Level}.
 * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
 * @see QoS_Level
 * @since 12
 */
int OH_QoS_GetThreadQoS(QoS_Level *level);
#ifdef __cplusplus
}
#endif
/** @} */
#endif // QOS_H
