/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Pasteboard
 * @{
 *
 * @brief Provides APIs for copying and pasting multiple types of data,
 * including plain text, HTML, URI, and pixel map.
 *
 * @since 13
 */

/**
 * @file oh_pasteboard_err_code.h
 *
 * @brief Defines the error codes used in the pasteboard framework.
 * File to include: <database/pasteboard/oh_pasteboard_err_code.h>
 *
 * @library libpasteboard.so
 * @syscap SystemCapability.MiscServices.Pasteboard
 *
 * @since 13
 */


#ifndef OH_PASTEBOARD_ERR_CODE_H
#define OH_PASTEBOARD_ERR_CODE_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the error codes.
 *
 * @since 13
 */
typedef enum PASTEBOARD_ErrCode {
    /**
     * @error Operation successful.
     */
    ERR_OK = 0,
    /**
     * @error Permission verification failed.
     */
    ERR_PERMISSION_ERROR = 201,
    /**
     * @error Invalid parameter.
     */
    ERR_INVALID_PARAMETER = 401,
    /**
     * @error The device capability is not supported.
     */
    ERR_DEVICE_NOT_SUPPORTED = 801,
    /**
     * @error Internal error.
     */
    ERR_INNER_ERROR = 12900000,
    /**
     * @error The system is busy.
     */
    ERR_BUSY = 12900003,
} PASTEBOARD_ErrCode;
#ifdef __cplusplus
};
#endif

/** @} */
#endif
