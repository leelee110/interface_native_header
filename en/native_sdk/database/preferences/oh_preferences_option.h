/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Preferences
 * @{
 *
 * @brief Provides APIs for key-value (KV) data processing, including querying, modifying, and persisting KV data.
 *
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

/**
 * @file oh_preferences_option.h
 *
 * @brief Provides APIs and structs for accessing the <b>PreferencesOption</b> object.
 *
 * File to include: <database/preferences/oh_preferences_option.h>
 * @library libohpreferences.so
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

#ifndef OH_PREFERENCES_OPTION_H
#define OH_PREFERENCES_OPTION_H

#include <cstdint>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a struct for <b>Preferences</b> configuration.
 *
 * @since 13
 */
typedef struct OH_PreferencesOption OH_PreferencesOption;

/**
 * @brief Enumerates the preferences storage types.
 *
 * @since 16
 */
typedef enum Preferences_StorageType {
    /**
     * XML storage, in which the data operations are performed in the memory and data is flushed by
     * calling {@link OH_Preferences_Close}. This type does not support multi-process operations.
     */
    PREFERENCES_STORAGE_XML = 0,
    /**
     * CLKV storage, in which data operations are flushed on a real-time basis.
     * This type supports multi-process operations.
     */
    PREFERENCES_STORAGE_CLKV
} Preferences_StorageType;


/**
 * @brief Creates a pointer to an {@link OH_PreferencesOption} instance.
 * If this pointer is no longer required, use {@link OH_PreferencesOption_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_PreferencesOption} instance created if the operation is successful;
 * returns a null pointer otherwise.
 * @see OH_PreferencesOption
 * @since 13
 */
OH_PreferencesOption *OH_PreferencesOption_Create(void);

/**
 * @brief Sets the file name for an {@link OH_PreferencesOption} instance.
 *
 * @param option Pointer to the target {@link OH_PreferencesOption} instance.
 * @param fileName Pointer to the file name to set.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_PreferencesOption
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesOption_SetFileName(OH_PreferencesOption *option, const char *fileName);

/**
 * @brief Sets the bundle name for an {@link OH_PreferencesOption} instance.
 *
 * @param option Pointer to the target {@link OH_PreferencesOption} instance.
 * @param bundleName Pointer to the bundle name to set.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_PreferencesOption
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesOption_SetBundleName(OH_PreferencesOption *option, const char *bundleName);

/**
 * @brief Sets the application group ID for an {@link OH_PreferencesOption} instance.
 *
 * After the application group ID is set, a <b>Preferences</b> instance will be created in the sandbox directory
 * of the application group ID. \n
 * The application group ID must be obtained from AppGallery Connect. This parameter is not supported currently. \n
 * If the application group ID is an empty string, a <b>Preferences</b> instance will be created in the sandbox
 * directory of the current application.
 *
 * @param option Pointer to the target {@link OH_PreferencesOption} instance.
 * @param dataGroupId Pointer to the application group ID to be set.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_PreferencesOption
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesOption_SetDataGroupId(OH_PreferencesOption *option, const char *dataGroupId);

/**
 * @brief Sets the storage type for a preferences instance object.
 *
 * @param option Pointer to the target configuration to be set with the storage type.
 * @param type Storage type to set.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_PreferencesOption.
 * @since 16
 */
int OH_PreferencesOption_SetStorageType(OH_PreferencesOption *option, Preferences_StorageType type);

/**
 * @brief Destroys an {@link OH_PreferencesOption} instance.
 *
 * @param option Pointer to the {@link OH_PreferencesOption} instance to destroy.
 * @return Returns the status code of the operation.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_PreferencesOption
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesOption_Destroy(OH_PreferencesOption *option);
#ifdef __cplusplus
};
#endif

/** @} */
#endif // OH_PREFERENCES_OPTION_H
