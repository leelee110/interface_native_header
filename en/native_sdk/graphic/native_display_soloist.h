/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_NATIVE_DISPLAY_SOLOIST_H_
#define C_INCLUDE_NATIVE_DISPLAY_SOLOIST_H_

/**
 * @addtogroup NativeDisplaySoloist
 * @{
 *
 * @brief Native service that control the frame rate in threads other than the UI thread.
 *
 * @since 12
 * @version 1.0
 */

/**
 * @file native_display_soloist.h
 *
 * @brief Declares the functions for obtaining and using <b>NativeDisplaySoloist</b>.
 *
 * File to include: "native_display_soloist/native_display_soloist.h"
 * @syscap SystemCapability.Graphic.Graphic2D.HyperGraphicManager
 * @library libnative_display_soloist.so
 * @since 12
 * @version 1.0
 */

#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Provides the declaration of an <b>OH_DisplaySoloist</b> struct.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_DisplaySoloist OH_DisplaySoloist;

/**
 * @brief Defines the pointer to an <b>OH_DisplaySoloist</b> callback function.
 *
 * @param timestamp VSync timestamp.
 * @param targetTimestamp Expected VSync timestamp of the next frame.
 * @param data Pointer to the custom data.
 * @since 12
 * @version 1.0
 */
typedef void (*OH_DisplaySoloist_FrameCallback)(long long timestamp, long long targetTimestamp, void* data);

/**
 * @brief Defines the expected frame rate range.
 *
 * @since 12
 * @version 1.0
 */
typedef struct DisplaySoloist_ExpectedRateRange {
    /** Minimum value of the expected frame rate range. The value range is [0,120]. */
    int32_t min;
    /** Maximum value of the expected frame rate range. The value range is [0,120]. */
    int32_t max;
    /** Expected frame rate. The value range is [0,120]. */
    int32_t expected;
} DisplaySoloist_ExpectedRateRange;

/**
 * @brief Creates an <b>OH_DisplaySoloist</b> instance.
 * A new <b>OH_DisplaySoloist</b> instance is created each time this function is called.
 *
 * @param useExclusiveThread Whether the <b>OH_DisplaySoloist</b> instance exclusively occupies a thread.
 * The value <b>true</b> means that the instance exclusively occupies a thread,
 * and <b>false</b> means that the instance shares a thread with others.
 * @return Returns the pointer to the {@link OH_DisplaySoloist} instance created if the operation is successful;
 * returns a null pointer otherwise. The failure cause may be out of memory.
 * @since 12
 * @version 1.0
 */
OH_DisplaySoloist* OH_DisplaySoloist_Create(bool useExclusiveThread);

/**
 * @brief Destroys an <b>OH_DisplaySoloist</b> object and reclaims the memory occupied.
 *
 * @param displaySoloist Pointer to an {@link OH_DisplaySoloist} instance.
 * @return Returns <b>0</b> if the operation is successful; returns <b>-1</b> otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_DisplaySoloist_Destroy(OH_DisplaySoloist* displaySoloist);

/**
 * @brief Sets a callback function for each frame. The callback function is triggered each time a VSync signal arrives.
 *
 * @param displaySoloist Pointer to an {@link OH_DisplaySoloist} instance.
 * @param callback Callback function to be triggered when the next VSync signal arrives.
 * @param data Pointer to the custom data. The type is void*.
 * @return Returns <b>0</b> if the operation is successful; returns <b>-1</b> otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_DisplaySoloist_Start(
    OH_DisplaySoloist* displaySoloist, OH_DisplaySoloist_FrameCallback callback, void* data);

/**
 * @brief Stops requesting the next VSync signal and triggering the callback function.
 *
 * @param displaySoloist Pointer to an {@link OH_DisplaySoloist} instance.
 * @return Returns <b>0</b> if the operation is successful; returns <b>-1</b> otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_DisplaySoloist_Stop(OH_DisplaySoloist* displaySoloist);

/**
 * @brief Sets the expected frame rate range.
 *
 * @param displaySoloist Pointer to an {@link OH_DisplaySoloist} instance.
 * @param range Pointer to a {@link DisplaySoloist_ExpectedRateRange} instance.
 * @return Returns <b>0</b> if the operation is successful; returns <b>-1</b> otherwise.
 * @since 12
 * @version 1.0
 */
int32_t OH_DisplaySoloist_SetExpectedFrameRateRange(
    OH_DisplaySoloist* displaySoloist, DisplaySoloist_ExpectedRateRange* range);

#ifdef __cplusplus
}
#endif

#endif
/** @} */
