/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_MASK_FILTER_H
#define C_INCLUDE_DRAWING_MASK_FILTER_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_mask_filter.h
 *
 * @brief Declares the functions related to the mask filter in the drawing module.
 *
 * File to include: native_drawing/drawing_mask_filter.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the blur types.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_BlurType {
    /**
     * Blurs both inside and outside the original border.
     */
    NORMAL,
    /**
     * Draws solid inside the border, and blurs outside.
     */
    SOLID,
    /**
     * Draws nothing inside the border, and blurs outside.
     */
    OUTER,
    /**
     * Blurs inside the border, and draws nothing outside.
     */
    INNER,
} OH_Drawing_BlurType;

/**
 * @brief Creates an <b>OH_Drawing_MaskFilter</b> object with a given blur type.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param blurType Blur type.
 * @param sigma Standard deviation of the Gaussian blur to apply. The value must be greater than <b>0</b>.
 * @param respectCTM Whether the blur's sigma is modified by the CTM. The default value is <b>true</b>.
 * @return Returns the pointer to the <b>OH_Drawing_MaskFilter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_MaskFilter* OH_Drawing_MaskFilterCreateBlur(OH_Drawing_BlurType blurType, float sigma, bool respectCTM);

/**
 * @brief Destroys an <b>OH_Drawing_MaskFilter</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_MaskFilter Pointer to an <b>OH_Drawing_MaskFilter</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_MaskFilterDestroy(OH_Drawing_MaskFilter*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
