/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_SAMPLING_OPTIONS_H
#define C_INCLUDE_DRAWING_SAMPLING_OPTIONS_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_sampling_options.h
 *
 * @brief Declares the functions related to the sampling options in the drawing module.
 * It is used for image or texture sampling.
 *
 * File to include: native_drawing/drawing_sampling_options.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the filter modes.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FilterMode {
    /** Nearest filter mode. */
    FILTER_MODE_NEAREST,
    /** Linear filter mode. */
    FILTER_MODE_LINEAR,
} OH_Drawing_FilterMode;

/**
 * @brief Enumerates the mipmap modes.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_MipmapMode {
    /** Mipmap level ignored. */
    MIPMAP_MODE_NONE,
    /** Nearest sampling from two adjacent mipmap levels. */
    MIPMAP_MODE_NEAREST,
    /** Linear interpolation sampling between two adjacent mipmap levels. */
    MIPMAP_MODE_LINEAR,
} OH_Drawing_MipmapMode;

/**
 * @brief Creates an <b>OH_Drawing_SamplingOptions</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_MipmapMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FilterMode Filter mode.
 * For details about the available options, see {@link OH_Drawing_FilterMode} object.
 * @param OH_Drawing_MipmapMode Mipmap mode.
 * For details about the available options, see {@link OH_Drawing_MipmapMode}.
 * @return Returns the pointer to the {@link OH_Drawing_SamplingOptions} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_SamplingOptions* OH_Drawing_SamplingOptionsCreate(OH_Drawing_FilterMode, OH_Drawing_MipmapMode);

/**
 * @brief Destroys an <b>OH_Drawing_SamplingOptions</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SamplingOptionsDestroy(OH_Drawing_SamplingOptions*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
