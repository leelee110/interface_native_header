/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_EFFECT_TYPES_H
#define C_INCLUDE_EFFECT_TYPES_H

/**
 * @addtogroup effectKit
 * @{
 *
 * @brief Provides the basic image processing capabilities, including brightness adjustment, blurring,
 * and grayscale adjustment.
 *
 * @since 12
 * @version 1.0
 */

/**
 * @file effect_types.h
 *
 * @brief Declares the data types of the image effect filter.
 *
 * File to include: <native_effect/effect_types.h>
 * @library libnative_effect.so
 * @syscap SystemCapability.Multimedia.Image.Core
 * @since 12
 */

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Describes a filter, which is used to generate a filter bitmap.
 *
 * @since 12
 */
typedef struct OH_Filter {
    /** Container for storing the pointer to the filter object. */
    std::vector<sk_sp<SkImageFilter> > skFilters_;
}

/**
 * @brief Describes a bitmap.
 *
 * @since 12
 */
typedef struct OH_PixelmapNative {
    /** Smart pointer to the <b>pixelMap</b> object. */
    std::shared_ptr<OHOS::Media::PixelMap> pixelMap;
};

/**
 * @brief Describes a matrix used to create an effect filter.
 *
 * @since 12
 */
struct OH_Filter_ColorMatrix {
    /** Custom color matrix. The value is a 5 x 4 array. */
    float val[20];
};

/**
 * @brief Enumerates the status codes that may be used by the effect filter.
 *
 * @since 12
 */
typedef enum EffectErrorCode {
    /** Success. */
    EFFECT_SUCCESS = 0,
    /** Invalid parameter. */
    EFFECT_BAD_PARAMETER = 401,
    /** Unsupported operation. */
    EFFECT_UNSUPPORTED_OPERATION = 7600201,
    /** Unknown error. */
    EFFECT_UNKNOWN_ERROR = 7600901,
} EffectErrorCode;

/**
 * @brief Enumerates the tile modes of the shader effect.
 *
 * @since 14
 */
typedef enum {
    /** Replicates the edge color if the shader effect draws outside of its original boundary. */
    CLAMP = 0,
    /** Repeats the shader effect in both horizontal and vertical directions. */
    REPEAT,
    /** Repeats the shader effect in both horizontal and vertical directions, alternating mirror images. */
    MIRROR,
    /** Renders the shader effect only within the original boundary. */
    DECAL,
} EffectTileMode;

#ifdef __cplusplus
}
#endif
/** @} */
#endif
