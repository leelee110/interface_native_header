/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAVSession
 * @{
 *
 * @brief Defines the C APIs of the playback control module.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 *
 * @since 13
 * @version 1.0
 */

/**
 * @file native_avsession.h
 *
 * @brief Declares the AVSession definition, which can be used to set metadata, playback state, and other information.
 *
 * @library libohavsession.so
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @kit AVSessionKit
 * @since 13
 * @version 1.0
 */

#ifndef NATIVE_AVSESSION_H
#define NATIVE_AVSESSION_H

#include <stdint.h>
#include "native_avsession_errors.h"
#include "native_avmetadata.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the session types.
 *
 * @since 13
 * @version 1.0
 */
typedef enum {
    /**
     * @brief Audio.
     */
    SESSION_TYPE_AUDIO = 0,

    /**
     * @brief Video.
     */
    SESSION_TYPE_VIDEO = 1,

    /**
     * @brief Audio call.
     */
    SESSION_TYPE_VOICE_CALL = 2,

    /**
     * @brief Video call.
     */
    SESSION_TYPE_VIDEO_CALL = 3
} AVSession_Type;

/**
 * @brief Enumerates the media playback states.
 *
 * @since 13
 * @version 1.0
 */
typedef enum {
    /**
     * @brief Initial.
     */
    PLAYBACK_STATE_INITIAL = 0,

    /**
     * @brief Ready.
     */
    PLAYBACK_STATE_PREPARING = 1,

    /**
     * @brief Playing.
     */
    PLAYBACK_STATE_PLAYING = 2,

    /**
     * @brief Paused.
     */
    PLAYBACK_STATE_PAUSED = 3,

    /**
     * @brief Fast-forwarding.
     */
    PLAYBACK_STATE_FAST_FORWARDING = 4,

    /**
     * @brief Rewinded.
     */
    PLAYBACK_STATE_REWINDED = 5,

    /**
     * @brief Stopped.
     */
    PLAYBACK_STATE_STOPPED = 6,

    /**
     * @brief Playback complete.
     */
    PLAYBACK_STATE_COMPLETED = 7,

    /**
     * @brief Released.
     */
    PLAYBACK_STATE_RELEASED = 8,

    /**
     * @brief Error.
     */
    PLAYBACK_STATE_ERROR = 9,

    /**
     * @brief Idle.
     */
    PLAYBACK_STATE_IDLE = 10,

    /**
     * @brief Buffering.
     */
    PLAYBACK_STATE_BUFFERING = 11,

    /**
     * @brief Maximum value. (Error code 401 is returned in this case.)
     */
    PLAYBACK_STATE_MAX = 12,
} AVSession_PlaybackState;

/**
 * @brief Describes the information related to the playback position.
 *
 * @since 13
 */
typedef struct AVSession_PlaybackPosition {
    /**
     * @brief Elapsed time, in ms.
     */
    int64_t elapsedTime;

    /**
     * @brief Updated time, in ms.
     */
    int64_t updateTime;
} AVSession_PlaybackPosition;

/**
 * @brief Enumerates the loop modes of media playback.
 *
 * @since 13
 */
typedef enum {
    /**
     * @brief Sequential playback.
     */
    LOOP_MODE_SEQUENCE = 0,

    /**
     * @brief Single loop.
     */
    LOOP_MODE_SINGLE = 1,

    /**
     * @brief Playlist loop.
     */
    LOOP_MODE_LIST = 2,

    /**
     * @brief Shuffle.
     */
    LOOP_MODE_SHUFFLE = 3,

    /**
     * @brief Custom playback.
     */
    LOOP_MODE_CUSTOM = 4,
} AVSession_LoopMode;

/**
 * @brief Enumerates the playback control commands.
 *
 * @since 13
 * @version 1.0
 */
typedef enum AVSession_ControlCommand {
    /**
     * @brief Invalid control command.
     */
    CONTROL_CMD_INVALID = -1,

    /**
     * @brief Play command.
     */
    CONTROL_CMD_PLAY = 0,

    /**
     * @brief Pause command.
     */
    CONTROL_CMD_PAUSE = 1,

    /**
     * @brief Stop command.
     */
    CONTROL_CMD_STOP = 2,

    /**
     * @brief Command for playing the next media asset.
     */
    CONTROL_CMD_PLAY_NEXT = 3,

    /**
     * @brief Command for playing the previous media asset.
     */
    CONTROL_CMD_PLAY_PREVIOUS = 4,
} AVSession_ControlCommand;

/**
 * @brief Enumerates the callback execution results.
 *
 * @since 13
 */
typedef enum {
    /**
     * @brief Execution successful.
     */
    AVSESSION_CALLBACK_RESULT_SUCCESS = 0,

    /**
     * @brief Execution failed.
     */
    AVSESSION_CALLBACK_RESULT_FAILURE = -1,
} AVSessionCallback_Result;

/**
 * @brief Describes the callback for a common playback control command.
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnCommand)(OH_AVSession* session,
    AVSession_ControlCommand command, void* userData);

/**
 * @brief Describes the callback for the fast-forward operation.
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnFastForward)(OH_AVSession* session,
    uint32_t seekTime, void* userData);

/**
 * @brief Describes the callback for the rewind operation.
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnRewind)(OH_AVSession* session,
    uint32_t seekTime, void* userData);

/**
 * @brief Describes the callback for the seek operation.
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnSeek)(OH_AVSession* session,
    uint64_t seekTime, void* userData);

/**
 * @brief Describes the callback for setting the loop mode.
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnSetLoopMode)(OH_AVSession* session,
    AVSession_LoopMode curLoopMode, void* userData);

/**
 * @brief Describes the callback for the operation of favoriting a media asset.
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnToggleFavorite)(OH_AVSession* session,
    const char* assetId, void* userData);

/**
 * @brief Describes a playback control session object.
 *
 * You can use <b>OH_AVSession_Create</b> to create such an object.
 *
 * @since 13
 * @version 1.0
 */
typedef struct OH_AVSession OH_AVSession;

/**
 * @brief Creates a session object.
 *
 * @param sessionType Session type. For details about the available options, see {@link AVSession_Type}.
 * @param sessionTag Pointer to the session tag.
 * @param bundleName Pointer to the bundle name.
 * @param abilityName Pointer to the ability name.
 * @param avsession Double pointer to the session object created.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal or the session object already
*                                                  exists.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>sessionType</b> is invalid.
 *                                                 2. <b>sessionTag</b> is a null pointer.
 *                                                 3. <b>bundleName</b> is a null pointer.
 *                                                 4. <b>abilityName</b> is a null pointer.
 *                                                 5. <b>avsession</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Create(AVSession_Type sessionType, const char* sessionTag,
    const char* bundleName, const char* abilityName, OH_AVSession** avsession);

/**
 * @brief Destroys a session object.
 *
 * @param avsession Pointer to a session object.
 * @return Returns either of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if <b>avsession</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Destroy(OH_AVSession* avsession);

/**
 * @brief Activates a session.
 *
 * @param avsession Pointer to a session object.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if <b>avsession</b> is a null pointer.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Activate(OH_AVSession* avsession);

/**
 * @brief Deactivates a session.
 *
 * @param avsession Pointer to a session object.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if <b>avsession</b> is a null pointer.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Deactivate(OH_AVSession* avsession);

/**
 * @brief Obtains the session type.
 *
 * @param avsession Pointer to a session object.
 * @param sessionType Pointer to the session type obtained.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal or an error occurs.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>sessionType</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_GetSessionType(OH_AVSession* avsession, AVSession_Type* sessionType);

/**
 * @brief Obtains the session ID.
 *
 * @param avsession Pointer to a session object.
 * @param sessionId Double pointer to the session ID obtained.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal or an error occurs.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>sessionId</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_GetSessionId(OH_AVSession* avsession, const char** sessionId);

/**
 * @brief Sets media metadata.
 *
 * @param avsession Pointer to a session object.
 * @param avmetadata Pointer to the media metadata.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>avmetadata</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetAVMetadata(OH_AVSession* avsession, OH_AVMetadata* avmetadata);

/**
 * @brief Sets the playback state.
 *
 * @param avsession Pointer to a session object.
 * @param playbackState Playback state.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                  2. <b>playbackState</b> is invalid.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetPlaybackState(OH_AVSession* avsession,
    AVSession_PlaybackState playbackState);

/**
 * @brief Sets the playback position.
 *
 * @param avsession Pointer to a session object.
 * @param playbackPosition Pointer to the playback position.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>playbackPosition</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetPlaybackPosition(OH_AVSession* avsession,
    AVSession_PlaybackPosition* playbackPosition);

/**
 * @brief Favorites or unfavorites the media asset.
 *
 * @param avsession Pointer to a session object.
 * @param favorite Whether to favorite or unfavorite the media asset.
 * The value <b>true</b> means to favorite the media asset, and <b>false</b> means the opposite.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if <b>avsession</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetFavorite(OH_AVSession* avsession, bool favorite);

/**
 * @brief Sets a loop mode.
 *
 * @param avsession Pointer to a session object.
 * @param loopMode Loop mode.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                   1. <b>avsession</b> is a null pointer.
 *                                                   2. <b>loopMode</b> is invalid.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetLoopMode(OH_AVSession* avsession, AVSession_LoopMode loopMode);

/**
 * @brief Registers a callback for a common playback control command.
 *
 * @param avsession Pointer to a session object.
 * @param command Playback control command.
 * @param callback Callback to register, which is {@link OH_AVSessionCallback_OnCommand}.
 * @param userData Pointer to the application data passed through the callback.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_CODE_COMMAND_INVALID} if the playback control command is invalid.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterCommandCallback(OH_AVSession* avsession,
    AVSession_ControlCommand command, OH_AVSessionCallback_OnCommand callback, void* userData);

/**
 * @brief Unregisters the callback for a common playback control command.
 *
 * @param avsession Pointer to a session object.
 * @param command Playback control command.
 * @param callback Callback to unregister, which is {@link OH_AVSessionCallback_OnCommand}.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_CODE_COMMAND_INVALID} if the playback control command is invalid.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterCommandCallback(OH_AVSession* avsession,
    AVSession_ControlCommand command, OH_AVSessionCallback_OnCommand callback);

/**
 * @brief Registers a callback for the fast-forward operation.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to register, which is {@link OH_AVSessionCallback_OnFastForward}.
 * @param userData Pointer to the application data passed through the callback.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterForwardCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnFastForward callback, void* userData);

/**
 * @brief Unregisters the callback for the fast-forward operation.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to unregister, which is {@link OH_AVSessionCallback_OnFastForward}.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterForwardCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnFastForward callback);

/**
 * @brief Registers a callback for the rewind operation.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to register, which is {@link OH_AVSessionCallback_OnRewind}.
 * @param userData Pointer to the application data passed through the callback.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterRewindCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnRewind callback, void* userData);

/**
 * @brief Unregisters the callback for the rewind operation.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to unregister, which is {@link OH_AVSessionCallback_OnRewind}.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterRewindCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnRewind callback);

/**
 * @brief Registers a callback for the seek operation.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to register, which is {@link OH_AVSessionCallback_OnSeek}.
 * @param userData Pointer to the application data passed through the callback.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterSeekCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSeek callback, void* userData);

/**
 * @brief Unregisters the callback for the seek operation.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to unregister, which is {@link OH_AVSessionCallback_OnSeek}.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterSeekCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSeek callback);

/**
 * @brief Registers a callback for the operation of setting the loop mode.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to register, which is {@link OH_AVSessionCallback_OnSetLoopMode}.
 * @param userData Pointer to the application data passed through the callback.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterSetLoopModeCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSetLoopMode callback, void* userData);

/**
 * @brief Unregisters the callback for the operation of setting the loop mode.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to unregister, which is {@link OH_AVSessionCallback_OnSetLoopMode}.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterSetLoopModeCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSetLoopMode callback);

/**
 * @brief Registers a callback for the operation of favoriting a media asset.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to register, which is {@link OH_AVSessionCallback_OnToggleFavorite}.
 * @param userData Pointer to the application data passed through the callback.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterToggleFavoriteCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnToggleFavorite callback, void* userData);

/**
 * @brief Unregisters the callback for the operation of favoriting a media asset.
 *
 * @param avsession Pointer to a session object.
 * @param callback Callback to unregister, which is {@link OH_AVSessionCallback_OnToggleFavorite}.
 * @return Returns any of the following result codes:
 *         {@link AV_SESSION_ERR_SUCCESS} if the function is successfully executed.
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} if the session service is abnormal.
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} if
 *                                                 1. <b>avsession</b> is a null pointer.
 *                                                 2. <b>callback</b> is a null pointer.
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterToggleFavoriteCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnToggleFavorite callback);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVSESSION_H
/** @} */
