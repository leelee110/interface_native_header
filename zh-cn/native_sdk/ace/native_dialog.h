/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的UI能力，如UI组件创建销毁、树节点操作，属性设置，事件监听等。
 *
 * @since 12
 */

/**
 * @file native_dialog.h
 *
 * @brief 提供ArkUI在Native侧的自定义弹窗接口定义集合。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef ARKUI_NATIVE_DIALOG_H
#define ARKUI_NATIVE_DIALOG_H

#include "native_type.h"
#include "native_node.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief 弹窗关闭的触发方式。
*
* @since 12
*/
typedef enum {
    /** 系统定义的返回操作、键盘ESC触发。*/
    DIALOG_DISMISS_BACK_PRESS = 0,
    /** 点击遮障层触发。*/
    DIALOG_DISMISS_TOUCH_OUTSIDE,
    /** 点击关闭按钮。*/
    DIALOG_DISMISS_CLOSE_BUTTON,
    /** 下拉关闭。*/
    DIALOG_DISMISS_SLIDE_DOWN,
} ArkUI_DismissReason;

/**
* @brief 设置弹窗显示层级。
*
* @since 15
*/
typedef enum {
    /** 显示在应用最上层。 */
    ARKUI_LEVEL_MODE_OVERLAY = 0,
    /** 嵌入式显示在应用的页面内。 */
    ARKUI_LEVEL_MODE_EMBEDDED,
} ArkUI_LevelMode;

/**
* @brief 指定嵌入式弹窗的蒙层覆盖区域。
*
* @since 15
*/
typedef enum {
    /** 弹窗蒙层按照显示页面给定的布局约束显示。 */
    ARKUI_IMMERSIVE_MODE_DEFAULT = 0,
    /** 弹窗蒙层可扩展至覆盖状态栏和导航条。 */
    ARKUI_IMMERSIVE_MODE_EXTEND,
} ArkUI_ImmersiveMode;

/**
* @brief 弹窗关闭的回调函数。
*
* @since 12
*/
typedef bool (*ArkUI_OnWillDismissEvent)(int32_t reason);

/**
 * @brief 定义弹窗关闭事件对象。
 *
 * @since 12
 */
typedef struct ArkUI_DialogDismissEvent ArkUI_DialogDismissEvent;

/**
 * @brief 定义自定义弹窗的内容对象。
 *
 * @since 18
 */
typedef struct ArkUI_CustomDialogOptions ArkUI_CustomDialogOptions;

/**
 * @brief ArkUI提供的Native侧自定义弹窗接口集合。
 *
 * @version 1
 * @since 12
 */
typedef struct {
    /**
    * @brief 创建自定义弹窗并返回指向自定义弹窗的指针。
    *
    * @note create方法需要在调用show方法之前调用。
    * @return 返回指向自定义弹窗的指针，如创建失败，返回空指针。
    */
    ArkUI_NativeDialogHandle (*create)();
    /**
    * @brief 销毁自定义弹窗。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    */
    void (*dispose)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 挂载自定义弹窗内容。
    *
    * @note setContent方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param content 弹窗内容根节点指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setContent)(ArkUI_NativeDialogHandle handle, ArkUI_NodeHandle content);
    /**
    * @brief 卸载自定义弹窗内容。
    *
    * @note removeContent方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*removeContent)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 为自定义弹窗设置对齐方式。
    *
    * @note setContentAlignment方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param alignment 对齐方式，参数类型{@Link ArkUI_Alignment}。
    * @param offsetX 弹窗的水平偏移量，浮点型。
    * @param offsetY 弹窗的垂直偏移量，浮点型。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setContentAlignment)(ArkUI_NativeDialogHandle handle, int32_t alignment, float offsetX, float offsetY);
    /**
    * @brief 重置setContentAlignment方法设置的属性，使用系统默认的对齐方式。
    *
    * @note resetContentAlignment方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*resetContentAlignment)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 设置自定义弹窗是否开启模态样式的弹窗。
    *
    * @note setModalMode方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param isModal 设置是否开启模态窗口，模态窗口有蒙层，非模态窗口无蒙层，为true时开启模态窗口。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setModalMode)(ArkUI_NativeDialogHandle handle, bool isModal);
    /**
    * @brief 设置自定义弹窗是否允许点击遮罩层退出。
    *
    * @note setAutoCancel方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param autoCancel 设置是否允许点击遮罩层退出，true表示关闭弹窗，false表示不关闭弹窗。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setAutoCancel)(ArkUI_NativeDialogHandle handle, bool autoCancel);
    /**
    * @brief 设置自定义弹窗遮罩属性。
    *
    * @note setMask方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param maskColor 设置遮罩颜色，0xargb格式。
    * @param maskRect 遮蔽层区域范围的指针，遮蔽层区域内的事件不透传，在遮蔽层区域外的事件透传。参数类型{@link ArkUI_Rect}。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setMask)(ArkUI_NativeDialogHandle handle, uint32_t maskColor, const ArkUI_Rect* maskRect);
    /**
    * @brief 设置弹窗背景色。
    *
    * @note setBackgroundColor方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param backgroundColor 设置弹窗背景颜色，0xargb格式。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setBackgroundColor)(ArkUI_NativeDialogHandle handle, uint32_t backgroundColor);
    /**
    * @brief 设置弹窗背板圆角半径。
    *
    * @note setCornerRadius方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param topLeft 设置弹窗背板左上角圆角半径。
    * @param topRight 设置弹窗背板右上角圆角半径。
    * @param bottomLeft 设置弹窗背板左下圆角半径。
    * @param bottomRight 设置弹窗背板右下角圆角半径。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setCornerRadius)(ArkUI_NativeDialogHandle handle, float topLeft, float topRight,
        float bottomLeft, float bottomRight);
    /**
    * @brief 弹窗宽度占栅格宽度的个数。
    *
    * @note setGridColumnCount方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param gridCount 默认为按照窗口大小自适应，最大栅格数为系统最大栅格数。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setGridColumnCount)(ArkUI_NativeDialogHandle handle, int32_t gridCount);
    /**
    * @brief 弹窗容器样式是否自定义。
    *
    * @note enableCustomStyle方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param enableCustomStyle true:宽度自适应子节点，圆角为0，弹窗背景色透明；false:高度自适应子节点，宽度由栅格系统定义, 圆角半径24vp。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*enableCustomStyle)(ArkUI_NativeDialogHandle handle, bool enableCustomStyle);
    /**
    * @brief 弹窗容器是否使用自定义弹窗动画。
    *
    * @note enableCustomAnimation方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param enableCustomAnimation true:使用自定义动画，关闭系统默认动画；false:使用系统默认动画。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*enableCustomAnimation)(ArkUI_NativeDialogHandle handle, bool enableCustomAnimation);
    /**
    * @brief 当触发系统定义的返回操作、键盘ESC关闭交互操作时，如果注册该回调函数，则不会立刻关闭弹窗，是否关闭由用户自行决定。
    *
    * @note registerOnWillDismiss方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param eventHandler 弹窗关闭的回调函数 参数类型{@Link OnWillDismissEvent}。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*registerOnWillDismiss)(ArkUI_NativeDialogHandle handle, ArkUI_OnWillDismissEvent eventHandler);
    /**
    * @brief 显示自定义弹窗。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    * @param showInSubWindow 是否在子窗口显示弹窗。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*show)(ArkUI_NativeDialogHandle handle, bool showInSubWindow);
    /**
    * @brief 关闭自定义弹窗，如已关闭，则不生效。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*close)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 注册系统关闭自定义弹窗的监听事件。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    * @param userData 用户自定义数据指针。
    * @param callback 监听自定义弹窗关闭的回调事件。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*registerOnWillDismissWithUserData)(
        ArkUI_NativeDialogHandle handle, void* userData, void (*callback)(ArkUI_DialogDismissEvent* event));
} ArkUI_NativeDialogAPI_1;

/**
 * @brief ArkUI提供的Native侧自定义弹窗接口集合。
 *
 * @version 2
 * @since 15
 */
typedef struct {
    /**
     * @brief ArkUI提供的Native侧自定义弹窗接口集合，范围是{@link ArkUI_NativeDialogAPI_1}。
     *
     * @since 15
     */
    ArkUI_NativeDialogAPI_1 nativeDialogAPI1;
    /**
     * @brief 指定自定义弹窗避让系统键盘的距离。
     *
     * @note setKeyboardAvoidDistance方法需要在调用show方法之前调用。
     * @param handle 指向自定义弹窗控制器的指针。
     * @param distance 避让键盘的距离，默认为vp。
     * @param unit 避让距离的单位，参数类型{@link ArkUI_LengthMetricUnit}。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 15
     */
    int32_t (*setKeyboardAvoidDistance)(ArkUI_NativeDialogHandle handle, float distance, ArkUI_LengthMetricUnit unit);

    /**
     * @brief 设置弹窗的显示层级。
     *
     * @note 本方法需要在调用show方法之前调用。
     * @param handle 指向自定义弹窗控制器的指针。
     * @param levelMode 显示层级的枚举值， 类型为{@link ArkUI_LevelMode}。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 15
     */
    int32_t (*setLevelMode)(ArkUI_NativeDialogHandle handle, ArkUI_LevelMode levelMode);

    /**
     * @brief 设置弹窗显示层级页面下的节点id。
     *
     * @note 本方法需要在调用setLevelMode方法之前调用。
     * @param handle 指向自定义弹窗控制器的指针。
     * @param uniqueId 指定节点id，会查找该节点所在页面，并将弹窗显示在该页面下。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 15
     */
    int32_t (*setLevelUniqueId)(ArkUI_NativeDialogHandle handle, int32_t uniqueId);

    /**
     * @brief 设置嵌入式弹窗蒙层的显示区域。
     *
     * @note 本方法需要在调用show方法之前调用。
     * @param handle 指向自定义弹窗控制器的指针。
     * @param immersiveMode 显示区域类型的枚举值， 类型为{@link ArkUI_ImmersiveMode}。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 15
     */
    int32_t (*setImmersiveMode)(ArkUI_NativeDialogHandle handle, ArkUI_ImmersiveMode immersiveMode);
} ArkUI_NativeDialogAPI_2;

/**
 * @brief ArkUI提供的Native侧自定义弹窗接口集合。
 *
 * @version 3
 * @since 16
 */
typedef struct {
    /**
     * @brief ArkUI提供的Native侧自定义弹窗接口集合，范围是{@link ArkUI_NativeDialogAPI_1}。
     *
     * @since 16
     */
    ArkUI_NativeDialogAPI_1 nativeDialogAPI1;
    /**
     * @brief ArkUI提供的Native侧自定义弹窗接口集合，范围是{@link ArkUI_NativeDialogAPI_2}。
     *
     * @since 16
     */
    ArkUI_NativeDialogAPI_2 nativeDialogAPI2;
    /**
     * @brief 设置弹窗的边框宽度。
     *
     * @note setBorderWidth方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param top 上边框的宽度。
     * @param right 右边框的宽度。
     * @param bottom 下边框的宽度。
     * @param  left 左边框的宽度。
     * @param unit 指定宽度单位，默认为vp。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setBorderWidth)(
        ArkUI_NativeDialogHandle handle, float top, float right, float bottom, float left, ArkUI_LengthMetricUnit unit);
    /**
     * @brief 设置弹窗的边框颜色。
     *
     * @note setBorderColor方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param top 上边框的颜色。
     * @param right 右边框的颜色。
     * @param bottom 下边框的颜色。
     * @param  left 左边框的颜色。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setBorderColor)(
        ArkUI_NativeDialogHandle handle, uint32_t top, uint32_t right, uint32_t bottom, uint32_t left);
    /**
     * @brief 设置弹窗的边框样式。
     *
     * @note setBorderStyle方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param top 上边框的样式。
     * @param right 右边框的样式。
     * @param bottom 下边框的样式。
     * @param  left 左边框的样式。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setBorderStyle)(
        ArkUI_NativeDialogHandle handle, int32_t top, int32_t right, int32_t bottom, int32_t left);
    /**
     * @brief 设置弹窗的背板宽度。
     *
     * @note setWidth方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param width 背板宽度。
     * @param unit 指定高度的单位，默认为vp。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setWidth)(ArkUI_NativeDialogHandle handle, float width, ArkUI_LengthMetricUnit unit);
    /**
     * @brief 设置弹窗的背板高度。
     *
     * @note setHeight方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param height 背板高度。
     * @param unit 指定高度的单位，默认为vp。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setHeight)(ArkUI_NativeDialogHandle handle, float height, ArkUI_LengthMetricUnit unit);
    /**
     * @brief 设置弹窗的背板阴影。
     *
     * @note setShadow方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param shadow 背板阴影样式，枚举值。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setShadow)(ArkUI_NativeDialogHandle handle, ArkUI_ShadowStyle shadow);
    /**
     * @brief 设置弹窗的背板阴影。
     *
     * @note setCustomShadow方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param customShadow 自定义阴影参数，格式与NODE_SHADOW属性一致。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setCustomShadow)(ArkUI_NativeDialogHandle handle, const ArkUI_AttributeItem* customShadow);
    /**
     * @brief 设置弹窗的背板模糊材质。
     *
     * @note setBackgroundBlurStyle方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param blurStyle 背板模糊材质，枚举值。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setBackgroundBlurStyle)(ArkUI_NativeDialogHandle handle, ArkUI_BlurStyle blurStyle);
    /**
     * @brief 设置弹窗避让键盘模式。
     *
     * @note setKeyboardAvoidMode方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param keyboardAvoidMode 避让键盘模式，枚举值。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setKeyboardAvoidMode)(ArkUI_NativeDialogHandle handle, ArkUI_KeyboardAvoidMode keyboardAvoidMode);
    /**
     * @brief 设置弹窗是否相应悬停态。
     *
     * @note enableHoverMode方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param enableHoverMode 是否相应悬停态，默认false。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*enableHoverMode)(ArkUI_NativeDialogHandle handle, bool enableHoverMode);
    /**
     * @brief 设置悬停态下弹窗默认展示区域。
     *
     * @note setHoverModeArea方法需要在调用show方法之前调用。
     * @param handle 弹窗控制器的指针。
     * @param hoverModeAreaType 悬停态区域，枚举值。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 18
     */
    int32_t (*setHoverModeArea)(ArkUI_NativeDialogHandle handle, ArkUI_HoverModeAreaType hoverModeAreaType);
} ArkUI_NativeDialogAPI_3;

/**
 * @brief 设置是否需要屏蔽系统关闭弹窗行为，true表示屏蔽系统行为不关闭弹窗，false表示不屏蔽。
 *
 * @param event 弹窗关闭事件对象指针。
 * @param shouldBlockDismiss 实现需要屏蔽系统关闭弹窗行为。
 * @since 12
 */
void OH_ArkUI_DialogDismissEvent_SetShouldBlockDismiss(ArkUI_DialogDismissEvent* event, bool shouldBlockDismiss);

/**
 * @brief 获取弹窗关闭事件对象中的用户自定义数据指针。
 *
 * @param event 弹窗关闭事件对象指针。
 *
 * @return 用户自定义数据指针。
 * @since 12
 */
void* OH_ArkUI_DialogDismissEvent_GetUserData(ArkUI_DialogDismissEvent* event);

/**
 * @brief 获取交互式关闭事件指针中的关闭原因。
 *
 * @param event 弹窗关闭事件对象指针。
 *
 * @return 关闭原因，异常情况返回-1。
 *         {@link DIALOG_DISMISS_BACK_PRESS} 对应点击三键back、左滑/右滑、键盘ESC关闭。
 *         {@link DIALOG_DISMISS_TOUCH_OUTSIDE} 点击遮障层时。
 *         {@link DIALOG_DISMISS_CLOSE_BUTTON} 点击关闭按钮。
 *         {@link DIALOG_DISMISS_SLIDE_DOWN} 下拉关闭。
 * @since 12
 */
int32_t OH_ArkUI_DialogDismissEvent_GetDismissReason(ArkUI_DialogDismissEvent* event);

/**
 * @brief 弹出自定义弹窗。
 *
 * @param options 弹窗参数。
 * @param callback 开启弹窗的回调，返回入参是弹窗ID。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_OpenDialog(ArkUI_CustomDialogOptions* options, void (*callback)(int32_t dialogId));

/**
 * @brief 更新自定义弹窗。
 *
 * @param options 弹窗参数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_UpdateDialog(ArkUI_CustomDialogOptions* options);

/**
 * @brief 关闭自定义弹窗。
 *
 * @param dialogId 需要关闭的弹窗ID。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_CloseDialog(int32_t dialogId);

/**
 * @brief 创建自定义弹窗options。
 *
 * @param content 自定义弹窗的内容。
 * @return 自定义弹窗options的指针。
 * @since 18
 */
ArkUI_CustomDialogOptions* OH_ArkUI_CustomDialog_CreateOptions(ArkUI_NodeHandle content);

/**
 * @brief 销毁自定义弹窗options.
 *
 * @param options 自定义弹窗options的指针.
 * @since 18
 */
void OH_ArkUI_CustomDialog_DisposeOptions(ArkUI_CustomDialogOptions* options);

/**
 * @brief 设置弹窗的显示层级。
 *
 * @note 本方法需要在调用 OH_ArkUI_CustomDialog_OpenDialog 方法之前调用。
 * @param options 指向自定义弹窗options的指针。
 * @param levelMode 显示层级的枚举值， 类型为{@link ArkUI_LevelMode}。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 16
 */
int32_t OH_ArkUI_CustomDialog_SetLevelMode(ArkUI_CustomDialogOptions* options, ArkUI_LevelMode levelMode);
 
/**
 * @brief 设置弹窗显示层级页面下的节点id。
 *
 * @note 本方法需要在调用 OH_ArkUI_CustomDialog_OpenDialog 方法之前调用。
 * @param options 指向自定义弹窗options的指针。
 * @param uniqueId 指定节点id，会查找该节点所在页面，并将弹窗显示在该页面下。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 16
 */
int32_t OH_ArkUI_CustomDialog_SetLevelUniqueId(ArkUI_CustomDialogOptions* options, int32_t uniqueId);
 
/**
 * @brief 设置嵌入式弹窗蒙层的显示区域。
 *
 * @note 本方法需要在调用 OH_ArkUI_CustomDialog_OpenDialog 方法之前调用。
 * @param options 指向自定义弹窗options的指针。
 * @param immersiveMode 显示区域类型的枚举值， 类型为{@link ArkUI_ImmersiveMode}。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 15
 */
int32_t OH_ArkUI_CustomDialog_SetImmersiveMode(ArkUI_CustomDialogOptions* options, ArkUI_ImmersiveMode immersiveMode);

/**
 * @brief 设置弹窗的背景颜色。
 *
 * @param options 弹窗参数。
 * @param backgroundColor 弹窗背景颜色。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetBackgroundColor(ArkUI_CustomDialogOptions* options, uint32_t backgroundColor);

/**
 * @brief 设置弹窗的圆角半径。
 *
 * @param options 弹窗参数。
 * @param topLeft 弹窗左上角的圆角半径。
 * @param topRight 弹窗右上角的圆角半径。
 * @param bottomLeft 弹窗左下角的圆角半径。
 * @param bottomRight 弹窗右下角的圆角半径。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetCornerRadius(
    ArkUI_CustomDialogOptions* options, float topLeft, float topRight, float bottomLeft, float bottomRight);

/**
 * @brief 设置弹窗的边框宽度。
 *
 * @param options 弹窗参数。
 * @param top 弹窗上边框的宽度。
 * @param right 弹窗右边框的宽度。
 * @param bottom 弹窗下边框的宽度。
 * @param left 弹窗左边框的宽度。
 * @param unit 指定宽度的单位，默认为vp。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetBorderWidth(
    ArkUI_CustomDialogOptions* options, float top, float right, float bottom, float left, ArkUI_LengthMetricUnit unit);

/**
 * @brief 设置弹窗的边框颜色。
 *
 * @param options 弹窗参数。
 * @param top 弹窗上边框的颜色。
 * @param right 弹窗右边框的颜色。
 * @param bottom 弹窗下边框的颜色。
 * @param left 弹窗左边框的颜色。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetBorderColor(
    ArkUI_CustomDialogOptions* options, uint32_t top, uint32_t right, uint32_t bottom, uint32_t left);

/**
 * @brief 设置弹窗的边框样式。
 *
 * @param options 弹窗参数。
 * @param top 弹窗上边框的样式。
 * @param right 弹窗右边框的样式。
 * @param bottom 弹窗下边框的样式。
 * @param left 弹窗左边框的样式。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetBorderStyle(
    ArkUI_CustomDialogOptions* options, int32_t top, int32_t right, int32_t bottom, int32_t left);

/**
 * @brief 设置弹窗的背板宽度。
 *
 * @param options 弹窗参数。
 * @param width 弹窗的背板宽度。
 * @param unit 指定宽度的单位，默认为vp。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetWidth(ArkUI_CustomDialogOptions* options, float width, ArkUI_LengthMetricUnit unit);

/**
 * @brief 设置弹窗的背板高度。
 *
 * @param options 弹窗参数。
 * @param width 弹窗的背板高度。
 * @param unit 指定高度的单位，默认为vp。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetHeight(ArkUI_CustomDialogOptions* options, float height, ArkUI_LengthMetricUnit unit);

/**
 * @brief 设置弹窗的背板阴影。
 *
 * @param options 弹窗参数。
 * @param shadow 弹窗的背板阴影样式，枚举值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetShadow(ArkUI_CustomDialogOptions* options, ArkUI_ShadowStyle shadow);

/**
 * @brief 设置弹窗的背板阴影。
 *
 * @param options 弹窗参数。
 * @param customShadow 弹窗的自定义阴影参数，格式与NODE_SHADOW属性一致。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetCustomShadow(
    ArkUI_CustomDialogOptions* options, const ArkUI_AttributeItem* customShadow);

/**
 * @brief 设置弹窗的背板模糊材质。
 *
 * @param options 弹窗参数。
 * @param blurStyle 弹窗的背板模糊材质，枚举值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetBackgroundBlurStyle(ArkUI_CustomDialogOptions* options, ArkUI_BlurStyle blurStyle);

/**
 * @brief 设置弹窗的对齐模式。
 *
 * @param options 弹窗参数。
 * @param alignment 弹窗的对齐模式，参数类型{@link ArkUI_Alignment}。
 * @param offsetX 弹窗的水平偏移量，浮点型。
 * @param offsetY 弹窗的垂直偏移量，浮点型。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetAlignment(
    ArkUI_CustomDialogOptions* options, int32_t alignment, float offsetX, float offsetY);

/**
 * @brief 设置自定义弹窗是否开启模态样式的弹窗。
 *
 * @param options 弹窗参数。
 * @param isModal 设置是否开启模态窗口。模态窗口有蒙层,非模态窗口无蒙层。设置为true表示开启模态窗口。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetModalMode(ArkUI_CustomDialogOptions* options, bool isModal);

/**
 * @brief 设置自定义弹窗是否允许点击遮罩层退出。
 *
 * @param options 弹窗参数。
 * @param autoCancel 设置是否允许点击遮罩层退出，true表示关闭弹窗，false表示不关闭弹窗。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetAutoCancel(ArkUI_CustomDialogOptions* options, bool autoCancel);

/**
 * @brief 设置弹窗是否在子窗口显示此弹窗。
 *
 * @param options 弹窗参数。
 * @param isShowInSubwindow 设置弹窗需要显示在主窗口之外时，是否在子窗口显示此弹窗。默认false，弹窗显示在应用内，而非独立子窗口。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetSubwindowMode(ArkUI_CustomDialogOptions* options, bool isShowInSubwindow);

/**
 * @brief 设置自定义弹窗遮罩属性。
 *
 * @param options 弹窗参数。
 * @param maskColor 弹窗的遮罩颜色，0xargb格式。
 * @param maskRect 遮蔽层区域范围的指针，遮蔽层区域内的事件不透传，在遮蔽层区域外的事件透传。参数类型{@link ArkUI_Rect}。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetMask(
    ArkUI_CustomDialogOptions* options, uint32_t maskColor, const ArkUI_Rect* maskRect);

/**
 * @brief 设置弹窗避让键盘的模式。
 *
 * @param options 弹窗参数。
 * @param keyboardAvoidMode 键盘避让模式，枚举值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetKeyboardAvoidMode(
    ArkUI_CustomDialogOptions* options, ArkUI_KeyboardAvoidMode keyboardAvoidMode);

/**
 * @brief 设置弹窗是否响应悬停态。
 *
 * @param options 弹窗参数。
 * @param enabled 是否响应悬停态，默认false。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetHoverModeEnabled(ArkUI_CustomDialogOptions* options, bool enabled);

/**
 * @brief 设置悬停态下弹窗默认展示区域。
 *
 * @param options 弹窗参数。
 * @param hoverModeAreaType 悬停态区域，枚举值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_SetHoverModeArea(
    ArkUI_CustomDialogOptions* options, ArkUI_HoverModeAreaType hoverModeAreaType);

/**
 * @brief 注册系统关闭自定义弹窗的监听事件。
 *
 * @param options 弹窗参数。
 * @param userData 用户自定义数据指针。
 * @param callback 监听自定义弹窗关闭的回调事件。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_RegisterOnWillDismissCallback(
    ArkUI_CustomDialogOptions* options, void* userData, void (*callback)(ArkUI_DialogDismissEvent* event));

/**
 * @brief 注册自定义弹窗显示动效前的监听事件。
 *
 * @param options 弹窗参数。
 * @param userData 用户自定义数据指针。
 * @param callback 弹窗显示动效前的事件回调。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_RegisterOnWillAppearCallback(
    ArkUI_CustomDialogOptions* options, void* userData, void (*callback)(void* userData));

/**
 * @brief 注册自定义弹窗弹出时的监听事件。
 *
 * @param options 弹窗参数。
 * @param userData 用户自定义数据指针。
 * @param callback 弹窗弹出时的事件回调。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_RegisterOnDidAppearCallback(
    ArkUI_CustomDialogOptions* options, void* userData, void (*callback)(void* userData));

/**
 * @brief 注册自定义弹窗退出动效前的监听事件。
 *
 * @param options 弹窗参数。
 * @param userData 用户自定义数据指针。
 * @param callback 弹窗退出动效前的事件回调。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_RegisterOnWillDisappearCallback(
    ArkUI_CustomDialogOptions* options, void* userData, void (*callback)(void* userData));

/**
 * @brief 注册自定义弹窗消失时的监听事件。
 *
 * @param options 弹窗参数。
 * @param userData 用户自定义数据指针。
 * @param callback 弹窗消失时的事件回调。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_CustomDialog_RegisterOnDidDisappearCallback(
    ArkUI_CustomDialogOptions* options, void* userData, void (*callback)(void* userData));

#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_DIALOG_H
/** @} */