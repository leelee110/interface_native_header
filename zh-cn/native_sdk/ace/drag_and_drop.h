/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的通用拖拽及主动发起拖拽能力。
 *
 * @since 12
 */

/**
 * @file drag_and_drop.h
 *
 * @brief 提供NativeDrag相关接口定义。
 *
 * @library libace_ndk.z.so
 * @kit ArkUI
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef ARKUI_NATIVE_DRAG_AND_DROP_H
#define ARKUI_NATIVE_DRAG_AND_DROP_H

#include <stdint.h>

#include "native_type.h"
#include "database/udmf/udmf.h"
#include "multimedia/image_framework/image/pixelmap_native.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 拖拽结果定义，由数据接收方设置，并由系统传递给数据拖出方，拖出方可感知接收方对数据的处理结果。
 *
 * @since 12
 */
typedef enum {
    /** 拖拽处理成功。 */
    ARKUI_DRAG_RESULT_SUCCESSFUL = 0,
    /** 拖拽处理失败。 */
    ARKUI_DRAG_RESULT_FAILED,
    /** 拖拽处理取消。 */
    ARKUI_DRAG_RESULT_CANCELED,
} ArkUI_DragResult;

/**
 * @brief 定义拖拽释放时的数据处理方式，可影响角标的显示。
 *
 * @since 12
 */
typedef enum {
    /** 复制行为。 */
    ARKUI_DROP_OPERATION_COPY = 0,
    /** 剪切行为。 */
    ARKUI_DROP_OPERATION_MOVE,
} ArkUI_DropOperation;

/**
 * @brief 定义拖拽发起前的长按交互阶段的变化状态。
 *
 * @since 12
 */
typedef enum {
    /** Unknown。 */
    ARKUI_PRE_DRAG_STATUS_UNKNOWN = -1,
    /** 拖拽手势启动阶段。 */
    ARKUI_PRE_DRAG_STATUS_ACTION_DETECTING,
    /** 拖拽准备完成，可发起拖拽阶段。 */
    ARKUI_PRE_DRAG_STATUS_READY_TO_TRIGGER_DRAG,
    /** 拖拽浮起动效发起阶段。*/
    ARKUI_PRE_DRAG_STATUS_PREVIEW_LIFT_STARTED,
    /** 拖拽浮起动效结束阶段。*/
    ARKUI_PRE_DRAG_STATUS_PREVIEW_LIFT_FINISHED,
    /** 拖拽落回动效发起阶段。*/
    ARKUI_PRE_DRAG_STATUS_PREVIEW_LANDING_STARTED,
    /** 拖拽落回动效结束阶段。*/
    ARKUI_PRE_DRAG_STATUS_PREVIEW_LANDING_FINISHED,
    /** 拖拽浮起落位动效中断。*/
    ARKUI_PRE_DRAG_STATUS_CANCELED_BEFORE_DRAG,
} ArkUI_PreDragStatus;

/**
 * @brief 拖拽预览缩放模式。
 *
 * @since 12
 */
typedef enum {
    /** 系统根据拖拽场景自动改变跟手点位置，根据规则自动对拖拽背板图进行缩放变换等。 */
    ARKUI_DRAG_PREVIEW_SCALE_AUTO = 0,
    /** 禁用系统对拖拽背板图的缩放行为。 */
    ARKUI_DRAG_PREVIEW_SCALE_DISABLED,
} ArkUI_DragPreviewScaleMode;

/**
 * @brief 拖拽状态。
 *
 * @since 12
 */
typedef enum {
    /** Unknown。*/
    ARKUI_DRAG_STATUS_UNKNOWN = -1,
    /** Started。 */
    ARKUI_DRAG_STATUS_STARTED,
    /** Ended。 */
    ARKUI_DRAG_STATUS_ENDED,
} ArkUI_DragStatus;

/**
 * @brief 组件事件的通用结构类型。
 *
 * @since 12
 */
typedef struct ArkUI_NodeEvent ArkUI_NodeEvent;

/**
 * @brief native UI的上下文实例对象。
 *
 * @since 12
 */
typedef struct ArkUI_Context ArkUI_Context;

/**
 * @brief native UI的上下文实例对象指针定义。
 *
 * @since 12
 */
typedef struct ArkUI_Context* ArkUI_ContextHandle;

/**
 * @brief 拖拽事件。
 *
 * @since 12
 */
typedef struct ArkUI_DragEvent ArkUI_DragEvent;

/**
 * @brief 设置拖拽跟手图的相关自定义参数。
 *
 * @since 12
 */
typedef struct ArkUI_DragPreviewOption ArkUI_DragPreviewOption;

/**
 * @brief 拖拽行为，用于主动发起拖拽。
 *
 * @since 12
 */
typedef struct ArkUI_DragAction ArkUI_DragAction;

/**
 * @brief 主动发起拖拽后，通过拖拽状态监听返回的系统拖拽相关数据。
 *
 * @since 12
 */
typedef struct ArkUI_DragAndDropInfo ArkUI_DragAndDropInfo;

/**
 * @brief 从 NodeEvent 中获取DragEvent。
 *
 * @param node ArkUI_NodeEvent事件指针。
 * @return ArkUI_DragEvent 事件指针，当传入的 NodeEvent 无效或不是拖拽相关的事件时，则返回空。
 * @since 12
 */
ArkUI_DragEvent* OH_ArkUI_NodeEvent_GetDragEvent(ArkUI_NodeEvent* nodeEvent);

/**
 * @brief 获取预览拖拽事件状态。
 *
 * @param node ArkUI_NodeEvent节点对象。
 * @return ArkUI_PreDragStatus 拖拽发起前交互状态。
 * @since 12
 */
ArkUI_PreDragStatus OH_ArkUI_NodeEvent_GetPreDragStatus(ArkUI_NodeEvent* nodeEvent);

/**
 * @brief 设置是否禁用松手时的系统默认动效，默认不禁用，通常在应用需要自定义落位动效时配置。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param disable 是否禁用松手时的系统默认动效，true禁用，false不禁用。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_DisableDefaultDropAnimation(ArkUI_DragEvent* event, bool disable);

/**
 * @brief 设置数据处理方式。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param dropOperation 角标显示状态的类型。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_SetSuggestedDropOperation(ArkUI_DragEvent* event, ArkUI_DropOperation dropOperation);

/**
 * @brief 设置拖拽事件的结果。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param result 拖拽数据处理结果。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_SetDragResult(ArkUI_DragEvent* event, ArkUI_DragResult result);

/**
 * @brief 向ArkUI_DragEvent中设置拖拽数据。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param data 拖拽数据。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_SetData(ArkUI_DragEvent* event, OH_UdmfData* data);

/**
 * @brief 从ArkUI_DragEvent中获取拖拽默认相关数据。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param data OH_UdmfData 拖拽的数据指针，应用在接收时需通过 {@link OH_UdmfData_Create} 方法创建一个用于接收数据的指针。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_GetUdmfData(ArkUI_DragEvent* event, OH_UdmfData *data);

/**
 * @brief 从ArkUI_DragEvent中获取所拖拽的数据类型种类个数。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param count 出参，返回所拖拽数据的类型的数量。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_GetDataTypeCount(ArkUI_DragEvent* event, int32_t* count);

/**
 * @brief 从ArkUI_DragEvent中获取拖拽数据的类型列表。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param eventTypeArray 返回拖拽数据的类型列表，需要先自行创建字符串数组。
 * @param length 数组总长度，不应少于使用{@link OH_ArkUI_DragEvent_GetDataTypesCount}获取到的数量。
 * @param maxStrLen 拖拽数据类型的最大字符串长度。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 *         {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} 缓冲区大小不足。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_GetDataTypes(
    ArkUI_DragEvent *event, char *eventTypeArray[], int32_t length, int32_t maxStrLen);

/**
 * @brief 从ArkUI_DragEvent中获取拖拽结果。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param result 出参，返回拖拽事件对应的拖拽结果。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_GetDragResult(ArkUI_DragEvent* event, ArkUI_DragResult* result);

/**
 * @brief 从ArkUI_DragEvent中获取拖拽结果。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param operation 数据的处理方式.
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 *                可能原因: 1. 参数为空或event非有效的DragEvent.
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_GetDropOperation(ArkUI_DragEvent* event, ArkUI_DropOperation* operation);

/**
 * @brief 从ArkUI_DragEvent中获取预览图跟手点的x轴坐标。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回拖拽跟手点的x轴坐标，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetPreviewTouchPointX(ArkUI_DragEvent* event);

/**
 * @brief 从ArkUI_DragEvent中获取预览图跟手点的y轴坐标。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回拖拽跟手点的y轴坐标，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetPreviewTouchPointY(ArkUI_DragEvent* event);

/**
 * @brief 从ArkUI_DragEvent中获取预览图的宽。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回拖拽跟手图宽度，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetPreviewRectWidth(ArkUI_DragEvent* event);

/**
 * @brief 从ArkUI_DragEvent中获取预览图的高。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回拖拽跟手图高度，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetPreviewRectHeight(ArkUI_DragEvent* event);

/**
 * @brief 从ArkUI_DragEvent中获取跟手点相对于window的x轴坐标。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回跟手点相对于window的x轴坐标，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetTouchPointXToWindow(ArkUI_DragEvent* event);

/**
 * @brief 从ArkUI_DragEvent中获取跟手点相对于window的y轴坐标。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回跟手点相对于window的y轴坐标，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetTouchPointYToWindow(ArkUI_DragEvent* event);

/**
 * @brief 从ArkUI_DragEvent中获取跟手点相对于当前Display的x轴坐标。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回拖拽跟手点相对于当前Display的x轴坐标，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetTouchPointXToDisplay(ArkUI_DragEvent* event);

/**
 * @brief 从ArkUI_DragEvent中获取跟手点相对于当前Display的y轴坐标。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回拖拽跟手点相对于当前Display的y轴坐标，单位为PX，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetTouchPointYToDisplay(ArkUI_DragEvent* event);

/**
 * @brief 获取当前拖拽的x轴方向拖动速度。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回当前拖拽的x轴方向移动速度，单位为PX/s，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetVelocityX(ArkUI_DragEvent* event);

/**
 * @brief 获取当前拖拽的y轴方向拖动速度。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回当前拖拽的y轴方向移动速度，单位为PX/s，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetVelocityY(ArkUI_DragEvent* event);

/**
 * @brief 获取当前拖拽的主方向拖动速度。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @return float 返回当前拖拽移动速度，单位为PX/s，传入参数无效时返回默认值 0。
 * @since 12
 */
float OH_ArkUI_DragEvent_GetVelocity(ArkUI_DragEvent* event);

/**
 * @brief 获取功能键按压状态。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param keys 返回当前处于按下状态的 modifier key组合，应用可通过位运算进行判断。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragEvent_GetModifierKeyStates(ArkUI_DragEvent* event, uint64_t* keys);

/**
 * @brief Request to start the data sync process with the sync option.
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param options OH_UdmfGetDataParams参数指针。
 * @param key 返回数据设置成功之后的key值，字符串长度不小于 {@link UDMF_KEY_BUFFER_LEN}。
 * @param keyLen 表示key字符串的长度。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 *         {@link ARKUI_ERROR_CODE_DRAG_DATA_SYNC_FAILED} 数据同步失败。
 * @since 15
 */
int32_t OH_ArkUI_DragEvent_StartDataLoading(
    ArkUI_DragEvent* event, OH_UdmfGetDataParams* options, char* key, unsigned int keyLen);

/**
 * @brief Cancel the data sync process.
 *
 * @param uiContext UI实例对象指针。
 * @param key 表示数据的key值并通过 {@link OH_ArkUI_DragEvent_StartDataLoading} 返回。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 *         {@link ARKUI_ERROR_CODE_OPERATION_FAILED} 当前没有进行中的数据同步任务。
 * @since 15
 */
int32_t OH_ArkUI_CancelDataLoading(ArkUI_ContextHandle uiContext, const char* key);

/**
 * @brief 设置是否在执行onDrop回调之前禁用数据预获取过程。系统将重试获取数据，直到达到最大时间限制（目前为2.4秒），
 *        这对跨设备的拖动操作非常有用，因为它有助于系统稳定通信。然而，对于{@link OH_ArkUI_DragEvent_StartDataLoading}方法而言，
 *        这一特性显得多余。该方法采用异步机制获取数据，因此在onDrop中使用{@link OH_ArkUI_DragEvent_StartDataLoading}时，
 *        为了避免在onDrop执行前意外获取数据，必须将此字段设置为true。
 *
 * @param node 组件节点指针。
 * @param disabled 表示是禁用数据预取过程。true表示禁止，false表示不禁止。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 15
 */
int32_t OH_ArkUI_DisableDropDataPrefetchOnNode(ArkUI_NodeHandle node, bool disabled);

/**
 * @brief 控制是否使能严格dragEvent上报，建议开启；默认是不开启的；
 *        当不开启时，从父组件拖移进子组件时，父组件并不会收到leave的通知；而开启之后，只要前后两个组件发生变化，上一个组件就会收到
 *        leave，新的组件收到enter通知；该配置与具体的UI实例相关，需要通过传入一个当前UI实例上的一个具体的组件节点来关联。
 *
 * @param node 组件节点指针。
 * @param enabled 是否开启严格上报。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_SetDragEventStrictReportWithNode(ArkUI_NodeHandle node, bool enabled);

/**
 * @brief 控制是否使能严格dragEvent上报，建议开启；默认是不开启的;
 *        当不开启时，从父组件拖移进子组件时，父组件并不会收到leave的通知；而开启之后，只要前后两个组件发生变化，上一个组件就会收到
 *        leave，新的组件收到enter通知；该配置与具体的UI实例相关，可通过传入一个UI实例进行关联。
 *
 * @param uiContext UI实例指针。
 * @param enabled 是否开启严格上报。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_SetDragEventStrictReportWithContext(ArkUI_ContextHandle uiContext, bool enabled);

/**
 * @brief 配置组件允许接受落入的数据类型，该接口会重置通过 {@link OH_ArkUI_DisallowNodeAnyDropDataTypes} 或
 *        {@link OH_ArkUI_AllowNodeAllDropDataTypes}进行的配置。
 *
 * @param node 组件节点指针。
 * @param typesArray 允许落入的数据类型数组。
 * @param count 数组的长度。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_SetNodeAllowedDropDataTypes(ArkUI_NodeHandle node, const char* typesArray[], int32_t count);

/**
 * @brief 配置组件不允许接受任何数据类型，该接口会重置通过{@link OH_ArkUI_SetNodeAllowedDropDataTypes}配置的数据类型。
 *
 * @param node 组件节点指针。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DisallowNodeAnyDropDataTypes(ArkUI_NodeHandle node);

/**
 * @brief 配置组件允许接受任意数据类型，该接口会重置通过{@link OH_ArkUI_SetNodeAllowedDropDataTypes}配置的数据类型。
 *
 * @param node 组件节点指针。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_AllowNodeAllDropDataTypes(ArkUI_NodeHandle node);

/**
 * @brief 设置该组件是否允许进行拖拽。
 *
 * @param node 组件节点指针。
 * @param bool 是否支持拖出。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_SetNodeDraggable(ArkUI_NodeHandle node, bool enabled);

/**
 * @brief 设置组件在被拖拽时的自定义跟手图。
 *
 * @param node 目标组件节点指针。
 * @param preview 自定义跟手图，使用 pixelmap 格式。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_SetNodeDragPreview(ArkUI_NodeHandle node, OH_PixelmapNative* preview);

/**
 * @brief 构建一个ArkUI_DragPreviewOption对象。
 *
 * @return ArkUI_DragPreviewOption对象。
 * @since 12
 */
ArkUI_DragPreviewOption* OH_ArkUI_CreateDragPreviewOption(void);

/**
 * @brief 销毁跟手图自定义参数对象实例。
 *
 * @param option 自定义参数。
 * @since 12
 */
void OH_ArkUI_DragPreviewOption_Dispose(ArkUI_DragPreviewOption* option);

/**
 * @brief 设置拖拽跟手图是否根据系统定义自动进行缩放。
 *
 * @param option 自定义参数。
 * @param scaleMode 设置组件拖拽过程中的跟手图缩放模式。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragPreviewOption_SetScaleMode(ArkUI_DragPreviewOption* option, ArkUI_DragPreviewScaleMode scaleMode);

/**
 * @brief 设置跟手图背板默认的投影效果，默认不开启。
 *
 * @param option 自定义参数。
 * @param enabled 是否使用默认投影效果。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragPreviewOption_SetDefaultShadowEnabled(ArkUI_DragPreviewOption* option, bool enabled);

/**
 * @brief 设置跟手图背板默认的圆角效果，默认不开启。
 *
 * @param option 自定义参数。
 * @param enabled 是否开启圆角效果显示。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragPreviewOption_SetDefaultRadiusEnabled(ArkUI_DragPreviewOption* option, bool enabled);

/**
 * @brief 设置跟手图背板是否显示角标,开启后,系统会根据拖拽数量自动进行角标显示。
 *
 * @param option 自定义参数。
 * @param enabled 是否开启角标显示。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragPreviewOption_SetNumberBadgeEnabled(ArkUI_DragPreviewOption* option, bool enabled);

/**
 * @brief 强制显示角标的数量,覆盖SetDragPreviewNumberBadgeEnabled设置的值。
 *
 * @param option 自定义参数。
 * @param forcedNumber 角标的数量。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragPreviewOption_SetBadgeNumber(ArkUI_DragPreviewOption* option, uint32_t forcedNumber);

/**
 * @brief 配置是否开启点按时的默认动画。
 *
 * @param option 自定义参数。
 * @param enabled 是否开启默认点按效果。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragPreviewOption_SetDefaultAnimationBeforeLiftingEnabled(
    ArkUI_DragPreviewOption* option, bool enabled);
/**
 * @brief 将构造的ArkUI_DragPreviewOption设置给组件。
 *
 * @param node 组件节点指针。
 * @param option 自定义参数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_SetNodeDragPreviewOption(ArkUI_NodeHandle node, ArkUI_DragPreviewOption* option);

/**
 * @brief 创建一个拖拽操作对象，该对象需与一个UI实例相关联，可通过传入一个当前UI实例的某个组件节点来指定。
 *
 * @param node 组件节点指针。
 * @return ArkUI_DragAction对象指针，如果创建失败，则返回空。
 * @since 12
 */
ArkUI_DragAction* OH_ArkUI_CreateDragActionWithNode(ArkUI_NodeHandle node);

/**
 * @brief 创建一个拖拽操作对象，该对象需与一个UI实例相关联，可通过传入一个UI实例指针来关联。
 *
 * @param uiContext UI实例对象指针。
 * @return ArkUI_DragAction对象，如果创建失败，则返回空。
 * @since 12
 */
ArkUI_DragAction* OH_ArkUI_CreateDragActionWithContext(ArkUI_ContextHandle uiContext);

/**
 * @brief 销毁创建的 ArkUI_DragAction 对象。
 *
 * @param dragAction 拖拽行为对象。
 * @since 12
 */
void OH_ArkUI_DragAction_Dispose(ArkUI_DragAction* dragAction);

/**
 * @brief 设置手指ID，当屏幕上仅有一只手指在操作时，pointer ID 为 0；一般情况下，配置 0 即可。
 *
 * @param dragAction 拖拽行为对象。
 * @param pointer 手指ID，范围 0～9。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragAction_SetPointerId(ArkUI_DragAction* dragAction, int32_t pointer);

/**
 * @brief 设置拖拽跟手图，只能使用 pixelmap 格式对象。
 *
 * @param dragAction 拖拽行为对象。
 * @param pixelmapArray 拖拽跟手图位图数组。
 * @param size 拖拽跟手图数量。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragAction_SetPixelMaps(
    ArkUI_DragAction* dragAction, OH_PixelmapNative* pixelmapArray[], int32_t size);

/**
 * @brief 设置跟手点，相对于设置的第一个pixelmap的左上角。
 *
 * @param dragAction 拖拽行为对象。
 * @param x 跟手点坐标x值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragAction_SetTouchPointX(ArkUI_DragAction* dragAction, float x);

/**
 * @brief 设置跟手点,相对于设置的第一个pixelmap的左上角。
 *
 * @param dragAction 拖拽行为对象。
 * @param y 跟手点坐标y值。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragAction_SetTouchPointY(ArkUI_DragAction* dragAction, float y);

/**
 * @brief 设置拖拽数据。
 *
 * @param dragAction 拖拽行为对象。
 * @param data 拖拽数据。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragAction_SetData(ArkUI_DragAction* dragAction, OH_UdmfData* data);

/**
 * @brief 将构造的ArkUI_DragPreviewOption设置给ArkUI_DragAction。
 *
 * @param dragAction 拖拽行为对象。
 * @param option 自定义参数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragAction_SetDragPreviewOption(ArkUI_DragAction* dragAction, ArkUI_DragPreviewOption* option);

/**
 * @brief 注册拖拽状态监听回调,该回调可感知到拖拽已经发起或用户松手结束的状态,
 *        可通过该监听获取到落入方对数据的接收处理是否成功。
 *
 * @param dragAction 拖拽行为对象。
 * @param userData 应用自定义数据。
 * @param listener
 * 状态监听回调，回调触发时，系统会返回一个拖拽状态对象指针，该指针会在回调之行完成后被销毁，应用不应再持有。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_DragAction_RegisterStatusListener(ArkUI_DragAction* dragAction, void* userData,
    void(*listener)(ArkUI_DragAndDropInfo* dragAndDropInfo, void* userData));

/**
 * @brief 获取给定拖拽事件的屏幕显示id。
 *
 * @param event ArkUI_DragEvent事件指针。
 * @param displayId 出参，返回屏幕的显示id。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 20
 */
ArkUI_ErrorCode OH_ArkUI_DragEvent_GetDisplayId(ArkUI_DragEvent event, int32_t* displayId);

/**
 * @brief 解注册拖拽状态监听回调。
 *
 * @param dragAction 拖拽行为对象。
 * @since 12
 */
void OH_ArkUI_DragAction_UnregisterStatusListener(ArkUI_DragAction* dragAction);

/**
 * @brief 获取dragaction发起拖拽的状态，获取异常时返回 ArkUI_DRAG_STATUS_UNKNOWN。
 *
 * @param dragAndDropInfo 拖拽状态监听返回的拖拽相关信息。
 * @return ArkUI_DragStatus 拖拽状态，如果获取失败，返回默认值 ArkUI_DRAG_STATUS_UNKNOWN。
 * @since 12
 */
ArkUI_DragStatus OH_ArkUI_DragAndDropInfo_GetDragStatus(ArkUI_DragAndDropInfo* dragAndDropInfo);

/**
 * @brief 通过dragAndDropInfo获取到DragEvent，可通过DragEvent获取释放结果等。
 *
 * @param dragAndDropInfo 拖拽状态监听返回的拖拽相关信息。
 * @return ArkUI_DragEvent 拖拽事件，如果获取失败，则返回空。
 * @since 12
 */
ArkUI_DragEvent* OH_ArkUI_DragAndDropInfo_GetDragEvent(ArkUI_DragAndDropInfo* dragAndDropInfo);

/**
 * @brief 通过调用该方法，系统可以判断目标组件上配置的可支持数据类型是否包含所拖拽的数据类型。如何在组件上配置所支持的数据类型，可参考{@link OH_ArkUI_SetNodeAllowedDropDataTypes}。
 *        系统默认仅进行数据类型的严格匹配。例如，当声明支持"general.image"数据类型时，仅当数据分享方填充的数据类型为"general.image"时，才会判定为允许落入。
 *        调用该方法后，系统在进行判断时，会考虑数据类型之间的关系，如"general.image.jpeg"类型是"general.image"的子类型之一，即使不完全相等，也会判定为允许落入。
 *
 * @param node 组件节点指针。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 20
 */
ArkUI_ErrorCode OH_ArkUI_EnableDragDataTypeRelationCheck(ArkUI_NodeHandle node);

/**
 * @brief 通过构造的DragAction对象发起拖拽。
 *
 * @param dragAction 拖拽action对象。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 12
 */
int32_t OH_ArkUI_StartDrag(ArkUI_DragAction* dragAction);

/**
 * @brief 设置是否开启禁止角标显示。默认情况下，如果组件无法接收或处理用户拖动的数据，系统将显示一个与移动操作相匹配的角标图标，即没有角标显示。
 *        应用程序可以在相应的UI实例上调用此方法，以明确声明在无法处理数据的情况下需要显示一个明确的“禁止”指示符。
 *
 * @param uiContext UI实例对象指针。
 * @param enabled 是否禁止角标显示。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 20
 */
ArkUI_ErrorCode OH_ArkUI_SetDragDisallowStatusShowingEnabled(ArkUI_ContextHandle uiContext, bool enabled);

/**
 * @brief 请求延迟处理拖拽结束事件，等待应用程序确认操作结果。应用程序需通过 {@link OH_ArkUI_NotifyDragResult}
 *        接口将最终结果回传至系统，并在所有处理完成后调用 {@link OH_ArkUI_NotifyDragEndPendingDone}。
 *        最大等待时间为2秒。
 *
 * @param event 指向 <b>ArkUI_DragEvent</b> 对象的指针。
 * @param requestIdentify 系统自动生成的请求标识符，是一个输出参数，需要为一个有效的地址。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR}  成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID}  函数参数异常。
 *         {@link ARKUI_ERROR_CODE_DRAG_DROP_OPERATION_NOT_ALLOWED}  当前不处于拖放处理阶段
 * @since 18
 */
int32_t OH_ArkUI_DragEvent_RequestDragEndPending(ArkUI_DragEvent* event, int32_t* requestIdentify);

/**
 * @brief 通知系统最终拖拽结果。系统会校验请求标识符是否与{@link OH_ArkUI_DragEvent_RequestDragEndPending}
 *        返回的一致，不一致则忽略本次调用。
 *
 * @param requestIdentify 由 {@link OH_ArkUI_DragEvent_RequestDragEndPending} 返回的标识符。
 * @param result 拖拽结果枚举值（{@link ArkUI_DragResult} 类型）。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR}  成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID}  函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_NotifyDragResult(int32_t requestIdentify, ArkUI_DragResult result);

/**
 * @brief 通知系统所有异步处理已完成，可结束拖拽结束挂起状态。
 *
 * @param requestIdentify 由 {@link OH_ArkUI_DragEvent_RequestDragEndPending} 返回的标识符。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR}  成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID}  函数参数异常。
 * @since 18
 */
int32_t OH_ArkUI_NotifyDragEndPendingDone(int32_t requestIdentify);

#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_DRAG_AND_DROP_H
/** @} */
