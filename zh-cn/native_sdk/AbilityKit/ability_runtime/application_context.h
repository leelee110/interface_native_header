/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AbilityRuntime
 * @{
 *
 * @brief 提供应用级别上下文相关的接口。
 *
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

/**
 * @file application_context.h
 *
 * @brief 提供应用级别上下文相关的接口。
 *
 * @library libability_runtime.so
 * @kit AbilityKit
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

#ifndef ABILITY_RUNTIME_APPLICATION_CONTEXT_H
#define ABILITY_RUNTIME_APPLICATION_CONTEXT_H

#include <stdint.h>
#include <stddef.h>
#include <AbilityKit/ability_base/want.h>
#include "ability_runtime_common.h"
#include "context_constant.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 获取应用级别上下文的缓存目录。
 *
 * @param buffer 指向缓冲区的指针，用于接收应用级别上下文的缓存目录。
 * @param bufferSize 缓冲区大小，单位为字节。
 * @param writeLength 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示实际写入到缓冲区的字符串长度。
 * @return The error code.
 *         {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 查询成功。
 *         {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 入参buffer或者writeLength为空，或者缓冲区大小小于需要写入的大小。
 * @since 13
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_ApplicationContextGetCacheDir(
    char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
 * @brief 获取应用级别上下文的文件数据加密等级。
 *
 * @param areaMode 指向接收数据加密等级的指针
 * @return The error code.
 *         {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 查询成功。
 *         {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 入参areaMode为空。
 * @since 13
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_ApplicationContextGetAreaMode(AbilityRuntime_AreaMode* areaMode);

/**
 * @brief 获取应用包名。
 *
 * @param buffer 指向缓冲区的指针，用于接收应用包名。
 * @param bufferSize 缓冲区大小，单位为字节。
 * @param writeLength 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示实际写入到缓冲区的字符串长度。
 * @return The error code.
 *         {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 查询成功。
 *         {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 入参buffer或者writeLength为空，或者缓冲区大小小于需要写入的大小。
 * @since 13
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_ApplicationContextGetBundleName(
    char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
 * @brief 启动当前应用的UIAbility。
 *
 * @permission {@code ohos.permission.NDK_START_SELF_UI_ABILITY}
 * @param want 启动当前应用UIAbility时需要的Want信息。
 * 详细内容参考 {@link AbilityBase_Want}.
 * @return 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_ERROR} 时，表示接口调用成功。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PERMISSION_DENIED} 时，表示调用方权限校验失败。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_PARAM_INVALID} 时，表示调用方入参校验失败。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NOT_SUPPORTED} 时，表示设备类型不支持。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NO_SUCH_ABILITY} 时，表示指定的Ability名称不存在。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_INCORRECT_ABILITY_TYPE} 时，表示接口调用Ability类型错误。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_CROWDTEST_EXPIRED} 时，表示众测应用到期。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_WUKONG_MODE} 时，表示Wukong模式，不允许启动/停止Ability。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_CONTROLLED} 时，表示应用被管控。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_EDM_CONTROLLED} 时，表示应用被EDM管控。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_CROSS_APP} 时，表示限制API 11以上版本三方应用跳转。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_INTERNAL} 时，表示内部错误。
 * 在返回 {@link ABILITY_RUNTIME_ERROR_CODE_NOT_TOP_ABILITY} 时，表示非顶层应用。
 * 详细内容参考 {@link AbilityRuntime_ErrorCode}.
 * @since 15
 */
AbilityRuntime_ErrorCode OH_AbilityRuntime_StartSelfUIAbility(AbilityBase_Want *want);

#ifdef __cplusplus
} // extern "C"
#endif

/** @} */
#endif // ABILITY_RUNTIME_APPLICATION_CONTEXT_H
