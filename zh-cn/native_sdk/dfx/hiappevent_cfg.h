/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HiAppEvent
 * @{
 *
 * @brief HiAppEvent模块提供应用事件打点功能。
 *
 * 为应用程序提供事件打点功能，记录运行过程中上报的故障事件、统计事件、安全事件和用户行为事件。基于事件信息，开发者可以分析应用的运行状态。
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file hiappevent_cfg.h
 *
 * @brief 定义事件打点配置函数的所有配置项名称。
 *
 * 如果开发者想要对应用事件打点功能进行配置，开发者可以直接使用配置项常量。
 *
 * 示例代码:
 * <pre>
 *     bool res = OH_HiAppEvent_Configure(MAX_STORAGE, "100M");
 * </pre>
 *
 * @kit PerformanceAnalysisKit
 * @library libhiappevent_ndk.z.so
 * @syscap SystemCapability.HiviewDFX.HiAppEvent
 * @since 8
 * @version 1.0
 */

#ifndef HIVIEWDFX_HIAPPEVENT_CONFIG_H
#define HIVIEWDFX_HIAPPEVENT_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 事件打点开关。默认值为false。true：关闭打点功能，false：不关闭打点功能。
 *
 * @since 8
 * @version 1.0
 */
#define DISABLE "disable"

/**
 * @brief 事件文件目录存储配额大小。默认值为“10M”。
 *
 * @since 8
 * @version 1.0
 */
#define MAX_STORAGE "max_storage"

#ifdef __cplusplus
}
#endif
/** @} */
#endif // HIVIEWDFX_HIAPPEVENT_CONFIG_H