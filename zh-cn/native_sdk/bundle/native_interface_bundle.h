/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Native_Bundle
 * @{
 *
 * @brief 提供查询应用包信息的功能，获取到的信息包含应用包名和应用指纹信息。
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_interface_bundle.h
 *
 * @brief 提供查询应用包信息的功能，获取到的信息包含应用包名和应用指纹信息。
 *
 * @library libbundle.z.so
 * @syscap SystemCapability.BundleManager.BundleFramework.Core
 * @since 9
 * @version 1.0
 */
#ifndef FOUNDATION_APPEXECFWK_STANDARD_KITS_APPKIT_NATIVE_BUNDLE_INCLUDE_NATIVE_INTERFACE_BUNDLE_H
#define FOUNDATION_APPEXECFWK_STANDARD_KITS_APPKIT_NATIVE_BUNDLE_INCLUDE_NATIVE_INTERFACE_BUNDLE_H

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 应用包信息数据结构，包含应用包名和应用指纹信息。
 *
 * @since 9
 * @version 1.0
 */
typedef struct OH_NativeBundle_ApplicationInfo
{
    /** 应用的包名 */
    char* bundleName;
    /** 应用的指纹信息 */
    char* fingerprint;
};

/**
 * @brief 获取当前应用信息，包含应用包名和应用指纹信息。
 *
 * @return 接口调用成功，返回当前应用信息，结构体内的数据需要主动释放；接口调用失败，返回结构体内的数据为空指针。
 * @since 9
 * @version 1.0
 */
OH_NativeBundle_ApplicationInfo OH_NativeBundle_GetCurrentApplicationInfo();

/**
 * @brief 获取当前应用的appId。appId是应用的唯一标识，由应用包名和签名信息决定。
 *
 * @return 接口调用成功，返回当前应用的appId信息。
 * @since 11
 * @version 1.0
 */
char* OH_NativeBundle_GetAppId();

/**
 * @brief 获取当前应用的载体ID。载体ID指应用的唯一标识，是AppGallery Connect创建应用时分配的APP ID，为云端统一分配的随机字符串。
 * 该ID在应用全生命周期中不会发生变化，包括版本升级、证书变更、开发者公私钥变更、应用转移等。
 *
 * @return 接口调用成功，返回当前应用的载体ID信息。
 * @since 11
 * @version 1.0
 */
char* OH_NativeBundle_GetAppIdentifier();

#ifdef __cplusplus
};
#endif
#endif // FOUNDATION_APPEXECFWK_STANDARD_KITS_APPKIT_NATIVE_BUNDLE_INCLUDE_NATIVE_INTERFACE_BUNDLE_H