/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ImageEffect
 * @{
 *
 * @brief 提供图片编辑能力。
 *
 * @since 12
 */

/**
 * @file image_effect.h
 *
 * @brief 声明效果器相关接口。
 *
 * 效果器提供了滤镜的添加、删除、查询等功能。开发者可以通过效果器提供的接口将多个滤镜组合串联，从而实现较为复杂的效果调节功能。<p>
 * 同时，效果器支持多种输入类型，如Pixelmap、URI、Surface、Picture。不同的输入类型在效果器内部都会转换为内存对象，通过滤镜的效果处理，
 * 获得处理结果。
 *
 * @library libimage_effect.so
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @since 12
 */

#ifndef NATIVE_IMAGE_EFFECT_H
#define NATIVE_IMAGE_EFFECT_H

#include "image_effect_errors.h"
#include "image_effect_filter.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct OH_NativeBuffer OH_NativeBuffer;
typedef struct NativeWindow OHNativeWindow;
typedef struct OH_PictureNative OH_PictureNative;

/**
 * @brief 定义效果器结构类型。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @since 12
 */
typedef struct OH_ImageEffect OH_ImageEffect;

/**
 * @brief 创建OH_ImageEffect实例，调用{@link OH_ImageEffect_Release}进行资源释放。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param name 效果器名，用于标识效果器，由用户自定义，不为空的字符串。
 * @return 返回一个指向OH_ImageEffect实例的指针，创建失败时返回空指针。
 * @since 12
 */
OH_ImageEffect *OH_ImageEffect_Create(const char *name);

/**
 * @brief 添加滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param filterName 滤镜名。
 * @return 返回一个指向OH_EffectFilter实例的指针，滤镜名无效时返回空指针。
 * @since 12
 */
OH_EffectFilter *OH_ImageEffect_AddFilter(OH_ImageEffect *imageEffect, const char *filterName);

/**
 * @brief 添加指定滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param filter 滤镜指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_AddFilterByFilter(OH_ImageEffect *imageEffect, OH_EffectFilter *filter);

/**
 * @brief 插入滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param index 插入滤镜位置索引。
 * @param filterName 滤镜名。
 * @return 返回一个指向OH_EffectFilter实例的指针，参数无效时返回空指针。
 * @since 12
 */
OH_EffectFilter *OH_ImageEffect_InsertFilter(OH_ImageEffect *imageEffect, uint32_t index, const char *filterName);

/**
 * @brief 按指定位置插入滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param index 插入滤镜位置索引。
 * @param filter 滤镜指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_InsertFilterByFilter(OH_ImageEffect *imageEffect, uint32_t index,
    OH_EffectFilter *filter);

/**
 * @brief 移除滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param filterName 滤镜名。
 * @return 所删除的滤镜个数。
 * @since 12
 */
int32_t OH_ImageEffect_RemoveFilter(OH_ImageEffect *imageEffect, const char *filterName);

/**
 * @brief 移除指定位置滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param index 移除滤镜位置索引。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_RemoveFilterByIndex(OH_ImageEffect *imageEffect, uint32_t index);

/**
 * @brief 替换滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param filterName 滤镜名。
 * @return 返回一个指向OH_EffectFilter实例的指针，替换失败时返回空指针。
 * @since 12
 */
OH_EffectFilter *OH_ImageEffect_ReplaceFilter(OH_ImageEffect *imageEffect, uint32_t index, const char *filterName);

/**
 * @brief 替换指定位置滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param index 替换滤镜位置索引。
 * @param filterName 滤镜名。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_ReplaceFilterByFilter(OH_ImageEffect *imageEffect, uint32_t index,
    const char *filterName);

/**
 * @brief 查询已添加滤镜个数。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @return 已添加的滤镜个数。
 * @since 12
 */
int32_t OH_ImageEffect_GetFilterCount(OH_ImageEffect *imageEffect);

/**
 * @brief 查询已添加滤镜。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param index 待查询滤镜位置索引。
 * @return 返回一个指向OH_EffectFilter实例的指针，参数无效时返回空指针。
 * @since 12
 */
OH_EffectFilter *OH_ImageEffect_GetFilter(OH_ImageEffect *imageEffect, uint32_t index);

/**
 * @brief 设置配置信息。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param key 配置参数。
 * @param value 配置参数值。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 *         {@link EFFECT_KEY_ERROR}如果参数无效。
 *         {@link EFFECT_PARAM_ERROR}如果参数值无效。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_Configure(OH_ImageEffect *imageEffect, const char *key,
    const ImageEffect_Any *value);

/**
 * @brief 设置输出Surface。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param nativeWindow 指向OHNativeWindow实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_SetOutputSurface(OH_ImageEffect *imageEffect, OHNativeWindow *nativeWindow);

/**
 * @brief 获取输入Surface。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param nativeWindow 指向OHNativeWindow实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_GetInputSurface(OH_ImageEffect *imageEffect, OHNativeWindow **nativeWindow);

/**
 * @brief 设置输入的Pixelmap。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param pixelmap 指向OH_PixelmapNative实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_SetInputPixelmap(OH_ImageEffect *imageEffect, OH_PixelmapNative *pixelmap);

/**
 * @brief 设置输出的Pixelmap。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param pixelmap 指向OH_PixelmapNative实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_SetOutputPixelmap(OH_ImageEffect *imageEffect, OH_PixelmapNative *pixelmap);

/**
 * @brief 设置输入的NativeBuffer。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param nativeBuffer 指向OH_NativeBuffer实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_SetInputNativeBuffer(OH_ImageEffect *imageEffect, OH_NativeBuffer *nativeBuffer);

/**
 * @brief 设置输出的NativeBuffer。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param nativeBuffer 指向OH_NativeBuffer实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_SetOutputNativeBuffer(OH_ImageEffect *imageEffect, OH_NativeBuffer *nativeBuffer);

/**
 * @brief 设置输入的URI。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param uri 图片URI。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_SetInputUri(OH_ImageEffect *imageEffect, const char *uri);

/**
 * @brief 设置输出的URI。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param uri 图片URI。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_SetOutputUri(OH_ImageEffect *imageEffect, const char *uri);

/**
 * @brief 设置输入的Picture。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param picture 指向OH_PictureNative实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 13
 */
ImageEffect_ErrorCode OH_ImageEffect_SetInputPicture(OH_ImageEffect *imageEffect, OH_PictureNative *picture);

/**
 * @brief 设置输出的Picture。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param picture 指向OH_PictureNative实例的指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 13
 */
ImageEffect_ErrorCode OH_ImageEffect_SetOutputPicture(OH_ImageEffect *imageEffect, OH_PictureNative *picture);

/**
 * @brief 启动效果器。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 *         {@link EFFECT_INPUT_OUTPUT_NOT_SUPPORTED}如果待处理输入、输出图像数据类型不一致。
 *         {@link EFFECT_COLOR_SPACE_NOT_MATCH}如果输入、输出图像色彩空间不配置。
 *         {@link EFFECT_ALLOCATE_MEMORY_FAILED}如果内存申请失败。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_Start(OH_ImageEffect *imageEffect);

/**
 * @brief 停止生效效果。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_Stop(OH_ImageEffect *imageEffect);

/**
 * @brief 释放OH_ImageEffect实例资源。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_Release(OH_ImageEffect *imageEffect);

/**
 * @brief 序列化效果器。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param imageEffect 效果器指针。
 * @param info 指向char数组的指针，返回序列化JSON字符串。
 * @return {@link EFFECT_SUCCESS}如果方法调用成功。
 *         {@link EFFECT_ERROR_PARAM_INVALID}如果入参为空指针。
 * @since 12
 */
ImageEffect_ErrorCode OH_ImageEffect_Save(OH_ImageEffect *imageEffect, char **info);

/**
 * @brief 反序列化效果器。
 *
 * @syscap SystemCapability.Multimedia.ImageEffect.Core
 * @param info 序列化JSON字符串。
 * @return 反序列化成功时返回OH_ImageEffect实例，否则返回空指针。
 * @since 12
 */
OH_ImageEffect *OH_ImageEffect_Restore(const char *info);

#ifdef __cplusplus
}
#endif
#endif // NATIVE_IMAGE_EFFECT_H
/** @} */