/*
 * Copyright (C) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AVMetadataExtractor
 * @{
 *
 * @brief 提供从媒体资源中获取元数据的API。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */

/**
 * @file avmetadata_extractor_base.h
 *
 * @brief 定义AVMetadataExtractor的常量。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @kit MediaKit
 * @library libavmetadata_extractor.so
 * @since 18
 */

#ifndef MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVMETADATA_EXTRACTOR_BASE_H
#define MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVMETADATA_EXTRACTOR_BASE_H

#include <stdint.h>

#include "native_avformat.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief 获取专辑的标题的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_ALBUM = "album";

/**
 * @brief 获取专辑的艺术家的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_ALBUM_ARTIST = "albumArtist";

/**
 * @brief 获取媒体资源的艺术家的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_ARTIST = "artist";

/**
 * @brief 获取媒体资源的作者的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_AUTHOR = "author";

/**
 * @brief 取媒体资源的创建时间的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_DATE_TIME = "dateTime";

/**
 * @brief 获取媒体资源的创建时间的关键字，对应值类型是const char*，按YYYY-MM-DD HH:mm:ss格式输出。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_DATE_TIME_FORMAT = "dateTimeFormat";

/**
 * @brief 获取媒体资源的作曲家的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_COMPOSER = "composer";

/**
 * @brief 获取媒体资源的时长的关键字，对应值类型是int64_t，单位为毫秒（ms）。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_DURATION = "duration";

/**
 * @brief 获取媒体资源的类型或体裁的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_GENRE = "genre";

/**
 * @brief 获取媒体资源是否包含音频的关键字，对应值类型是int32_t。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_HAS_AUDIO = "hasAudio";

/**
 * @brief 获取媒体资源是否包含视频的关键字，对应值类型是int32_t。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_HAS_VIDEO = "hasVideo";

/**
 * @brief 获取媒体资源的mime类型的关键字，对应值类型是const char*，例如：“video/mp4”、“audio/mp4”和“audio/amr wb”。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_MIME_TYPE = "mimeType";

/**
 * @brief 获取媒体资源的轨道数量的关键字，对应值类型是int32_t。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_TRACK_COUNT = "trackCount";

/**
 * @brief 获取音频的采样率的关键字，对应值类型是int32_t，单位为赫兹（Hz）。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_SAMPLE_RATE = "sampleRate";

/**
 * @brief 获取媒体资源的标题的关键字，对应值类型是const char*。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_TITLE = "title";

/**
 * @brief 获取视频的高度的关键字，对应值类型int32_t，单位为像素。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_VIDEO_HEIGHT = "videoHeight";

/**
 * @brief 获取视频的宽度的关键字，对应值类型int32_t，单位为像素。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_VIDEO_WIDTH = "videoWidth";

/**
 * @brief 获取视频的旋转方向的关键字，对应值类型int32_t，单位为度（°）。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_VIDEO_ORIENTATION = "videoOrientation";

/**
 * @brief 获取是否是HDR Vivid视频的关键字，对应值类型int32_t。
 *        详情请参阅 {@link OH_Core_HdrType} 定义在 {@link media_types.h} 。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_VIDEO_IS_HDR_VIVID = "hdrType";

/**
 * @brief 获取地理位置中的纬度值的关键字，对应值类型float。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_LOCATION_LATITUDE = "latitude";

/**
 * @brief 获取地理位置中的经度值的关键字，对应值类型float。
 *
 * @syscap SystemCapability.Multimedia.Media.AVMetadataExtractor
 * @since 18
 */
static const char* OH_AVMETADATA_EXTRACTOR_LOCATION_LONGITUDE = "longitude";

#ifdef __cplusplus
}
#endif
#endif // MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVMETADATA_EXTRACTOR_BASE_H
/** @} */
