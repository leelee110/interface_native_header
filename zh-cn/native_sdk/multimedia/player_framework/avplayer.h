/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AVPlayer
 * @{
 *
 * @brief 为媒体源提供播放能力的API。
 *
 * @Syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */

/**
 * @file avplayer.h
 *
 * @brief定义avplayer接口。使用AVPlayer提供的Native API播放媒体源

 *
 * @library libavplayer.so
 * @since 11
 * @version 1.0
 */

#ifndef MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_H
#define MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "native_averrors.h"
#include "avplayer_base.h"
#include "native_window/external_window.h"
#include "ohaudio/native_audiostream_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief MediaKeySession类型。
 * @since 12
 * @version 1.0
 */
typedef struct MediaKeySession MediaKeySession;
/**
 * @brief DRM_MediaKeySystemInfo类型。
 * @since 12
 * @version 1.0
 */
typedef struct DRM_MediaKeySystemInfo DRM_MediaKeySystemInfo;

/**
 * @brief 播放器DRM信息更新时被调用。
 * @param player 指向OH_AVPlayer实例的指针。
 * @param mediaKeySystemInfo DRM信息。
 * @return void
 * @since 12
 * @version 1.0
 */
typedef void (*Player_MediaKeySystemInfoCallback)(OH_AVPlayer *player, DRM_MediaKeySystemInfo* mediaKeySystemInfo);

/**
 * @brief 创建播放器
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @return 返回指向OH_AVPlayer实例的指针如果创建成功，否则返回空指针。\n
 * 可能的失败原因：1.PlayerFactory::CreatePlayer执行失败；2.new PlayerObject执行失败。
 * @since 11
 * @version 1.0
*/
OH_AVPlayer *OH_AVPlayer_Create(void);

/**
 * @brief 设置播放器的播放源。对应的源可以是http url
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param url播放源
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果设置成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针，url为空或者player SetUrlSource执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetURLSource(OH_AVPlayer *player, const char *url);

/**
 * @brief 设置播放器的播放媒体文件描述符来源
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param fd媒体源的文件描述符
 * @param offset媒体源在文件描述符中的偏移量
 * @param  size表示媒体源的大小
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果fd设置成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player SetFdSource执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetFDSource(OH_AVPlayer *player, int32_t fd, int64_t offset, int64_t size);

/**
 * @brief 准备播放环境，异步缓存媒体数据
 *
 * 此函数必须在{@link SetSource}之后调用
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功将{@link准备}添加到任务队列中；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player Prepare执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Prepare(OH_AVPlayer *player);

/**
 * @brief 开始播放
 *
 * 此函数必须在{@link Prepare}之后调用。如果播放器状态为<Prepared>。调用此函数开始播放。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果开始播放；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player Play执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Play(OH_AVPlayer *player);

/**
 * @brief 暂停播放.
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功将{@link Pause}添加到任务队列中；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player Pause执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Pause(OH_AVPlayer *player);

/**
 * @brief 停止播放.
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功将{@link stop}添加到任务队列；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player Stop执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Stop(OH_AVPlayer *player);

/**
 * @brief 将播放器恢复到初始状态
 *
 * 函数调用完成后，调用{@link SetSource}添加播放源。调用{@link Prepare}后，调用{@link Play}重新开始播放。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功将{@link reset}添加到任务队列；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player Reset执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Reset(OH_AVPlayer *player);

/**
 * @brief 异步释放播放器资源
 *
 * 异步释放保证性能，但无法保证是否释放了播放画面的surfacebuffer。调用者需要保证播放画面窗口的生命周期安全
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功将{@link Release}添加到任务队列中；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player Release执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Release(OH_AVPlayer *player);

/**
 * @brief 同步释放播放器资源
 *
 * 同步过程保证了播放画面的surfacebuffer释放，但这个界面会花费很长时间（发动机非怠速状态时），要求调用者自己设计异步机制
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果播放被释放；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player ReleaseSync执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_ReleaseSync(OH_AVPlayer *player);

/**
 * @brief 设置播放器的音量
 *
 * 可以在播放或暂停的过程中使用。<0>表示无声音。,<1>为原始值。

 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param 要设置的左声道目标音量
 * @param 要设置的右声道目标音量
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果设置了音量；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player SetVolume执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetVolume(OH_AVPlayer *player, float leftVolume, float rightVolume);

/**
 * @brief 改变播放位置
 *
 * 此函数可以在播放或暂停时使用.
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param mSeconds播放目标位置，精确到毫秒
 * @param mode播放器的跳转模式。具体请参见{@link AVPlayerSeekMode}
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果完成跳转；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player Seek执行失败。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_Seek(OH_AVPlayer *player, int32_t mSeconds, AVPlayerSeekMode mode);

/**
 * @brief 获取播放位置，精确到毫秒
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param currentTime播放位置
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果获取当前位置；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player GetCurrentTime执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetCurrentTime(OH_AVPlayer *player, int32_t *currentTime);

/**
 * @brief 获取视频宽度
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param videoWidth视频宽度
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果获取视频宽度；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetVideoWidth(OH_AVPlayer *player, int32_t *videoWidth);

/**
 * @brief 获取视频高度
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param videoHeights视频高度
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果获取视频高度；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetVideoHeight(OH_AVPlayer *player, int32_t *videoHeight);

/**
 * @brief 设置播放器播放速率
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param speed可以设置速率模式{@link AVPlaybackSpeed}。
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果设置播放速率成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetPlaybackSpeed(OH_AVPlayer *player, AVPlaybackSpeed speed);

/**
 * @brief 获取当前播放器播放速率
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param speed 可以获取的速率模式{@link AVPlaybackSpeed}
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果获取播放速率成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player GetPlaybackSpeed执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetPlaybackSpeed(OH_AVPlayer *player, AVPlaybackSpeed *speed);

/**
 * @brief 设置player音频流类型。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param streamUsage player音频流设置的类型{@link OH_AudioStream_Usage}。
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者streamUsage值无效。
 * @since 12
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetAudioRendererInfo(OH_AVPlayer *player, OH_AudioStream_Usage streamUsage);

/**
 * @brief 设置player音频流音量模式。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param volumeMode 要设置的音频流音量模式{@link OH_AudioStream_VolumeMode}。
 * @return 函数结果代码：\n
 *     {@link AV_ERR_OK} 如果成功。
 *     {@link AV_ERR_INVALID_VAL} 如果输入player为空指针或者volumeMode值无效。
 *     {@link AV_ERR_INVALID_STATE} 函数在无效状态下调用，应先处于准备状态。
 *     {@link AV_ERR_SERVICE_DIED} 系统错误。
 * @since 18
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetVolumeMode(OH_AVPlayer *player, OH_AudioStream_VolumeMode volumeMode);

/**
 * @brief 设置player音频流的打断模式。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param interruptMode player音频流使用的打断模式{@link OH_AudioStream_Usage}。
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者interruptMode值无效。
 * @since 12
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetAudioInterruptMode(OH_AVPlayer *player, OH_AudioInterrupt_Mode interruptMode);

/**
 * @brief 设置player音频流的音效模式。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param interruptMode player音频流使用的音效模式{@link OH_AudioStream_AudioEffectMode}。
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者effectMode值无效。
 * @since 12
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetAudioEffectMode(OH_AVPlayer *player, OH_AudioStream_AudioEffectMode effectMode);

/**
 * @brief 设置hls播放器使用的码率
 *
 * 播放比特率，以比特/秒为单位，以比特/秒为单位。
 * 仅对HLS协议网络流有效。默认情况下，
 * 播放器会根据网络连接情况选择合适的码率和速度。
 * 通过INFO_TYPE_BITRATE_COLLECT上报有效码率链表
 * 设置并选择指定的码率，选择小于和最接近的码率
 * 到指定的码率播放。准备好后，读取它以查询当前选择的比特率。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param bitRate码率，单位为bps
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果设置码率成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player SelectBitRate执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SelectBitRate(OH_AVPlayer *player, uint32_t bitRate);

/**
 * @brief 设置播放画面窗口.
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param window指向OHNativeWindow实例的指针，参见{@link OHNativeWindow}
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果设置播放画面窗口成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针，输入window为空指针或者player SetVideoSurface执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode  OH_AVPlayer_SetVideoSurface(OH_AVPlayer *player, OHNativeWindow *window);

/**
 * @brief 获取媒体文件的总时长，精确到毫秒
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param duration媒体文件的总时长
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果获取当前时长；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player GetDuration执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetDuration(OH_AVPlayer *player, int32_t *duration);

/**
 * @brief 获取当前播放状态
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param state 当前播放状态
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果获取当前播放状态；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player GetState执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetState(OH_AVPlayer *player, AVPlayerState *state);

/**
 * @brief 判断播放器是否在播放
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果正在播放，则返回true；如果不在播放或者输入player为空指针则返回false。
 * @since 11
 * @version 1.0
 */
bool OH_AVPlayer_IsPlaying(OH_AVPlayer *player);

/**
 * @brief 判断是用循环播放
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果循环播放，则返回true；如果不是循环播放或者输入player为空指针则返回false。
 * @since 11
 * @version 1.0
 */
bool OH_AVPlayer_IsLooping(OH_AVPlayer *player);

/**
 * @brief 设置循环播放
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param loop 循环播放开关
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果设置了循环；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player SetLooping执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetLooping(OH_AVPlayer *player, bool loop);

/**
 * @brief 设置播放器回调方法
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param callback 回调对象指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果设置了播放器回调；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针，callback.onInfo或onError为空或者player SetPlayerCallback执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetPlayerCallback(OH_AVPlayer *player, AVPlayerCallback callback);

/**
 * @brief 选择音频或字幕轨道
 *
 * 默认播放第一个带数据的音频流，不播放字幕轨迹。
 * 设置生效后，原曲目将失效。请设置字幕
 * 处于准备/播放/暂停/完成状态，并将音轨设置为准备状态。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param index 索引
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功选择；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player SelectTrack执行失败。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_SelectTrack(OH_AVPlayer *player, int32_t index);

/**
 * @brief 取消选择当前音频或字幕轨道
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param index 索引
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player DeselectTrack执行失败。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_DeselectTrack(OH_AVPlayer *player, int32_t index);

/**
 * @brief 获取当前有效的轨道索引
 *
 * 请将其设置为准备/正在播放/暂停/完成状态
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param trackType 媒体类型
 * @param index 索引
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player GetCurrentTrack执行失败。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetCurrentTrack(OH_AVPlayer *player, int32_t trackType, int32_t *index);

/**
 * @brief 设置播放器媒体密钥系统信息回调的方法
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param callback 对象指针
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针，callback为空指针，player SetDrmSystemInfoCallback，
 * SetDrmSystemInfoCallback或SetDrmSystemInfoCallback执行失败。
 * @since 12
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetMediaKeySystemInfoCallback(OH_AVPlayer *player,
    Player_MediaKeySystemInfoCallback callback);

/**
 * @brief 获取媒体密钥系统信息以创建媒体密钥会话
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param mediaKeySystemInfo 媒体密钥系统信息
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者内存不足。
 * @since 12
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetMediaKeySystemInfo(OH_AVPlayer *player, DRM_MediaKeySystemInfo *mediaKeySystemInfo);

/**
 * @brief 设置解密信息
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param mediaKeySession 具有解密功能的媒体密钥会话实例
 * @param secureVideoPath 是否需要安全解码器
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK}如果成功；\n
 *         {@link AV_ERR_INVALID_VAL}如果输入player为空指针或者player SetDecryptionConfig执行失败。
 * @since 12
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_SetDecryptionConfig(OH_AVPlayer *player, MediaKeySession *mediaKeySession,
    bool secureVideoPath);

/**
 * @brief 设置播放器消息回调监听函数。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param callback 执行回调监听函数的指针, 空指针表示取消设置播放器消息回调监听。
 * @param userData 指向应用调用者设置的实例的指针。
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK} 如果成功；
 *         {@link AV_ERR_NO_MEMORY} 如果输入分配内存失败；
 *         {@link AV_ERR_INVALID_VAL} 如果输入player为空指针或者函数执行失败。
 * @since 12
 */
OH_AVErrCode OH_AVPlayer_SetOnInfoCallback(OH_AVPlayer *player, OH_AVPlayerOnInfoCallback callback, void *userData);

/**
 * @brief 设置播放器错误回调监听函数。
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player 指向OH_AVPlayer实例的指针。
 * @param callback 执行回调监听函数的指针, 空指针表示取消设置播放器错误回调监听。
 * @param userData 指向应用调用者设置的实例的指针。
 * @return 函数结果代码：\n
 *         {@link AV_ERR_OK} 如果成功；
 *         {@link AV_ERR_NO_MEMORY} 如果输入分配内存失败；
 *         {@link AV_ERR_INVALID_VAL} 如果输入player为空指针或者函数执行失败。
 * @since 12
 */
OH_AVErrCode OH_AVPlayer_SetOnErrorCallback(OH_AVPlayer *player, OH_AVPlayerOnErrorCallback callback, void *userData);

#ifdef __cplusplus
}
#endif

#endif // MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_H
