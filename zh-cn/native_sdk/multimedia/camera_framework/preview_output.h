/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file preview_output.h
 *
 * @brief 声明预览输出概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_PREVIEWOUTPUT_H
#define NATIVE_INCLUDE_CAMERA_PREVIEWOUTPUT_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 预览输出对象
 *
 * 可以使用{@link OH_CameraManager_CreatePreviewOutput}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_PreviewOutput Camera_PreviewOutput;

/**
 * @brief 在{@link PreviewOutput_Callbacks}中被调用的预览输出帧开始回调。
 *
 * @param previewOutput 传递回调的{@link Camera_PreviewOutput}。
 * @since 11
 */
typedef void (*OH_PreviewOutput_OnFrameStart)(Camera_PreviewOutput* previewOutput);

/**
 * @brief 在{@link PreviewOutput_Callbacks}中被调用的预览输出帧结束回调。
 *
 * @param previewOutput 传递回调的{@link Camera_PreviewOutput}。
 * @param frameCount 回调传递的帧计数。
 * @since 11
 */
typedef void (*OH_PreviewOutput_OnFrameEnd)(Camera_PreviewOutput* previewOutput, int32_t frameCount);

/**
 * @brief 在{@link PreviewOutput_Callbacks}中被调用的预览输出帧错误回调。
 *
 * @param previewOutput 传递回调的{@link Camera_PreviewOutput}。
 * @param errorCode 预览输出的{@link Camera_ErrorCode}。
 *
 * @see CAMERA_SERVICE_FATAL_ERROR
 * @since 11
 */
typedef void (*OH_PreviewOutput_OnError)(Camera_PreviewOutput* previewOutput, Camera_ErrorCode errorCode);

/**
 * @brief 用于预览输出的回调。
 *
 * @see OH_PreviewOutput_RegisterCallback
 * @since 11
 * @version 1.0
 */
typedef struct PreviewOutput_Callbacks {
    /**
     * 预览输出帧开始事件。
     */
    OH_PreviewOutput_OnFrameStart onFrameStart;

    /**
     * 预览输出帧结束事件。
     */
    OH_PreviewOutput_OnFrameEnd onFrameEnd;

    /**
     * 预览输出错误事件。
     */
    OH_PreviewOutput_OnError onError;
} PreviewOutput_Callbacks;

/**
 * @brief 注册预览输出更改事件回调。
 *
 * @param previewOutput {@link Camera_PreviewOutput}实例。
 * @param callback 要注册的{@link PreviewOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_PreviewOutput_RegisterCallback(Camera_PreviewOutput* previewOutput,
    PreviewOutput_Callbacks* callback);

/**
 * @brief 注销预览输出更改事件回调。
 *
 * @param previewOutput {@link Camera_PreviewOutput}实例。
 * @param callback 要注销的{@link PreviewOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_PreviewOutput_UnregisterCallback(Camera_PreviewOutput* previewOutput,
    PreviewOutput_Callbacks* callback);

/**
 * @brief 开始预览输出。
 *
 * @param previewOutput 要启动的{@link Camera_PreviewOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_CONFIG}如果捕获会话未配置。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PreviewOutput_Start(Camera_PreviewOutput* previewOutput);

/**
 * @brief 停止预览输出。
 *
 * @param previewOutput 要停止的{@link Camera_PreviewOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PreviewOutput_Stop(Camera_PreviewOutput* previewOutput);

/**
 * @brief 释放预览输出。
 *
 * @param previewOutput 要释放的{@link Camera_PreviewOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PreviewOutput_Release(Camera_PreviewOutput* previewOutput);

/**
 * @brief 获取当前预览输出配置文件。
 *
 * @param previewOutput 提供当前预览输出配置文件的{@link Camera_PreviewOutput}实例。
 * @param profile 如果方法调用成功，则将记录当前的{@link Camera_Profile}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_PreviewOutput_GetActiveProfile(Camera_PreviewOutput* previewOutput, Camera_Profile** profile);

/**
 * @brief 删除预览配置文件实例。
 *
 * @param profile 要被删除的{@link Camera_Profile}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PreviewOutput_DeleteProfile(Camera_Profile* profile);

/**
 * @brief 获取支持的预览输出帧率列表。
 *
 * @param previewOutput 传递支持的帧率列表的{@link Camera_PreviewOutput}实例。
 * @param frameRateRange 如果方法调用成功，则将记录支持的{@link Camera_FrameRateRange}列表。
 * @param size 如果方法调用成功，则将记录支持的{@link Camera_FrameRateRange}列表大小。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_PreviewOutput_GetSupportedFrameRates(Camera_PreviewOutput* previewOutput,
    Camera_FrameRateRange** frameRateRange, uint32_t* size);

/**
 * @brief 删除帧率列表。
 *
 * @param previewOutput {@link Camera_PreviewOutput}实例。
 * @param frameRateRange 要删除的{@link Camera_FrameRateRange}列表。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PreviewOutput_DeleteFrameRates(Camera_PreviewOutput* previewOutput,
    Camera_FrameRateRange* frameRateRange);

/**
 * @brief 设置预览输出帧率。
 *
 * @param previewOutput 要设置帧率的{@link Camera_PreviewOutput}实例。
 * @param minFps 要设置的最小值。
 * @param maxFps 要设置的最大值。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PreviewOutput_SetFrameRate(Camera_PreviewOutput* previewOutput,
    int32_t minFps, int32_t maxFps);

/**
 * @brief 获取当前预览输出帧率。
 *
 * @param previewOutput 传递当前预览输出帧率的{@link Camera_PreviewOutput}实例。
 * @param frameRateRange 如果方法调用成功，则将记录当前的{@link Camera_FrameRateRange}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_PreviewOutput_GetActiveFrameRate(Camera_PreviewOutput* previewOutput,
    Camera_FrameRateRange* frameRateRange);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_PREVIEWOUTPUT_H
/** @} */