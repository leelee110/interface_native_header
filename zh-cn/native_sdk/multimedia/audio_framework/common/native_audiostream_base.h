/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAudio
 * @{
 *
 * @brief 提供音频模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 *
 * @since 10
 * @version 1.0
 */

/**
 * @file native_audiostream_base.h
 *
 * @brief 声明OHAudio基础的数据结构。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 * @version 1.0
 */

#ifndef NATIVE_AUDIOSTREAM_BASE_H
#define NATIVE_AUDIOSTREAM_BASE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 音频错误码。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 操作成功
     */
    AUDIOSTREAM_SUCCESS = 0,

    /**
     * 入参错误。
     */
    AUDIOSTREAM_ERROR_INVALID_PARAM = 1,

    /**
     * 非法状态。
     */
    AUDIOSTREAM_ERROR_ILLEGAL_STATE = 2,

    /**
     * 系统通用错误。
     */
    AUDIOSTREAM_ERROR_SYSTEM = 3
} OH_AudioStream_Result;

/**
 * @brief 音频流类型。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 该类型代表音频流是输出流。
     */
    AUDIOSTREAM_TYPE_RENDERER = 1,

    /**
     * 该类型代表音频流是输入流。
     */
    AUDIOSTREAM_TYPE_CAPTURER = 2
} OH_AudioStream_Type;

/**
 * @brief 定义音频流采样格式。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * Unsigned 8位。
     */
    AUDIOSTREAM_SAMPLE_U8 = 0,
    /**
     * Short 16位小端。
     */
    AUDIOSTREAM_SAMPLE_S16LE = 1,
    /**
     * Short 24位小端。
     */
    AUDIOSTREAM_SAMPLE_S24LE = 2,
    /**
     * Short 32位小端。
     */
    AUDIOSTREAM_SAMPLE_S32LE = 3,
} OH_AudioStream_SampleFormat;

/**
 * @brief 定义音频流编码类型。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * PCM编码。
     */
    AUDIOSTREAM_ENCODING_TYPE_RAW = 0,
    /**
     * AudioVivid编码。
     *
     * @since 12
     */
    AUDIOSTREAM_ENCODING_TYPE_AUDIOVIVID = 1,
} OH_AudioStream_EncodingType;

/**
 * @brief 定义音频流使用场景。
 *
 * 通常用来描述音频输出流的使用场景。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 未知类型。
     */
    AUDIOSTREAM_USAGE_UNKNOWN = 0,
    /**
     * 音乐。
     */
    AUDIOSTREAM_USAGE_MUSIC = 1,
    /**
     * VoIP语音通话。
     */
    AUDIOSTREAM_USAGE_COMMUNICATION = 2,
    /**
     * 语音播报。
     */
    AUDIOSTREAM_USAGE_VOICE_ASSISTANT = 3,
    /**
     * 闹钟。
     */
    AUDIOSTREAM_USAGE_ALARM = 4,
    /**
     * 语音消息。
     */
    AUDIOSTREAM_USAGE_VOICE_MESSAGE = 5,
    /**
     * 铃声。
     */
    AUDIOSTREAM_USAGE_RINGTONE = 6,
    /**
     * 通知。
     */
    AUDIOSTREAM_USAGE_NOTIFICATION = 7,
    /**
     * 无障碍。
     */
    AUDIOSTREAM_USAGE_ACCESSIBILITY = 8,
    /**
     * 电影或视频。
     */
    AUDIOSTREAM_USAGE_MOVIE = 10,
    /**
     * 游戏。
     */
    AUDIOSTREAM_USAGE_GAME = 11,
    /**
     * 有声读物（包括听书、相声、评书）、听新闻、播客等。
     */
    AUDIOSTREAM_USAGE_AUDIOBOOK = 12,
    /**
     * 导航。
     */
    AUDIOSTREAM_USAGE_NAVIGATION = 13
    /**
     * VoIP视频通话。
     * @since 12
     */
    AUDIOSTREAM_USAGE_VIDEO_COMMUNICATION = 17
} OH_AudioStream_Usage;

/**
 * @brief 定义音频时延模式。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 该模式代表一个普通时延的音频流。
     */
    AUDIOSTREAM_LATENCY_MODE_NORMAL = 0,
    /**
     * 该模式代表一个低时延的音频流。
     */
    AUDIOSTREAM_LATENCY_MODE_FAST = 1
} OH_AudioStream_LatencyMode;

/**
 * @brief 定义音频流音量模式。
 *
 * @since 18
 */
typedef enum {
    /**
     * 系统级音量（默认模式）。
     *
     * @since 18
     */
    AUDIOSTREAM_VOLUMEMODE_SYSTEM_GLOBAL = 0,
    /**
     * 应用级音量（设置为该模式后可以通过提供的接口设置、查询应用音量）。
     *
     * @since 18
     */
    AUDIOSTREAM_VOLUMEMODE_APP_INDIVIDUAL = 1
} OH_AudioStream_VolumeMode;

/**
 * @brief 定义音频流的状态。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 不合法的状态。
     */
    AUDIOSTREAM_STATE_INVALID = -1,
    /**
     * 新创建时的状态。
     */
    AUDIOSTREAM_STATE_NEW = 0,
    /**
     * 准备状态。
     */
    AUDIOSTREAM_STATE_PREPARED = 1,
    /**
     * 工作状态。
     */
    AUDIOSTREAM_STATE_RUNNING = 2,
    /**
     * 停止状态。
     */
    AUDIOSTREAM_STATE_STOPPED = 3,
    /**
     * 释放状态。
     */
    AUDIOSTREAM_STATE_RELEASED = 4,
    /**
     * 暂停状态。
     */
    AUDIOSTREAM_STATE_PAUSED = 5,
} OH_AudioStream_State;

/**
 * @brief 定义音频流使用场景。
 *
 * 通常用来描述音频输入流的使用场景。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 不合法状态。
     */
    AUDIOSTREAM_SOURCE_TYPE_INVALID = -1,
    /**
     * 录音。
     */
    AUDIOSTREAM_SOURCE_TYPE_MIC = 0,
    /**
     * 语音识别。
     */
    AUDIOSTREAM_SOURCE_TYPE_VOICE_RECOGNITION = 1,
    /**
     * 播放录音。
     */
    AUDIOSTREAM_SOURCE_TYPE_PLAYBACK_CAPTURE = 2,
    /**
     * 通话。
     */
    AUDIOSTREAM_SOURCE_TYPE_VOICE_COMMUNICATION = 7,
    /**
     * 录像
     * 
     * @since 13
     */
    AUDIOSTREAM_SOURCE_TYPE_CAMCORDER = 13
} OH_AudioStream_SourceType;

/**
 * @brief 定义音频事件。
 *
 * 通常用来描述音频事件。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 音频的路由已更改。
     */
    AUDIOSTREAM_EVENT_ROUTING_CHANGED = 0
} OH_AudioStream_Event;

/**
 * @brief 定义音频中断类型。
 *
 * 当用户监听到音频中断时，将获取此信息。
 * 此类型表示本次音频打断的操作是否已由系统强制执行，具体操作信息（如音频暂停、停止等）可通过{@link OH_AudioInterrupt_Hint}获取。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 强制打断类型，即具体操作已由系统强制执行。
     */
    AUDIOSTREAM_INTERRUPT_FORCE = 0,
    /**
     * 共享打断类型，即系统不执行具体操作，通过{@link OH_AudioInterrupt_Hint}提示并建议应用操作，应用可自行决策下一步处理方式。
     */
    AUDIOSTREAM_INTERRUPT_SHAR = 1
} OH_AudioInterrupt_ForceType;

/**
 * @brief 定义音频中断提示类型。
 *
 * 当用户监听到音频中断时，将获取此信息。
 * 此类型表示根据焦点策略，当前需要对音频流的具体操作（如暂停、调整音量等）。
 * 可以结合{@link OH_AudioInterrupt_ForceType}信息，判断该操作是否已由系统强制执行。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef enum {
    /**
     * 不提示。
     */
    AUDIOSTREAM_INTERRUPT_HINT_NONE = 0,
    /**
     * 提示音频恢复，应用可主动触发开始渲染或开始采集的相关操作。
     * 此操作无法由系统强制执行，其对应的{@link OH_AudioInterrupt_ForceType}一定为{@link AUDIOSTREAM_INTERRUPT_SHAR}类型。
     */
    AUDIOSTREAM_INTERRUPT_HINT_RESUME = 1,
    /**
     * 提示音频暂停，暂时失去音频焦点。
     * 后续待焦点可用时，会出现{@link AUDIOSTREAM_INTERRUPT_HINT_RESUME}事件。
     */
    AUDIOSTREAM_INTERRUPT_HINT_PAUSE = 2,
    /**
     * 提示音频停止，彻底失去音频焦点。
     */
    AUDIOSTREAM_INTERRUPT_HINT_STOP = 3,
    /**
     * 提示音频躲避开始，音频降低音量播放，而不会停止。
     */
    AUDIOSTREAM_INTERRUPT_HINT_DUCK = 4,
    /**
     * 提示音量躲避结束，音频恢复正常音量。
     */
    AUDIOSTREAM_INTERRUPT_HINT_UNDUCK = 5
} OH_AudioInterrupt_Hint;

/**
 * @brief 定义音频中断模式。
 *
 * 通常用来设置音频中断模式。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 */
typedef enum {
    /**
     * 共享模式。
     * @since 12
     */
    AUDIOSTREAM_INTERRUPT_MODE_SHARE = 0,
    /**
     * 独立模式。
     * @since 12
     */
    AUDIOSTREAM_INTERRUPT_MODE_INDEPENDENT = 1
} OH_AudioInterrupt_Mode;

/**
 * @brief 定义音效模式。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 */
typedef enum {
    /**
     * 无音效模式。
     */
    EFFECT_NONE = 0,
    /**
     * 默认音效模式。
     */
    EFFECT_DEFAULT = 1,
} OH_AudioStream_AudioEffectMode;

/**
 * @brief 声明音频流的构造器。
 *
 * 构造器实例通常被用来设置音频流属性和创建音频流。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioStreamBuilderStruct OH_AudioStreamBuilder;

/**
 * @brief 声明输出音频流。
 *
 * 输出音频流的实例被用来播放音频数据。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioRendererStruct OH_AudioRenderer;

/**
 * @brief 声明输入音频流。
 *
 * 输入音频流的实例被用来获取音频数据。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioCapturerStruct OH_AudioCapturer;

/**
 * @brief 声明输出音频流的回调函数指针。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioRenderer_Callbacks_Struct {
    /**
     * @brief 这个函数指针将指向用于写入音频数据的回调函数。
     *
     * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param buffer 指向播放数据存储区域，用于应用填充播放数据。
     * @param length buffer的长度。
     */
    int32_t (*OH_AudioRenderer_OnWriteData)(
            OH_AudioRenderer* renderer,
            void* userData,
            void* buffer,
            int32_t length);

    /**
     * @brief 这个函数指针将指向用于处理音频播放流事件的回调函数。
     *
     * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param event 音频事件{@link OH_AudioStream_Event}。
     */
    int32_t (*OH_AudioRenderer_OnStreamEvent)(
            OH_AudioRenderer* renderer,
            void* userData,
            OH_AudioStream_Event event);

    /**
     * @brief 这个函数指针将指向用于处理音频播放中断事件的回调函数。
     *
     * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param type 音频中断类型{@link OH_AudioInterrupt_ForceType}。
     * @param hint 音频中断提示类型{@link OH_AudioInterrupt_Hint}。
     */
    int32_t (*OH_AudioRenderer_OnInterruptEvent)(
            OH_AudioRenderer* renderer,
            void* userData,
            OH_AudioInterrupt_ForceType type,
            OH_AudioInterrupt_Hint hint);

    /**
     * @brief 这个函数指针将指向用于处理音频播放错误结果的回调函数。
     *
     * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param error 音频播放错误结果{@link AUDIOSTREAM_ERROR_INVALID_PARAM} 或者
     * {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 或者 {@link AUDIOSTREAM_ERROR_SYSTEM}。
     * @param length buffer的长度{@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 或者 {@link AUDIOSTREAM_ERROR_SYSTEM}。
     */
    int32_t (*OH_AudioRenderer_OnError)(
            OH_AudioRenderer* renderer,
            void* userData,
            OH_AudioStream_Result error);
} OH_AudioRenderer_Callbacks;

/**
 * @brief 声明输入音频流的回调函数指针。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 10
 */
typedef struct OH_AudioCapturer_Callbacks_Struct {
    /**
     * @brief 这个函数指针将指向用于读取音频数据的回调函数。
     *
     * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param buffer 指向播放数据存储区域，用于应用填充播放数据。
     * @param length buffer的长度。
     */
    int32_t (*OH_AudioCapturer_OnReadData)(
            OH_AudioCapturer* capturer,
            void* userData,
            void* buffer,
            int32_t length);

    /**
     * @brief 这个函数指针将指向用于处理音频录制流事件的回调函数。
     *
     * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param event 音频事件{@link OH_AudioStream_Event}。
     */
    int32_t (*OH_AudioCapturer_OnStreamEvent)(
            OH_AudioCapturer* capturer,
            void* userData,
            OH_AudioStream_Event event);

    /**
     * @brief 这个函数指针将指向用于处理音频录制中断事件的回调函数。
     *
     * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param type 音频中断类型{@link OH_AudioInterrupt_ForceType}。
     * @param hint 音频中断提示类型{@link OH_AudioInterrupt_Hint}。
     */
    int32_t (*OH_AudioCapturer_OnInterruptEvent)(
            OH_AudioCapturer* capturer,
            void* userData,
            OH_AudioInterrupt_ForceType type,
            OH_AudioInterrupt_Hint hint);

    /**
     * @brief 这个函数指针将指向用于处理音频录制错误结果的回调函数。
     *
     * @param capturer 指向{@link OH_AudioStreamBuilder_GenerateCapturer}创建的音频流实例。
     * @param userData 指向应用自定义的数据存储区域。
     * @param error 音频录制错误结果{@link AUDIOSTREAM_ERROR_INVALID_PARAM} 或者
     * {@link AUDIOSTREAM_ERROR_ILLEGAL_STATE} 或者 {@link AUDIOSTREAM_ERROR_SYSTEM}。
     */
    int32_t (*OH_AudioCapturer_OnError)(
            OH_AudioCapturer* capturer,
            void* userData,
            OH_AudioStream_Result error);
} OH_AudioCapturer_Callbacks;

/**
 * @brief 流设备变更原因。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 11
 */
typedef enum {
    /**
     * 未知原因。
     */
    REASON_UNKNOWN = 0,
    /**
     * 新设备可用。
     */
    REASON_NEW_DEVICE_AVAILABLE = 1,
    /**
     * 旧设备不可用。当报告此原因时，应用程序应考虑暂停音频播放。
     */
    REASON_OLD_DEVICE_UNAVAILABLE = 2,
    /**
     * 用户或系统强制选择切换。
     */
    REASON_OVERRODE = 3
} OH_AudioStream_DeviceChangeReason;

/**
 * @brief 输出音频流设备变更的回调函数。
 *
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @param reason 流设备变更原因。
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 11
 */
typedef void (*OH_AudioRenderer_OutputDeviceChangeCallback)(OH_AudioRenderer* renderer, void* userData,
    OH_AudioStream_DeviceChangeReason reason);

/**
 * @brief 到达标记位置时回调。
 *
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param samplePos 设置目标标记位置。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @since 12
 */
typedef void (*OH_AudioRenderer_OnMarkReachedCallback)(OH_AudioRenderer* renderer, uint32_t samplePos, void* userData);

/**
 * @brief 这个函数指针将指向用于同时写入音频数据和元数据的回调函数。
 *
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @param audioData 指向用户写入的音频数据的指针。
 * @param audioDataSize 用户写入的音频数据的数据长度，以字节为单位。
 * @param metadata 指向用户写入的元数据的指针。
 * @param metadataSize 用户写入的元数据的数据长度，以字节为单位。
 * @return 用户返回的回调函数的错误码。
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 */
typedef int32_t (*OH_AudioRenderer_WriteDataWithMetadataCallback)(OH_AudioRenderer* renderer,
    void* userData, void* audioData, int32_t audioDataSize, void* metadata, int32_t metadataSize);

/**
 * @brief 用于标识对应播放音频流是否支持被其他应用录制。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 */
typedef enum {
    /**
     * 表示音频流可以被其他应用录制。
     */
    AUDIO_STREAM_PRIVACY_TYPE_PUBLIC = 0,
    /**
     * 表示音频流不可以被其他应用录制。
     */
    AUDIO_STREAM_PRIVACY_TYPE_PRIVATE = 1
} OH_AudioStream_PrivacyType;

/**
 * @brief 定义音频数据回调结果。
 *
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 */
typedef enum {
    /** 表示音频数据回调结果无效，且音频数据不播放。 */
    AUDIO_DATA_CALLBACK_RESULT_INVALID = -1,
    /** 表示音频数据回调结果有效，将播放音频数据。 */
    AUDIO_DATA_CALLBACK_RESULT_VALID = 0
} OH_AudioData_Callback_Result;

/**
 * @brief 这个函数指针将指向用于写入音频数据的回调函数。
 *
 * 这个函数类似于 OH_AudioRenderer_Callbacks_Struct.OH_AudioRenderer_OnWriteData 函数指针。但具有返回值，用于标识音频数据回调结果。
 * 该函数的返回结果表示填充到缓冲区的数据是否有效。如果结果无效，用户填写的数据将不被播放。
 *
 * @param renderer 指向{@link OH_AudioStreamBuilder_GenerateRenderer}创建的音频流实例。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @param audioData 指向用户写入的音频数据的指针。
 * @param audioDataSize 用户写入的音频数据的数据长度，以字节为单位。
 * @return 函数返回值：
 *         {@link AUDIO_DATA_CALLBACK_RESULT_INVALID} 音频数据回调结果无效，且音频数据不播放。
 *         {@link AUDIO_DATA_CALLBACK_RESULT_VALID} 音频数据回调结果有效，将播放音频数据。
 * @see OH_AudioRenderer_Callbacks_Struct.OH_AudioRenderer_OnWriteData
 * @syscap SystemCapability.Multimedia.Audio.Core
 * @since 12
 */
typedef OH_AudioData_Callback_Result (*OH_AudioRenderer_OnWriteDataCallback)(OH_AudioRenderer* renderer, void* userData,
    void* audioData, int32_t audioDataSize);
#ifdef __cplusplus
}
#endif

#endif // NATIVE_AUDIOSTREAM_BASE_H
/** @} */