/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Core
 * @{
 *
 * @brief Core模块提供用于媒体框架的基础骨干能力，包含内存、错误码、媒体数据结构等相关函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @since 9
 */

/**
 * @file native_averrors.h
 *
 * @brief 媒体框架错误码。
 * 
 * @library libnative_media_core.so
 * @syscap SystemCapability.Multimedia.Media.Core
 * @since 9
 */

#ifndef NATIVE_AVERRORS_H
#define NATIVE_AVERRORS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 媒体框架错误码。
 * @syscap SystemCapability.Multimedia.Media.Core
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVErrCode {
    /**
     * 操作成功。
     */
    AV_ERR_OK = 0,
    /**
     * 无内存。
     */
    AV_ERR_NO_MEMORY = 1,
    /**
     * 操作不允许。
     */
    AV_ERR_OPERATE_NOT_PERMIT = 2,
    /**
     * 无效值。
     */
    AV_ERR_INVALID_VAL = 3,
    /**
     * IO错误。
     */
    AV_ERR_IO = 4,
    /**
     * 超时错误。
     */
    AV_ERR_TIMEOUT = 5,
    /**
     * 未知错误。
     */
    AV_ERR_UNKNOWN = 6,
    /**
     * 服务死亡。
     */
    AV_ERR_SERVICE_DIED = 7,
    /**
     * 当前状态不支持此操作。
     */
    AV_ERR_INVALID_STATE = 8,
    /**
     * 未支持的接口。
     */
    AV_ERR_UNSUPPORT = 9,
    /**
     * @error 不支持的格式。
     * @since 18
     */
    AV_ERR_UNSUPPORTED_FORMAT = 11,
    /**
     * 扩展错误码初始值。
     */
    AV_ERR_EXTEND_START = 100,
	/** DRM起始错误码。
     * @since 12
     */
    AV_ERR_DRM_BASE = 200,
    /** DRM解密失败。
     * @since 12
     */
    AV_ERR_DRM_DECRYPT_FAILED = 201,
    /**
     * 视频起始错误码。
     * @since 12
     */
    AV_ERR_VIDEO_BASE = 300,
    /**
     * 视频不支持色彩空间转换。
     * @since 12
     */
    AV_ERR_VIDEO_UNSUPPORTED_COLOR_SPACE_CONVERSION = 301,
} OH_AVErrCode;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVERRORS_H
/** @} */
