/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Image_NativeModule
 * @{
 *
 * @brief 提供从native层获取图片数据的方法。
 *
 * @since 12
 */

/**
 * @file image_receiver_native.h
 *
 * @brief 声明从native层获取图片数据的方法。
 * @library libimage_receiver.so
 * @syscap SystemCapability.Multimedia.Image.ImageReceiver
 * @since 12
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_RECEIVER_NATIVE_H
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_RECEIVER_NATIVE_H

#include "image_native.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义native层OH_ImageReceiverNative对象。
 *
 * @since 12
 */
struct OH_ImageReceiverNative;

/**
 * @brief 用于定义OH_ImageReceiverNative数据类型名称。
 *
 * @since 12
 */
typedef struct OH_ImageReceiverNative OH_ImageReceiverNative;

/**
 * @brief 定义native层OH_ImageReceiverOptions对象。
 *
 * @since 12
 */
struct OH_ImageReceiverOptions;

/**
 * @brief 用于定义OH_ImageReceiverOptions数据类型名称。
 *
 * @since 12
 */
typedef struct OH_ImageReceiverOptions OH_ImageReceiverOptions;

/**
 * @brief 定义native层图片的回调方法。
 *
 * @since 12
 */
typedef void (*OH_ImageReceiver_OnCallback)(OH_ImageReceiverNative *receiver);

/**
 * @brief 创建应用层 OH_ImageReceiverOptions 对象。
 *
 * @param options 表示作为获取结果的 {@link OH_ImageReceiverOptions} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 如果申请内存失败返回 IMAGE_ALLOC_FAILED；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverOptions_Create(OH_ImageReceiverOptions **options);

/**
 * @brief 获取 {@link OH_ImageReceiverOptions} 对象的 {@link Image_Size} 信息。
 *
 * @param options 表示 {@link OH_ImageReceiverOptions} 对象的指针。
 * @param size 表示作为获取结果的 {@link Image_Size} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverOptions_GetSize(OH_ImageReceiverOptions* options, Image_Size* size);

/**
 * @brief 设置 {@link OH_ImageReceiverOptions} 对象的 {@link Image_Size} 信息。
 *
 * @param options 表示 {@link OH_ImageReceiverOptions} 对象的指针。
 * @param size 表示 {@link Image_Size} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverOptions_SetSize(OH_ImageReceiverOptions* options, Image_Size size);

/**
 * @brief 获取 {@link OH_ImageReceiverOptions} 对象的图片缓存容量的信息。
 *
 * @param options 表示 {@link OH_ImageReceiverOptions} 对象的指针。
 * @param capacity 表示作为获取结果的图片缓存容量对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverOptions_GetCapacity(OH_ImageReceiverOptions* options, int32_t* capacity);

/**
 * @brief 设置 {@link OH_ImageReceiverOptions} 对象的图片缓存容量的信息。
 *
 * @param options 表示 {@link OH_ImageReceiverOptions} 对象的指针。
 * @param capacity 表示图片缓存容量对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverOptions_SetCapacity(OH_ImageReceiverOptions* options, int32_t capacity);

/**
 * @brief 释放 {@link OH_ImageReceiverOptions} 对象。
 *
 * @param options 表示 {@link OH_ImageReceiverOptions} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverOptions
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverOptions_Release(OH_ImageReceiverOptions* options);

/**
 * @brief 创建应用层 OH_ImageReceiverNative 对象。
 *
 * @param options 表示 {@link OH_ImageReceiverOptions} 对象的指针。
 * @param receiver 表示作为获取结果的 {@link OH_ImageReceiverNative} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 如果申请内存失败返回 IMAGE_ALLOC_FAILED；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_Create(OH_ImageReceiverOptions* options, OH_ImageReceiverNative** receiver);

/**
 * @brief 通过{@link OH_ImageReceiverNative}获取receiver的id。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @param surfaceId 表示作为获取结果的id对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 如果未知原因错误返回 IMAGE_UNKNOWN_ERROR；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_GetReceivingSurfaceId(OH_ImageReceiverNative* receiver, uint64_t* surfaceId);

/**
 * @brief 通过{@link OH_ImageReceiverNative}获取最新的一张图片。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @param image 获取到的应用层的 OH_ImageNative 指针对象。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 如果未知原因错误返回 IMAGE_UNKNOWN_ERROR；
 * 如果申请内存失败返回 IMAGE_ALLOC_FAILED；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative, OH_ImageNative
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_ReadLatestImage(OH_ImageReceiverNative* receiver, OH_ImageNative** image);

/**
 * @brief 通过{@link OH_ImageReceiverNative}获取下一张图片。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @param image 获取到的应用层的 OH_ImageNative 指针对象。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 如果未知原因错误返回 IMAGE_UNKNOWN_ERROR；
 * 如果申请内存失败返回 IMAGE_ALLOC_FAILED；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative, OH_ImageNative
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_ReadNextImage(OH_ImageReceiverNative* receiver, OH_ImageNative** image);

/**
 * @brief 注册一个{@link OH_ImageReceiver_OnCallback}回调事件。
 *
 * 每当接收到新的图片，该回调事件就会响应。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @param callback 表示 {@link OH_ImageReceiver_OnCallback} 事件的回调函数。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative, OH_ImageReceiver_OnCallback
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_On(OH_ImageReceiverNative* receiver, OH_ImageReceiver_OnCallback callback);

/**
 * @brief 关闭{@link OH_ImageReceiver_OnCallback}回调事件。
 *
 * 关闭被 {@link OH_ImageReceiverNative_On} 开启的回调事件。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative, OH_ImageReceiverNative_On
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_Off(OH_ImageReceiverNative* receiver);

/**
 * @brief 通过{@link OH_ImageReceiverNative}获取ImageReceiver的大小。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @param size 表示作为获取结果的 {@link Image_Size} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative, Image_Size
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_GetSize(OH_ImageReceiverNative* receiver, Image_Size* size);

/**
 * @brief 通过{@link OH_ImageReceiverNative}获取ImageReceiver的容量。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @param capacity 表示作为获取结果的图片缓存容量对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_GetCapacity(OH_ImageReceiverNative* receiver, int32_t* capacity);

/**
 * @brief 释放native {@link OH_ImageReceiverNative} 对象。
 *
 * @param receiver 表示 {@link OH_ImageReceiverNative} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @see OH_ImageReceiverNative
 * @since 12
 */
Image_ErrorCode OH_ImageReceiverNative_Release(OH_ImageReceiverNative* receiver);

#ifdef __cplusplus
};
#endif
/** @} */

#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_RECEIVER_NATIVE_H
