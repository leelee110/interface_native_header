/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 * @since 10
 */

/**
 * @file mutex.h
 *
 * @brief 声明mutex的C接口。
 *
 * @library libffrt.z.so
 * @kit FunctionFlowRuntimeKit
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 * @since 10
 * @version 1.0
 */

#ifndef FFRT_API_C_MUTEX_H
#define FFRT_API_C_MUTEX_H

#include "type_def.h"

/**
 * @brief  初始化mutex属性。
 *
 * @param attr mutex属性指针。
 * @return mutex属性初始化成功返回ffrt_success，
           mutex属性初始化失败返回ffrt_error_inval。
 * @since 12
 * @version 1.0
 */
FFRT_C_API int ffrt_mutexattr_init(ffrt_mutexattr_t* attr);

/**
 * @brief 设置mutex属性类型。
 *
 * @param attr mutex属性指针。
 * @param type mutex类型。
 * @return mutex属性类型设置成功返回ffrt_success，
           mutex属性指针是空或者，
           mutex类型不是ffrt_mutex_normal或ffrt_mutex_recursive返回ffrt_error_inval。
 * @since 12
 * @version 1.0
 */
FFRT_C_API int ffrt_mutexattr_settype(ffrt_mutexattr_t* attr, int type);

/**
 * @brief 获取mutex类型。
 *
 * @param attr mutex属性指针。
 * @param type mutex类型指针。
 * @return mutex类型获取成功返回ffrt_success，
           mutex属性指针或mutex类型指针是空返回ffrt_error_inval。
 * @since 12
 * @version 1.0
 */
FFRT_C_API int ffrt_mutexattr_gettype(ffrt_mutexattr_t* attr, int* type);

/**
 * @brief 销毁mutex属性，用户需要调用此接口。
 *
 * @param attr mutex属性指针。
 * @return mutex属性销毁成功返回ffrt_success，
           mutex属性销毁失败返回ffrt_error_inval。
 * @since 12
 * @version 1.0
 */
FFRT_C_API int ffrt_mutexattr_destroy(ffrt_mutexattr_t* attr);

/**
 * @brief 初始化mutex。
 *
 * @param mutex mutex指针。
 * @param attr mutex属性。
 * @return 初始化mutex成功返回ffrt_success，
           初始化mutex失败返回ffrt_error_inval。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_mutex_init(ffrt_mutex_t* mutex, const ffrt_mutexattr_t* attr);

/**
 * @brief 获取mutex。
 *
 * @param mutex mutex指针。
 * @return 获取mutex成功返回ffrt_success，
           获取mutex失败返回ffrt_error_inval或者阻塞当前任务。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_mutex_lock(ffrt_mutex_t* mutex);

/**
 * @brief 释放mutex。
 *
 * @param mutex mutex指针。
 * @return 释放mutex成功返回ffrt_success，
           释放mutex失败返回ffrt_error_inval。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_mutex_unlock(ffrt_mutex_t* mutex);

/**
 * @brief 尝试获取mutex。
 *
 * @param mutex mutex指针。
 * @return 获取mutex成功返回ffrt_success，
           获取mutex失败返回ffrt_error_inval或ffrt_error_busy。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_mutex_trylock(ffrt_mutex_t* mutex);

/**
 * @brief 销毁mutex。
 *
 * @param mutex mutex指针。
 * @return 销毁mutex成功返回ffrt_success，
           销毁mutex失败返回ffrt_error_inval。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_mutex_destroy(ffrt_mutex_t* mutex);

#endif // FFRT_API_C_MUTEX_H
/** @} */
