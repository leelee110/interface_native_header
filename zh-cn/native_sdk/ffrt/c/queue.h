/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 * @since 10
 */

/**
 * @file queue.h
 *
 * @brief 声明队列的C接口。
 *
 * @library libffrt.z.so
 * @kit FunctionFlowRuntimeKit
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 * @since 10
 * @version 1.0
 */

#ifndef FFRT_API_C_QUEUE_H
#define FFRT_API_C_QUEUE_H

#include "type_def.h"

/**
 * @brief 队列类型。
 *
 * @since 12
 */
typedef enum {
    /** 串行队列 */
    ffrt_queue_serial,
    /** 并行队列 */
    ffrt_queue_concurrent,
    /** 无效队列类型 */
    ffrt_queue_max
} ffrt_queue_type_t;

/**
 * @brief 队列句柄。
 *
 * @since 10
 */
typedef void* ffrt_queue_t;

/**
 * @brief 初始化队列属性。
 *
 * @param attr 队列属性指针。
 * @return 执行成功时返回0，
           执行失败时返回-1。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_queue_attr_init(ffrt_queue_attr_t* attr);

/**
 * @brief 销毁队列属性。
 *
 * @param attr 队列属性指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_attr_destroy(ffrt_queue_attr_t* attr);

/**
 * @brief 设置队列QoS属性。
 *
 * @param attr 队列属性指针。
 * @param qos QoS属性值。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_attr_set_qos(ffrt_queue_attr_t* attr, ffrt_qos_t qos);

/**
 * @brief 获取队列QoS属性。
 *
 * @param attr 队列属性指针。
 * @return 返回队列的QoS属性。
 * @since 10
 * @version 1.0
 */
FFRT_C_API ffrt_qos_t ffrt_queue_attr_get_qos(const ffrt_queue_attr_t* attr);

/**
 * @brief 设置串行队列timeout属性。
 *
 * @param attr 串行队列属性指针。
 * @param timeout_us 串行队列任务执行的timeout时间。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_attr_set_timeout(ffrt_queue_attr_t* attr, uint64_t timeout_us);

/**
 * @brief 获取串行队列任务执行的timeout时间。
 *
 * @param attr 串行队列属性指针。
 * @return 返回串行队列任务执行的timeout时间。
 * @since 10
 * @version 1.0
 */
FFRT_C_API uint64_t ffrt_queue_attr_get_timeout(const ffrt_queue_attr_t* attr);

/**
 * @brief 设置串行队列超时回调方法。
 *
 * @param attr 串行队列属性指针。
 * @param f 超时回调方法执行体。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_attr_set_callback(ffrt_queue_attr_t* attr, ffrt_function_header_t* f);

/**
 * @brief 获取串行队列超时回调方法。
 *
 * @param attr 串行队列属性指针。
 * @return 返回串行队列超时回调方法。
 * @since 10
 * @version 1.0
 */
FFRT_C_API ffrt_function_header_t* ffrt_queue_attr_get_callback(const ffrt_queue_attr_t* attr);

/**
 * @brief 设置并行队列最大并发度。
 *
 * @param attr 队列属性指针。
 * @param max_concurrency 最大并发度。
 * @since 12
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_attr_set_max_concurrency(ffrt_queue_attr_t* attr, const int max_concurrency);

/**
 * @brief 获取并行队列最大并发度。
 *
 * @param attr 队列属性指针。
 * @return 返回最大并发度。
 * @since 12
 * @version 1.0
 */
FFRT_C_API int ffrt_queue_attr_get_max_concurrency(const ffrt_queue_attr_t* attr);

/**
 * @brief 创建队列。
 *
 * @param type 队列类型。
 * @param name 队列名字。
 * @param attr 队列属性。
 * @return 创建队列成功返回非空队列句柄，
           创建队列失败返回空指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API ffrt_queue_t ffrt_queue_create(ffrt_queue_type_t type, const char* name, const ffrt_queue_attr_t* attr);

/**
 * @brief 销毁队列。
 *
 * @param queue 队列句柄。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_destroy(ffrt_queue_t queue);

/**
 * @brief 提交一个任务到队列中调度执行。
 *
 * @param queue 队列句柄。
 * @param f 任务的执行体。
 * @param attr 任务属性。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_submit(ffrt_queue_t queue, ffrt_function_header_t* f, const ffrt_task_attr_t* attr);

/**
 * @brief 提交一个任务到队列中调度执行，并返回任务句柄。
 *
 * @param queue 队列句柄。
 * @param f 任务的执行体。
 * @param attr 任务属性。
 * @return 提交成功返回非空任务句柄，
           提交失败返回空指针。
 * @since 10
 * @version 1.0
 */
FFRT_C_API ffrt_task_handle_t ffrt_queue_submit_h(
    ffrt_queue_t queue, ffrt_function_header_t* f, const ffrt_task_attr_t* attr);

/**
 * @brief 等待队列中一个任务执行完成。
 *
 * @param handle 任务句柄。
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_queue_wait(ffrt_task_handle_t handle);

/**
 * @brief 取消队列中一个任务。
 *
 * @param handle 任务句柄。
 * @return 取消任务成功返回0，
           取消任务失败返回-1。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_queue_cancel(ffrt_task_handle_t handle);

/**
 * @brief 获取主线程队列。
 *
 * @return 返回主线程队列句柄。
 * @since 12
 * @version 1.0
 */
FFRT_C_API ffrt_queue_t ffrt_get_main_queue(void);

/**
 * @brief 获取应用Worker(ArkTs)线程队列。
 *
 * @return 返回当前线程队列句柄。
 * @deprecated since 15
 * @since 12
 * @version 1.0
 */
FFRT_C_API ffrt_queue_t ffrt_get_current_queue(void);

#endif // FFRT_API_C_QUEUE_H
/** @} */
