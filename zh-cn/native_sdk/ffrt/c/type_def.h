/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 * @since 10
 */

/**
 * @file type_def.h
 *
 * @brief 定义通用类型。
 *
 * @library libffrt.z.so
 * @kit FunctionFlowRuntimeKit
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 * @since 10
 * @version 1.0
 */

#ifndef FFRT_API_C_TYPE_DEF_H
#define FFRT_API_C_TYPE_DEF_H

#include <stdint.h>
#include <errno.h>

#ifdef __cplusplus
#define FFRT_C_API  extern "C"
#else
#define FFRT_C_API
#endif

/**
 * @brief 任务的优先级类型。
 *
 * @since 12
 */
typedef enum {
    /** immediate 优先级 */
    ffrt_queue_priority_immediate = 0,
    /** high 优先级 */
    ffrt_queue_priority_high,
    /** low 优先级 */
    ffrt_queue_priority_low,
    /** lowest 优先级 */
    ffrt_queue_priority_idle,
} ffrt_queue_priority_t;

/**
 * @brief 任务的QoS类型。
 *
 * @since 10
 */
typedef enum {
    /** 继承当前任务QoS属性 */
    ffrt_qos_inherit = -1,
    /** 后台任务 */
    ffrt_qos_background,
    /** 实时工具 */
    ffrt_qos_utility,
    /** 默认类型 */
    ffrt_qos_default,
    /** 用户期望 */
    ffrt_qos_user_initiated,
} ffrt_qos_default_t;

/**
 * @brief QoS类型。
 *
 * @since 10
 */
typedef int ffrt_qos_t;

/**
 * @brief 任务执行函数指针类型。
 *
 * @since 10
 */
typedef void(*ffrt_function_t)(void*);

/**
 * @brief 任务执行体。
 *
 * @since 10
 */
typedef struct {
    /** 任务执行函数 */
    ffrt_function_t exec;
    /** 任务销毁函数 */
    ffrt_function_t destroy;
    /** 保留位需要设置为0 */
    uint64_t reserve[2];
} ffrt_function_header_t;

/**
 * @brief 多种类型数据结构分配大小定义。
 *
 * @since 10
 */
typedef enum {
    /** 任务属性 */
    ffrt_task_attr_storage_size = 128,
    /** 任务执行体 */
    ffrt_auto_managed_function_storage_size = 64 + sizeof(ffrt_function_header_t),
    /** 互斥锁 */
    ffrt_mutex_storage_size = 64,
    /** 条件变量 */
    ffrt_cond_storage_size = 64,
    /** 队列属性 */
    ffrt_queue_attr_storage_size = 128,
} ffrt_storage_size_t;

/**
 * @brief 任务类型。
 *
 * @since 10
 */
typedef enum {
    /** 通用任务类型 */
    ffrt_function_kind_general,
    /** 队列任务类型 */
    ffrt_function_kind_queue,
} ffrt_function_kind_t;

/**
 * @brief 依赖类型。
 *
 * @since 10
 */
typedef enum {
    /** 数据依赖类型 */
    ffrt_dependence_data,
    /** 任务依赖类型 */
    ffrt_dependence_task,
} ffrt_dependence_type_t;

/**
 * @brief 依赖数据结构。
 *
 * @since 10
 */
typedef struct {
    /** 依赖类型 */
    ffrt_dependence_type_t type;
    /** 依赖数据地址 */
    const void* ptr;
} ffrt_dependence_t;

/**
 * @brief 依赖结构定义。
 *
 * @since 10
 */
typedef struct {
    /** 依赖数量 */
    uint32_t len;
    /** 依赖数据 */
    const ffrt_dependence_t* items;
} ffrt_deps_t;

/**
 * @brief 并行任务属性结构。
 *
 * @since 10
 */
typedef struct {
    /** 任务属性所占空间 */
    uint32_t storage[(ffrt_task_attr_storage_size + sizeof(uint32_t) - 1) / sizeof(uint32_t)];
} ffrt_task_attr_t;

/**
 * @brief 串行队列属性结构。
 *
 * @since 10
 */
typedef struct {
    /** 串行队列属性所占空间 */
    uint32_t storage[(ffrt_queue_attr_storage_size + sizeof(uint32_t) - 1) / sizeof(uint32_t)];
} ffrt_queue_attr_t;

/**
 * @brief 并行任务句柄。
 *
 * @since 10
 */
typedef void* ffrt_task_handle_t;

/**
 * @brief FFRT错误码。
 *
 * @since 10
 */
typedef enum {
    /** 失败 */
    ffrt_error = -1,
    /** 成功 */
    ffrt_success = 0,
    /** 内存不足 */
    ffrt_error_nomem = ENOMEM,
    /** 超时 */
    ffrt_error_timedout = ETIMEDOUT,
    /** 重新尝试 */
    ffrt_error_busy = EBUSY,
    /** 值无效 */
    ffrt_error_inval = EINVAL
} ffrt_error_t;

/**
 * @brief FFRT条件变量属性结构。
 *
 * @since 10
 */
typedef struct {
    /** FFRT条件变量属性所占空间 */
    long storage;
} ffrt_condattr_t;

/**
 * @brief FFRT锁属性结构。
 *
 * @since 10
 */
typedef struct {
    /** FFRT锁属性所占空间 */
    long storage;
} ffrt_mutexattr_t;

/**
 * @brief mutex类型枚举。
 *
 * 描述互斥类型，ffrt_mutex_normal是普通互斥锁；
 * ffrt_mutex_recursive是递归互斥锁，ffrt_mutex_default是普通互斥锁。
 *
 * @since 12
 */
typedef enum {
    /** 普通互斥锁 */
    ffrt_mutex_normal = 0,
    /** 递归互斥锁 */
    ffrt_mutex_recursive = 2,
    /** 默认互斥锁 */
    ffrt_mutex_default = ffrt_mutex_normal
} ffrt_mutex_type;

/**
 * @brief FFRT互斥锁结构。
 *
 * @since 10
 */
typedef struct {
    /** FFRT互斥锁所占空间 */
    uint32_t storage[(ffrt_mutex_storage_size + sizeof(uint32_t) - 1) / sizeof(uint32_t)];
} ffrt_mutex_t;

/**
 * @brief FFRT条件变量结构。
 *
 * @since 10
 */
typedef struct {
    /** FFRT条件变量所占空间 */
    uint32_t storage[(ffrt_cond_storage_size + sizeof(uint32_t) - 1) / sizeof(uint32_t)];
} ffrt_cond_t;

/**
 * @brief poller回调函数定义。
 *
 * @since 12
 */
typedef void (*ffrt_poller_cb)(void* data, uint32_t event);

/**
 * @brief timer回调函数定义。
 *
 * @since 12
 */
typedef void (*ffrt_timer_cb)(void* data);

/**
 * @brief  定时器句柄。
 *
 * @since 12
 */
typedef int ffrt_timer_t;
#ifdef __cplusplus
namespace ffrt {

/**
 * @brief 任务QoS类型枚举。
 *
 * @since 10
 */
enum qos_default {
    /** 继承当前任务的QoS类型 */
    qos_inherit = ffrt_qos_inherit,
    /** 后台任务 */
    qos_background = ffrt_qos_background,
    /** 实时工具 */
    qos_utility = ffrt_qos_utility,
    /** 默认类型 */
    qos_default = ffrt_qos_default,
    /** 用户期望 */
    qos_user_initiated = ffrt_qos_user_initiated,
};

/**
 * @brief QoS类型。
 *
 * @since 10
 */
using qos = int;

}

#endif // __cplusplus
#endif // FFRT_API_C_TYPE_DEF_H
/** @} */