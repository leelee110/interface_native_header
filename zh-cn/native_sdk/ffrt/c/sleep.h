/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 * @since 10
 */

/**
 * @file sleep.h
 *
 * @brief 声明sleep和yield的C接口。
 *
 * @library libffrt.z.so
 * @kit FunctionFlowRuntimeKit
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 * @since 10
 * @version 1.0
 */

#ifndef FFRT_API_C_SLEEP_H
#define FFRT_API_C_SLEEP_H

#include <stdint.h>
#include "type_def.h"

/**
 * @brief 睡眠调用线程固定的时间。
 *
 * @param usec 睡眠时间，单位是微秒。
 * @return 执行成功时返回ffrt_success。
           执行失败时返回ffrt_error。
 * @since 10
 * @version 1.0
 */
FFRT_C_API int ffrt_usleep(uint64_t usec);

/**
 * @brief 当前任务主动放权，让其他任务有机会调度执行。
 *
 * @since 10
 * @version 1.0
 */
FFRT_C_API void ffrt_yield(void);

#endif // FFRT_API_C_SLEEP_H
/** @} */
