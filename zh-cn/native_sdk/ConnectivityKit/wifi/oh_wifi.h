/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Wifi
 * @{
 *
 * @brief 提供用于查询WIFI开关状态的功能。
 * @since 13
 */
/**
 * @file oh_wifi.h
 * @kit ConnectivityKit
 * @brief 定义查询WIFI开关状态的接口。
 * @library libwifi_ndk.so
 * @syscap SystemCapability.Communication.WiFi.STA
 * @since 13
 */

#ifndef OH_WIFI_H
#define OH_WIFI_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义WIFI接口返回值的错误码。
 *
 * @since 13
 */
typedef enum Wifi_ResultCode {
    /**
     * 操作成功。
     */
    WIFI_SUCCESS = 0,
    /**
     * 权限校验失败。
     */
    WIFI_PERMISSION_DENIED = 201,
    /**
     * 参数错误。\n
     * 可能原因：1.输入参数为空指针；2.参数数值超出定义范围。
     */
    WIFI_INVALID_PARAM = 401,
    /**
     * 该功能不支持。由于设备能力有限，无法调用该函数。
     */
    WIFI_NOT_SUPPORTED = 801,
    /**
     * 操作失败。\n
     * 可能原因：服务内部执行失败。
     */
    WIFI_OPERATION_FAILED = 2501000
} Wifi_ResultCode;

/**
 * @brief 查询WIFI开关是否开启。
 *
 * @param enabled - bool类型的指针，用于接收WIFI开关状态值。\n
 * 等于true表示WIFI开关开启，false表示WIFI开关关闭。\n
 * 需要传入非空指针，否则会返回错误。\n
 * @return 返回操作结果，详细定义参见{@link Wifi_ResultCode}.\n
 *     {@link WIFI_SUCCESS} 查询WIFI开关状态成功。\n
 *     {@link WIFI_INVALID_PARAM} 入参是空指针。\n
 *     {@link WIFI_OPERATION_FAILED} 服务内部执行错误。\n
 * @since 13
 */
Wifi_ResultCode OH_Wifi_IsWifiEnabled(bool *enabled);

#ifdef __cplusplus
}
#endif
/** @} */
#endif // OH_WIFI_H