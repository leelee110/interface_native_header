/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup NeuralNetworkRuntime
 * @{
 *
 * @brief 提供Neural Network Runtime加速模型推理的相关接口。
 *
 * @since 9
 * @version 2.0
 */

/**
 * @file neural_network_runtime.h
 *
 * @brief Neural Network Runtime模块接口定义，AI推理框架使用Neural Network Runtime提供的Native接口，完成模型构建。
 * 
 * 注意：Neural Network Runtime的接口目前均不支持多线程并发调用。\n
 * 
 * 引用文件"neural_network_runtime/neural_network_runtime_type.h"
 * @library libneural_network_runtime.so
 * @Syscap SystemCapability.Ai.NeuralNetworkRuntime
 * @since 9
 * @version 2.0
 */

#ifndef NEURAL_NETWORK_RUNTIME_H
#define NEURAL_NETWORK_RUNTIME_H

#include "neural_network_runtime_type.h"
#include "neural_network_core.h"
#include "neural_network_runtime_compat.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建一个{@link NN_QuantParam}量化参数实例。\n
 *
 * 创建{@link NN_QuantParam}量化参数实例后，调用{@link OH_NNQuantParam_SetScales}、{@link OH_NNQuantParam_SetZeroPoints}或{@link OH_NNQuantParam_SetNumBits}设置它的属性值，并调用{@link OH_NNModel_SetTensorQuantParams}将它设置到{@link NN_Tensor}中。最后再调用{@link OH_NNQuantParam_Destroy}销毁它，以避免内存泄露。 
 *
 * @return 指向{@link NN_QuantParam}实例的指针，如果创建失败就返回NULL。
 * @since 11
 * @version 1.0
 */
NN_QuantParam *OH_NNQuantParam_Create();

/**
 * @brief 设置{@link NN_QuantParam}的缩放系数。\n
 *
 * 参数<b>quantCount</b>是张量中量化参数的数量，例如对于per-channel量化，<b>quantCount</b>就是通道数量。
 *
 * @param quantParams 指向{@link NN_QuantParam}实例的指针。
 * @param scales 张量中所有量化参数的缩放系数构成的数组。
 * @param quantCount 张量中量化参数的数量。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 11
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNQuantParam_SetScales(NN_QuantParam *quantParams, const double *scales, size_t quantCount);

/**
 * @brief 设置{@link NN_QuantParam}的零点。\n
 *
 * 参数<b>quantCount</b>是张量中量化参数的数量，例如对于per-channel量化，<b>quantCount</b>就是通道数量。 
 *
 * @param quantParams 指向{@link NN_QuantParam}实例的指针。
 * @param zeroPoints 张量中所有量化参数的零点构成的数组。
 * @param quantCount 张量中量化参数的数量。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 11
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNQuantParam_SetZeroPoints(NN_QuantParam *quantParams, const int32_t *zeroPoints, size_t quantCount);

/**
 * @brief 设置{@link NN_QuantParam}的量化位数。\n
 *
 * 参数<b>quantCount</b>是张量中量化参数的数量，例如对于per-channel量化，<b>quantCount</b>就是通道数量。 
 *
 * @param quantParams 指向{@link NN_QuantParam}实例的指针。
 * @param numBits 张量中所有量化参数的量化位数构成的数组。
 * @param quantCount 张量中量化参数的数量。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 11
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNQuantParam_SetNumBits(NN_QuantParam *quantParams, const uint32_t *numBits, size_t quantCount);

/**
 * @brief 销毁{@link NN_QuantParam}实例。 \n
 *
 * 当设置{@link NN_QuantParam}实例到一个{@link NN_Tensor}中后，如果不再使用该实例，需要销毁它以避免内存泄漏。 \n
 * 
 * 如果<b>quantParams</b>或<b>*quantParams</b>是空指针，那么该接口仅打印警告日志，不会执行销毁操作。
 *
 * @param quantParams 指向{@link NN_QuantParam}实例的二级指针。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 11
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNQuantParam_Destroy(NN_QuantParam **quantParams);

/**
 * @brief 创建{@link OH_NNModel}类型的模型实例，搭配OH_NNModel模块提供的其他接口，完成模型实例的构造。\n 
 *
 * 在开始构图前，先调用{@link OH_NNModel_Construct}创建模型实例，根据模型的拓扑结构，调用{@link OH_NNModel_AddTensorToModel}、{@link OH_NNModel_AddOperation}和{@link OH_NNModel_SetTensorData}方法，填充模型的数据节点和算子节点；然后调用{@link OH_NNModel_SpecifyInputsAndOutputs}指定模型的输入和输出；当构造完模型的拓扑结构，调用{@link OH_NNModel_Finish}完成模型的构建。\n 
 *
 * 模型实例使用完毕后，需要调用{@link OH_NNModel_Destroy}销毁模型实例，避免内存泄漏。
 *
 * @return 返回一个指向{@link OH_NNModel}实例的指针，如果创建失败就返回NULL。
 * @since 9
 * @version 1.0
 */
OH_NNModel *OH_NNModel_Construct(void);

/**
 * @brief 向模型实例中添加张量。\n
 *
 * Neural Network Runtime模型中的数据节点和算子参数均由模型的张量构成。该接口根据{@link NN_TensorDesc}向model实例中添加张量，张量添加的顺序是模型中记录张量的索引值。{@link OH_NNModel_SetTensorData}、{@link OH_NNModel_AddOperation}和{@link OH_NNModel_SpecifyInputsAndOutputs}接口根据该索引值，指定不同的张量。\n 
 *
 * Neural Network Runtime支持动态形状的输入和输出张量。在添加动态形状的数据节点时，需要将tensor.dimensions中支持动态变化的维度设置为-1。例如可将一个四维tensor的dimensions设置为[1, -1, 2, 2]，表示其第二个维度支持动态变化。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param tensorDesc {@link NN_TensorDesc}张量的指针，{@link NN_TensorDesc}指定了添加到模型实例中张量的属性。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_AddTensorToModel(OH_NNModel *model, const NN_TensorDesc *tensorDesc);

/**
 * @brief 设置张量的数值。\n 
 *
 * 对于具有常量值的张量（如模型的权重），需要在构图阶段使用该接口设置数值。张量的索引值根据张量添加进模型的顺序决定，张量的添加参考{@link OH_NNModel_AddTensorToModel}。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param index 张量的索引值。
 * @param dataBuffer 指向真实数据内存的指针。
 * @param length 数据内存的长度。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_SetTensorData(OH_NNModel *model, uint32_t index, const void *dataBuffer, size_t length);

/**
 * @brief 设置张量的量化参数，参考{@link NN_QuantParam}。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param index 张量的索引值。
 * @param quantParam 指向{@link NN_QuantParam}的指针。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 11
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_SetTensorQuantParams(OH_NNModel *model, uint32_t index, NN_QuantParam *quantParam);

/**
 * @brief 设置张量的类型，参考{@link OH_NN_TensorType}。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param index 张量的索引值。
 * @param tensorType 张量类型。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}
 * @since 11
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_SetTensorType(OH_NNModel *model, uint32_t index, OH_NN_TensorType tensorType);

/**
 * @brief 向模型实例中添加算子。\n
 *
 * 该接口向模型实例中添加算子，算子类型由op指定，算子的参数、输入和输出由paramIndices、inputIndices和outputIndices指定。该接口将对算子参数的属性和输入、输出张量的数量进行校验，这些属性需要在调用{@link OH_NNModel_AddTensorToModel}添加张量时正确设置。每个算子期望的参数、输入和输出属性请参考{@link OH_NN_OperationType}。\n 
 *
 * paramIndices、inputIndices和outputIndices中存储的是张量的索引值，每个索引值根据张量添加进模型的顺序决定，正确设置并添加算子要求准确设置每个张量的索引值。张量的添加参考{@link OH_NNModel_AddTensorToModel}。\n 
 *
 * 如果添加算子时，添加了额外的参数（非算子需要的参数），该接口返回{@link OH_NN_INVALID_PARAMETER}；如果没有设置算子参数，则算子按默认值设置缺省的参数，默认值请参考{@link OH_NN_OperationType}。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param op 指定添加的算子类型，取值请参考{@link OH_NN_OperationType}的枚举值。
 * @param paramIndices OH_NN_UInt32Array实例的指针，设置算子的参数张量索引。
 * @param inputIndices OH_NN_UInt32Array实例的指针，指定算子的输入张量索引。
 * @param outputIndices OH_NN_UInt32Array实例的指针，设置算子的输出张量索引。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_AddOperation(OH_NNModel *model,
                                         OH_NN_OperationType op,
                                         const OH_NN_UInt32Array *paramIndices,
                                         const OH_NN_UInt32Array *inputIndices,
                                         const OH_NN_UInt32Array *outputIndices);

/**
 * @brief 指定模型的输入和输出张量的索引值。\n 
 *
 * 模型实例需要指定张量作为端到端的输入和输出张量。注意设置一个张量为输入或输出张量后，就不能再通过调用{@link OH_NNModel_SetTensorData}设置张量数据，而需要在执行阶段调用OH_NNExecutor的方法设置输入或输出张量数据。\n 
 *
 * 张量的索引值根据张量添加进模型的顺序决定，张量的添加参考{@link OH_NNModel_AddTensorToModel}。\n 
 *
 * 暂时不支持异步设置模型输入和输出张量。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param inputIndices OH_NN_UInt32Array实例的指针，指定算子的输入张量。
 * @param outputIndices OH_NN_UInt32Array实例的指针，指定算子的输出张量。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_SpecifyInputsAndOutputs(OH_NNModel *model,
                                                    const OH_NN_UInt32Array *inputIndices,
                                                    const OH_NN_UInt32Array *outputIndices);

/**
 * @brief 完成模型构图。\n 
 *
 * 完成模型拓扑结构的搭建后，调用该接口指示构图已完成。在调用该接口后，无法进行额外的构图操作，调用{@link OH_NNModel_AddTensorToModel}、{@link OH_NNModel_AddOperation}、{@link OH_NNModel_SetTensorData}和{@link OH_NNModel_SpecifyInputsAndOutputs}将返回{@link OH_NN_OPERATION_FORBIDDEN}。\n 
 *
 * 在调用{@link OH_NNModel_GetAvailableOperations}和{@link OH_NNCompilation_Construct}之前，必须先调用该接口完成构图。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_Finish(OH_NNModel *model);

/**
 * @brief 销毁模型实例。\n 
 *
 * 调用{@link OH_NNModel_Construct}创建的模型实例需要调用该接口主动销毁，否则将造成内存泄漏。\n 
 *
 * 如果model为空指针或者*model为空指针，该接口仅打印警告日志，不执行销毁操作。
 *
 * @param model 指向{@link OH_NNModel}实例的二级指针。模型实例销毁后，该接口会将*model主动设置为空指针。
 * @since 9
 * @version 1.0
 */
void OH_NNModel_Destroy(OH_NNModel **model);

/**
 * @brief 查询硬件对模型内所有算子的支持情况，通过布尔值序列指示支持情况。\n 
 *
 * 查询底层硬件对模型实例内每个算子的支持情况，硬件由deviceID指定，结果将通过isSupported指向的数组表示。如果支持第i个算子，则(*isSupported)[i] == true，否则为false。\n 
 *
 * 该接口成功执行后，(*isSupported)将指向记录算子支持情况的bool数组，数组长度和模型实例的算子数量相等。该数组对应的内存由Neural Network Runtime管理，在模型实例销毁或再次调用该接口后自动销毁。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param deviceID 指定查询的硬件ID，通过{@link OH_NNDevice_GetAllDevicesID}获取。
 * @param isSupported 指向bool数组的指针。调用该接口时，要求(*isSupported)为空指针，否则返回{@link OH_NN_INVALID_PARAMETER}。
 * @param opCount 模型实例中算子的数量，对应(*isSupported)数组的长度。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_GetAvailableOperations(OH_NNModel *model,
                                                   size_t deviceID,
                                                   const bool **isSupported,
                                                   uint32_t *opCount);

/**
 * @brief 向模型实例中添加张量。\n
 *
 * Neural Network Runtime模型中的数据节点和算子参数均由模型的张量构成，该接口根据tensor，向model实例中添加张量。张量添加的顺序由模型中记录张量的索引值来确定，{@link OH_NNModel_SetTensorData}、{@link OH_NNModel_AddOperation}和{@link OH_NNModel_SpecifyInputsAndOutputs}接口根据该索引值，指定不同的张量。\n 
 *
 * Neural Network Runtime支持动态形状输入和输出。在添加动态形状的数据节点时，需要将tensor.dimensions中支持动态变化的维度设置为-1。例如可将一个四维tensor的dimensions设置为[1, -1, 2, 2]，表示其第二个维度支持动态变化。
 *
 * @param model 指向{@link OH_NNModel}实例的指针。
 * @param tensor {@link OH_NN_Tensor}张量的指针，tensor指定了添加到模型实例中张量的属性。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @deprecated since 11
 * @useinstead {@link OH_NNModel_AddTensorToModel}
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNModel_AddTensor(OH_NNModel *model, const OH_NN_Tensor *tensor);

/**
 * @brief 设置模型单个输入的数据。\n 
 *
 * 该接口将dataBuffer中，长度为length个字节的数据，拷贝到底层硬件的共享内存。inputIndex指定设置的输入，tensor用于设置输入张量的形状、数据类型、量化参数等信息。\n 
 *
 * 由于Neural Network Runtime支持动态输入形状的模型，在固定形状输入和动态形状输入的场景下，该接口采取不同的处理策略：\n
 *
 * - 固定形状输入的场景：tensor各属性必须和构图阶段调用{@link OH_NNModel_AddTensor}添加的张量保持一致；\n
 * - 动态形状输入的场景：在构图阶段，由于动态输入的形状不确定，调用该接口时，要求tensor.dimensions中的每个值必须大于0，以确定执行计算阶段输入的形状。设置形状时，只允许调整数值为-1的维度。\n
 * 假设在构图阶段，输入A的维度为[-1, 224, 224, 3]，调用该接口时，只能调整第一个维度的尺寸，如：[3, 224, 224, 3]。调整其他维度将返回{@link OH_NN_INVALID_PARAMETER}。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @param inputIndex 输入的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输入数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，inputIndices为{1, 5, 9}，则在设置输入的阶段，三个输入的索引值分别为{0, 1, 2}。
 * @param tensor 设置输入数据对应的张量。
 * @param dataBuffer 指向输入数据的指针。
 * @param length 数据内存的字节长度。
 * @return  函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @deprecated since 11
 * @useinstead {@link OH_NNExecutor_RunSync}
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNExecutor_SetInput(OH_NNExecutor *executor,
                                        uint32_t inputIndex,
                                        const OH_NN_Tensor *tensor,
                                        const void *dataBuffer,
                                        size_t length);

/**
 * @brief 设置模型单个输出的内存。\n
 *
 * 该接口将dataBuffer指向的内存与outputIndex指定的输出绑定，内存的长度由length指定。\n 
 *
 * 调用{@link OH_NNExecutor_Run}完成单次模型推理后，Neural Network Runtime将比对dataBuffer指向的内存与输出数据的长度，根据不同情况，返回不同结果：\n 
 *
 * - 如果内存大小大于或等于数据长度：则推理后的结果将拷贝至内存，并返回{@link OH_NN_SUCCESS}，可以通过访问dataBuffer读取推理结果。\n 
 * - 如果内存大小小于数据长度：则{@link OH_NNExecutor_Run}将返回{@link OH_NN_INVALID_PARAMETER}，并输出日志告知内存太小的信息。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @param outputIndex 输出的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输出数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，outputIndices为{4, 6, 8}，则在设置输出内存时，三个输出的索引值分别为{0, 1, 2}。
 * @param dataBuffer 指向输出数据的指针。
 * @param length 数据内存的字节长度。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @deprecated since 11
 * @useinstead {@link OH_NNExecutor_RunSync}
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNExecutor_SetOutput(OH_NNExecutor *executor,
                                         uint32_t outputIndex,
                                         void *dataBuffer,
                                         size_t length);

/**
 * @brief 执行推理。\n 
 *
 * 在执行器关联的硬件上，执行模型的端到端推理计算。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @deprecated since 11
 * @useinstead {@link OH_NNExecutor_RunSync}
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNExecutor_Run(OH_NNExecutor *executor);

/**
 * @brief 在硬件上为单个输入申请共享内存。\n 
 *
 * Neural Network Runtime 提供主动申请硬件共享内存的方法。通过指定执行器和输入索引值，该接口在单个输入关联的硬件上，申请大小为length的共享内存，通过{@link OH_NN_Memory}实例返回。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @param inputIndex 输入的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输入数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，inputIndices为{1, 5, 9}，则在申请输入内存时，三个输入的索引值分别为{0, 1, 2}。
 * @param length 申请的内存字节。
 * @return 指向{@link OH_NN_Memory}实例的指针，如果创建失败就返回NULL。
 * @deprecated since 11
 * @useinstead {@link OH_NNTensor_CreateWithSize}
 * @since 9
 * @version 1.0
 */
OH_NN_Memory *OH_NNExecutor_AllocateInputMemory(OH_NNExecutor *executor, uint32_t inputIndex, size_t length);

/**
 * @brief 在硬件上为单个输出申请共享内存。\n
 *
 * Neural Network Runtime 提供主动申请硬件共享内存的方法。通过指定执行器和输出索引值，该接口在单个输出关联的硬件上，申请大小为length的共享内存，通过{@link OH_NN_Memory}实例返回。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @param outputIndex 输出的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输出数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，outputIndices为{4, 6, 8}，则在申请输出内存时，三个输出的索引值分别为{0, 1, 2}。
 * @param length 申请的内存字节。
 * @return 指向{@link OH_NN_Memory}实例的指针，如果创建失败就返回NULL。
 * @deprecated since 11
 * @useinstead {@link OH_NNTensor_CreateWithSize}
 * @since 9
 * @version 1.0
 */
OH_NN_Memory *OH_NNExecutor_AllocateOutputMemory(OH_NNExecutor *executor, uint32_t outputIndex, size_t length);

/**
 * @brief 释放{@link OH_NN_Memory}实例指向的输入内存。\n 
 *
 * 调用{@link OH_NNExecutor_AllocateInputMemory}创建的内存实例，需要主动调用该接口进行释放，否则将造成内存泄漏。inputIndex和memory的对应关系需要和创建内存实例时保持一致。\n 
 *
 * 如果memory或*memory为空指针，该接口仅打印警告日志，不执行释放操作。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @param inputIndex 输入的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输入数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，inputIndices为{1, 5, 9}，则在释放输入内存时，三个输入的索引值分别为{0, 1, 2}。
 * @param memory 指向{@link OH_NN_Memory}实例的二级指针。共享内存释放后，该接口将*memory主动设置为空指针。
 * @deprecated since 11
 * @useinstead {@link OH_NNTensor_Destroy}
 * @since 9
 * @version 1.0
 */
void OH_NNExecutor_DestroyInputMemory(OH_NNExecutor *executor, uint32_t inputIndex, OH_NN_Memory **memory);

/**
 * @brief 释放{@link OH_NN_Memory}实例指向的输出内存。\n 
 *
 * 调用{@link OH_NNExecutor_AllocateOutputMemory}创建的内存实例，需要主动调用该接口进行释放，否则将造成内存泄漏。outputIndex和memory的对应关系需要和创建内存实例时保持一致。\n 
 *
 * 如果memory或*memory为空指针，该接口仅打印警告日志，不执行释放操作。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @param outputIndex 输出的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输出数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，outputIndices为{4, 6, 8}，则在释放输出内存时，三个输出的索引值分别为{0, 1, 2}。
 * @param memory 指向{@link OH_NN_Memory}实例的二级指针。共享内存释放后，该接口将*memory主动设置为空指针。
 * @deprecated since 11
 * @useinstead {@link OH_NNTensor_Destroy} 
 * @since 9
 * @version 1.0
 */
void OH_NNExecutor_DestroyOutputMemory(OH_NNExecutor *executor, uint32_t outputIndex, OH_NN_Memory **memory);

/**
 * @brief 将{@link OH_NN_Memory}实例指向的硬件共享内存，并指定为单个输入使用的内存。\n 
 *
 * 在需要自行管理内存的场景下，该接口将执行输入和{@link OH_NN_Memory}内存实例绑定。执行计算时，底层硬件从内存实例指向的共享内存中读取输入数据。通过该接口，可以实现设置输入、执行计算、读取输出的并发执行，提升数据流的推理效率。
 *
 * @param executor 指向{@link OH_NNExecutor}实例的指针。
 * @param inputIndex 输入的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输入数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，inputIndices为{1, 5, 9}，则在指定输入的共享内存时，三个输入的索引值分别为{0, 1, 2}。
 * @param tensor 指向{@link OH_NN_Tensor}的指针，设置单个输入所对应的张量。
 * @param memory 指向{@link OH_NN_Memory}的指针。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @deprecated since 11
 * @useinstead {@link OH_NNExecutor_RunSync}
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNExecutor_SetInputWithMemory(OH_NNExecutor *executor,
                                                  uint32_t inputIndex,
                                                  const OH_NN_Tensor *tensor,
                                                  const OH_NN_Memory *memory);

/**
 * @brief 将{@link OH_NN_Memory}实例指向的硬件共享内存，并指定为单个输出使用的内存。\n
 *
 * 在需要自行管理内存的场景下，该接口将执行输出和{@link OH_NN_Memory}内存实例绑定。执行计算时，底层硬件将计算结果直接写入内存实例指向的共享内存。通过该接口，可以实现设置输入、执行计算、读取输出的并发执行，提升数据流的推理效率。
 *
 * @param executor 执行器。
 * @param outputIndex 输出的索引值，与调用{@link OH_NNModel_SpecifyInputsAndOutputs}时输出数据的顺序一致。\n
 * 假设调用{@link OH_NNModel_SpecifyInputsAndOutputs}时，outputIndices为{4, 6, 8}，则在指定输出的共享内存时，三个输出的索引值分别为{0, 1, 2}。
 * @param memory 指向{@link OH_NN_Memory}的指针。
 * @return 函数执行的结果状态。执行成功返回OH_NN_SUCCESS；失败返回具体错误码，具体失败错误码可参考{@link OH_NN_ReturnCode}。
 * @deprecated since 11
 * @useinstead {@link OH_NNExecutor_RunSync}
 * @since 9
 * @version 1.0
 */
OH_NN_ReturnCode OH_NNExecutor_SetOutputWithMemory(OH_NNExecutor *executor,
                                                   uint32_t outputIndex,
                                                   const OH_NN_Memory *memory);

#ifdef __cplusplus
}
#endif // __cplusplus

/** @} */
#endif // NEURAL_NETWORK_RUNTIME_H
