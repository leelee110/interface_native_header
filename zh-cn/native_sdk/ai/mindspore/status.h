/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MindSpore
 * @{
 * 
 * @brief 提供MindSpore Lite的模型推理相关接口，该模块下的接口是非线程安全的。
 * 
 * @Syscap SystemCapability.Ai.MindSpore
 * @since 9
 */

/**
 * @file status.h
 * 
 * @brief 提供了MindSpore Lite运行时的状态码。
 *
 * @include <mindspore/status.h>
 * @library libmindspore_lite_ndk.so
 * @since 9
 */
#ifndef MINDSPORE_INCLUDE_C_API_STATUS_C_H
#define MINDSPORE_INCLUDE_C_API_STATUS_C_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif
  /**
   * @brief Minspore不同组件的代码。
   * 
   * @since 9
   */
  enum OH_AI_CompCode
  {
    /** MindSpore Core的代码。 */
    OH_AI_COMPCODE_CORE = 0x00000000u,
    /** MindSpore MindData的代码。 */
    OH_AI_COMPCODE_MD = 0x10000000u,
    /** MindSpore MindExpression的代码。 */
    OH_AI_COMPCODE_ME = 0x20000000u,
    /** MindSpore的代码。 */
    OH_AI_COMPCODE_MC = 0x30000000u,
    /** MindSpore Lite的代码。 */
    OH_AI_COMPCODE_LITE = 0xF0000000u,
  };

  /**
   * @brief Minspore的状态码。
   * 
   * @since 9
   */
  typedef enum OH_AI_Status
  {
    /** 通用的成功状态码。 */
    OH_AI_STATUS_SUCCESS = 0,

    // Core
    /** MindSpore Core 失败状态码。 */
    OH_AI_STATUS_CORE_FAILED = OH_AI_COMPCODE_CORE | 0x1,

    // Lite
    /** MindSpore Lite 异常状态码。 */
    OH_AI_STATUS_LITE_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -1),
    /** MindSpore Lite 空指针状态码。 */
    OH_AI_STATUS_LITE_NULLPTR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -2),
    /** MindSpore Lite 参数异常状态码。 */
    OH_AI_STATUS_LITE_PARAM_INVALID = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -3),
    /** MindSpore Lite 未改变状态码。 */
    OH_AI_STATUS_LITE_NO_CHANGE = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -4),
    /** MindSpore Lite 没有错误但是退出的状态码。 */
    OH_AI_STATUS_LITE_SUCCESS_EXIT = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -5),
    /** MindSpore Lite 内存分配失败的状态码。 */
    OH_AI_STATUS_LITE_MEMORY_FAILED = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -6),
    /** MindSpore Lite 功能未支持的状态码。 */
    OH_AI_STATUS_LITE_NOT_SUPPORT = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -7),
    /** MindSpore Lite 线程池异常状态码。 */
    OH_AI_STATUS_LITE_THREADPOOL_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -8),
    /** MindSpore Lite 未初始化状态码。 */
    OH_AI_STATUS_LITE_UNINITIALIZED_OBJ = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -9),

    // 执行器相关的错误码，范围 [-100,-200)
    /** MindSpore Lite 张量溢出错误的状态码。*/
    OH_AI_STATUS_LITE_OUT_OF_TENSOR_RANGE = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -100),
    /** MindSpore Lite 输入张量异常的状态码。*/
    OH_AI_STATUS_LITE_INPUT_TENSOR_ERROR =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -101),
    /** MindSpore Lite 重入异常的状态码。*/
    OH_AI_STATUS_LITE_REENTRANT_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -102),

    // 图相关的错误码，范围 [-200,-300)
    /** MindSpore Lite 文件异常状态码。*/
    OH_AI_STATUS_LITE_GRAPH_FILE_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -200),

    // 算子相关的错误码，范围 [-300,-400)
    /** MindSpore Lite 未找到算子的状态码。*/
    OH_AI_STATUS_LITE_NOT_FIND_OP = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -300),
    /** MindSpore Lite 无效算子状态码。*/
    OH_AI_STATUS_LITE_INVALID_OP_NAME = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -301),
    /** MindSpore Lite 无效算子超参数状态码。*/
    OH_AI_STATUS_LITE_INVALID_OP_ATTR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -302),
    /** MindSpore Lite 算子执行失败的状态码。*/
    OH_AI_STATUS_LITE_OP_EXECUTE_FAILURE =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -303),

    // 张量相关的错误码，范围 [-400,-500)
    /** MindSpore Lite 张量格式异常状态码。*/
    OH_AI_STATUS_LITE_FORMAT_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -400),

    // 形状推理相关的错误码，范围 [-500,-600)
    /** MindSpore Lite 形状推理异常状态码。*/
    OH_AI_STATUS_LITE_INFER_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -500),
    /** MindSpore Lite 无效的形状推理的状态码。*/
    OH_AI_STATUS_LITE_INFER_INVALID =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -501),

    // 用户输入相关的错误码，范围 [-600, 700)
    /** MindSpore Lite 用户输入的参数无效状态码。*/
    OH_AI_STATUS_LITE_INPUT_PARAM_INVALID =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -600),

  } OH_AI_Status;
#ifdef __cplusplus
}
#endif

/** @} */
#endif // MINDSPORE_INCLUDE_C_API_STATUS_C_H
