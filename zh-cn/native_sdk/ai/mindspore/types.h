/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MindSpore
 * @{
 *
 * @brief 提供MindSpore Lite的模型推理相关接口，该模块下的接口是非线程安全的。
 *
 * @Syscap SystemCapability.Ai.MindSpore
 * @since 9
 */

/**
 * @file types.h
 *
 * @brief 提供了MindSpore Lite支持的模型文件类型和设备类型。
 *
 * @include <mindspore/types.h>
 * @library libmindspore_lite_ndk.so
 * @since 9
 */

#ifndef MINDSPORE_INCLUDE_C_API_TYPES_C_H
#define MINDSPORE_INCLUDE_C_API_TYPES_C_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef OH_AI_API
#ifdef _WIN32
#define OH_AI_API __declspec(dllexport)
#else
#define OH_AI_API __attribute__((visibility("default")))
#endif
#endif
/**
 * @brief 模型文件的类型。
 *
 * @since 9
 */
typedef enum OH_AI_ModelType {
    /** 模型类型是MindIR，对应的模型文件后缀为.ms。
     *
     * @since 9
     */
    OH_AI_MODELTYPE_MINDIR = 0,
    /** 模型类型无效。
     *
     * @since 9
     */
    OH_AI_MODELTYPE_INVALID = 0xFFFFFFFF
} OH_AI_ModelType;

/**
 * @brief 设备类型信息，包含了目前支持的设备类型。
 *
 * @since 9
 */
typedef enum OH_AI_DeviceType {
    /** 设备类型是CPU。
     *
     * @since 9
     */
    OH_AI_DEVICETYPE_CPU = 0,
    /** Device type: GPU。
     * 
     * 预留选项，暂不支持。
     * 
     * @since 9
     */
    OH_AI_DEVICETYPE_GPU,
    /** 设备类型是麒麟NPU。
     * 
     * 预留选项，暂不支持。
     * 
     * @since 9
     */
    OH_AI_DEVICETYPE_KIRIN_NPU,
    /** 设备类型是NNRt。
     *
     * OHOS设备范围是[60,80)。
     * 
     * @since 9
     */
    OH_AI_DEVICETYPE_NNRT = 60,
    /** 设备类型无效。
     * 
     * @since 9
     * 
     */
    OH_AI_DEVICETYPE_INVALID = 100,
} OH_AI_DeviceType;

/**
 * @brief NNRt管理的硬件设备类型。
 *
 * @since 10
 */
typedef enum OH_AI_NNRTDeviceType {
    /** 设备类型不属于以下3种，则属于其它。
     *
     * @since 10
     */
    OH_AI_NNRTDEVICE_OTHERS = 0,
    /** CPU设备。
     *
     * @since 10
     */
    OH_AI_NNRTDEVICE_CPU = 1,
    /** GPU设备。
     *
     * @since 10
     */
    OH_AI_NNRTDEVICE_GPU = 2,
    /** 特定的加速设备。
     *
     * @since 10
     */
    OH_AI_NNRTDEVICE_ACCELERATOR = 3,
} OH_AI_NNRTDeviceType;

/**
 * @brief NNRt硬件的工作性能模式。
 *
 * @since 10
 */
typedef enum OH_AI_PerformanceMode {
    /** 无特殊设置。
     *
     * @since 10
     */
    OH_AI_PERFORMANCE_NONE = 0,
    /** 低功耗模式。
     *
     * @since 10
     */
    OH_AI_PERFORMANCE_LOW = 1,
    /** 功耗-性能均衡模式。
     *
     * @since 10
     */
    OH_AI_PERFORMANCE_MEDIUM = 2,
    /** 高性能模式。
     *
     * @since 10
     */
    OH_AI_PERFORMANCE_HIGH = 3,
    /** 极致性能模式。
     *
     * @since 10
     */
    OH_AI_PERFORMANCE_EXTREME = 4,
} OH_AI_PerformanceMode;

/**
 * @brief NNRt推理任务优先级。
 *
 * @since 10
 */
typedef enum OH_AI_Priority {
    /** 无优先级偏好。
     *
     * @since 10
     */
    OH_AI_PRIORITY_NONE = 0,
    /** 低优先级任务。
     *
     * @since 10
     */
    OH_AI_PRIORITY_LOW = 1,
    /** 中优先级任务。
     *
     * @since 10
     */
    OH_AI_PRIORITY_MEDIUM = 2,
    /** 高优先级。
     *
     * @since 10
     */
    OH_AI_PRIORITY_HIGH = 3,
} OH_AI_Priority;

/**
 * @brief 训练优化等级。
 *
 * @since 11
 */
typedef enum OH_AI_OptimizationLevel {
    /** 无优化等级。
     *
     * @since 11
     */
    OH_AI_KO0 = 0,
    /** 将网络转换为float16，保持批量归一化层和损失函数为float32。
     *
     * @since 11
     */
    OH_AI_KO2 = 2,
    /** 将网络转换为float16，包括批量归一化层。
     *
     * @since 11
     */
    OH_AI_KO3 = 3,
    /** 根据设备选择优化等级。
     *
     * @since 11
     */
    OH_AI_KAUTO = 4,
    /** 无效优化等级。
     *
     * @since 11
     */
    OH_AI_KOPTIMIZATIONTYPE = 0xFFFFFFFF
} OH_AI_OptimizationLevel;

/**
 * @brief 量化类型信息。
 *
 * @since 11
 */
typedef enum OH_AI_QuantizationType {
    /** 不做量化。
     *
     * @since 11
     */
    OH_AI_NO_QUANT = 0,
    /** 权重量化。
     *
     * @since 11
     */
    OH_AI_WEIGHT_QUANT = 1,
    /** 全量化。
     *
     * @since 11
     */
    OH_AI_FULL_QUANT = 2,
    /** 无效量化类型。
     *
     * @since 11
     */
    OH_AI_UNKNOWN_QUANT_TYPE = 0xFFFFFFFF
} OH_AI_QuantizationType;

/**
 * @brief NNRt设备信息描述，包含设备ID，设备名称等信息。
 *
 * @since 10
 */
typedef struct NNRTDeviceDesc NNRTDeviceDesc;

#ifdef __cplusplus
}
#endif

/** @} */
#endif // MINDSPORE_INCLUDE_C_API_TYPES_C_H
