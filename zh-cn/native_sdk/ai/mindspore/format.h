/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MindSpore
 * @{
 *
 * @brief 提供MindSpore Lite的模型推理相关接口，该模块下的接口是非线程安全的。
 *
 * @Syscap SystemCapability.Ai.MindSpore
 * @since 9
 */

/**
 * @file format.h
 *
 * @brief 提供张量数据的排列格式。
 *
 * @include <mindspore/format.h>
 * @library libmindspore_lite_ndk.so
 * @since 9
 */
#ifndef MINDSPORE_INCLUDE_C_API_FORMAT_C_H
#define MINDSPORE_INCLUDE_C_API_FORMAT_C_H

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief  MSTensor保存的数据支持的排列格式。
 *
 * @since 9
 */
typedef enum OH_AI_Format {
  /** 按批次N、通道C、高度H和宽度W的顺序存储张量数据。 */
  OH_AI_FORMAT_NCHW = 0,
  /** 按批次N、高度H、宽度W和通道C的顺序存储张量数据。 */
  OH_AI_FORMAT_NHWC = 1,
  /** 按批次N、高度H、宽度W和通道C的顺序存储张量数据，其中C轴是4字节对齐格式。 */
  OH_AI_FORMAT_NHWC4 = 2,
  /** 按高度H、宽度W、核数K和通道C的顺序存储张量数据。 */
  OH_AI_FORMAT_HWKC = 3,
  /** 按高度H、宽度W、通道C和核数K的顺序存储张量数据。 */
  OH_AI_FORMAT_HWCK = 4,
  /** 按核数K、通道C、高度H和宽度W的顺序存储张量数据。 */
  OH_AI_FORMAT_KCHW = 5,
  /** 按通道C、核数K、高度H和宽度W的顺序存储张量数据。 */
  OH_AI_FORMAT_CKHW = 6,
  /** 按核数K、高度H、宽度W和通道C的顺序存储张量数据。 */
  OH_AI_FORMAT_KHWC = 7,
  /** 按通道C、高度H、宽度W和核数K的顺序存储张量数据。 */
  OH_AI_FORMAT_CHWK = 8,
  /** 按高度H和宽度W的顺序存储张量数据。 */
  OH_AI_FORMAT_HW = 9,
  /** 按高度H和宽度W的顺序存储张量数据，其中W轴是4字节对齐格式。 */
  OH_AI_FORMAT_HW4 = 10,
  /** 按批次N和通道C的顺序存储张量数据。 */
  OH_AI_FORMAT_NC = 11,
  /** 按批次N和通道C的顺序存储张量数据，其中C轴是4字节对齐格式。 */
  OH_AI_FORMAT_NC4 = 12,
  /** 按批次N、通道C、高度H和宽度W的顺序存储张量数据，其中C轴和W轴是4字节对齐格式。 */
  OH_AI_FORMAT_NC4HW4 = 13,
  /** 按批次N、通道C、深度D、高度H和宽度W的顺序存储张量数据。 */
  OH_AI_FORMAT_NCDHW = 15,
  /** 按批次N、宽度W和通道C的顺序存储张量数据。 */
  OH_AI_FORMAT_NWC = 16,
  /** 按批次N、通道C和宽度W的顺序存储张量数据。 */
  OH_AI_FORMAT_NCW = 17
} OH_AI_Format;

#ifdef __cplusplus
}
#endif

/** @} */
#endif // MINDSPORE_INCLUDE_C_API_FORMAT_C_H
