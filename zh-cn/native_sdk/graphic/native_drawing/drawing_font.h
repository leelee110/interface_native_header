/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FONT_H
#define C_INCLUDE_DRAWING_FONT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_font.h
 *
 * @brief 文件中定义了与字体相关的功能函数。
 *
 * @include native_drawing/drawing_font.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_error_code.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 字型边缘效果类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FontEdging {
    /** 无抗锯齿处理。 */
    FONT_EDGING_ALIAS,
    /** 使用抗锯齿来平滑字型边缘。 */
    FONT_EDGING_ANTI_ALIAS,
    /** 使用次像素级别的抗锯齿来平滑字型边缘，可以获得更加平滑的字型渲染效果。 */
    FONT_EDGING_SUBPIXEL_ANTI_ALIAS,
} OH_Drawing_FontEdging;

/**
 * @brief 字型轮廓效果类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FontHinting {
    /** 不修改字型轮廓。 */
    FONT_HINTING_NONE,
    /** 最小限度修改字型轮廓以改善对比度。 */
    FONT_HINTING_SLIGHT,
    /** 修改字型轮廓以提高对比度。 */
    FONT_HINTING_NORMAL,
    /** 修改字型轮廓以获得最大对比度。 */
    FONT_HINTING_FULL,
} OH_Drawing_FontHinting;

/**
 * @brief 当前画布矩阵轴对齐时，将字型基线设置为是否与像素对齐。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param baselineSnap 指示字型基线是否和像素对齐。true表示对齐，false表示不对齐。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetBaselineSnap(OH_Drawing_Font*, bool baselineSnap);

/**
 * @brief 当前画布矩阵轴对齐时，获取字型基线是否与像素对齐。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回字型基线是否与像素对齐。true为对齐，false为没有对齐。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsBaselineSnap(const OH_Drawing_Font*);

/**
 * @brief 用于设置字型边缘效果。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_FontEdging不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param OH_Drawing_FontEdging 字型边缘效果。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetEdging(OH_Drawing_Font*, OH_Drawing_FontEdging);

/**
 * @brief 获取字型边缘效果。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回字型边缘效果。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontEdging OH_Drawing_FontGetEdging(const OH_Drawing_Font*);

/**
 * @brief 用于设置是否自动调整字型轮廓。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param isForceAutoHinting 是否自动调整字型轮廓。true为自动调整，false为不自动调整。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetForceAutoHinting(OH_Drawing_Font*, bool isForceAutoHinting);

/**
 * @brief 获取字型轮廓是否自动调整。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回字型轮廓是否自动调整。true为自动调整，false为不自动调整。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsForceAutoHinting(const OH_Drawing_Font*);

/**
 * @brief 设置字型是否使用次像素渲染。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param isSubpixel 字型是否使用次像素渲染。true为使用，false为不使用。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetSubpixel(OH_Drawing_Font*, bool isSubpixel);

/**
 * @brief 获取字型是否使用次像素渲染。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回字型是否使用次像素渲染。true为使用，false为不使用。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsSubpixel(const OH_Drawing_Font*);

/**
 * @brief 用于创建一个字型对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的字型对象。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Font* OH_Drawing_FontCreate(void);

/**
 * @brief 用于给字型设置字体。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象的指针。
 * @param OH_Drawing_Typeface 指向字体对象的指针，为NULL会使用系统默认字体对象。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTypeface(OH_Drawing_Font*, OH_Drawing_Typeface*);

/**
 * @brief 获取字体对象。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return OH_Drawing_Typeface 函数返回一个指针，指向字体对象{@link OH_Drawing_Typeface}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_FontGetTypeface(OH_Drawing_Font*);

/**
 * @brief 用于给字型对象设置文字大小。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象的指针。
 * @param textSize 文字大小，该参数为浮点数，为负数时字体大小会被置为0。字体大小为0时，绘制的文字不会显示。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSize(OH_Drawing_Font*, float textSize);

/**
 * @brief 获取字型对象的文字大小。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回一个浮点数，表示文字大小。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetTextSize(const OH_Drawing_Font*);

/**
 * @brief 获取文本所表示的字符数量。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font、text任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param text 文本存储首地址。
 * @param byteLength 文本长度，单位为字节。
 * @param encoding 文本编码类型{@link OH_Drawing_TextEncoding}。
 * @since 12
 * @version 1.0
 */
int OH_Drawing_FontCountText(OH_Drawing_Font*, const void* text, size_t byteLength,
    OH_Drawing_TextEncoding encoding);

/**
 * @brief 用于将文本转换为字形索引。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font、text、glyphs任意一个为NULL或者byteLength等于0或者maxGlyphCount小于等于0时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param text 文本存储首地址。
 * @param byteLength 文本长度，单位为字节。
 * @param encoding 文本编码类型{@link OH_Drawing_TextEncoding}。
 * @param glyphs 字形索引存储首地址，用于存储得到的字形索引。
 * @param maxGlyphCount 文本所表示的最大字符数量。
 * @return 返回字形索引数量。
 * @since 12
 * @version 1.0
 */
uint32_t OH_Drawing_FontTextToGlyphs(const OH_Drawing_Font*, const void* text, uint32_t byteLength,
    OH_Drawing_TextEncoding encoding, uint16_t* glyphs, int maxGlyphCount);

/**
 * @brief 用于获取字符串中每个字符的宽度。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font、glyphs、widths任意一个为NULL或者count小于等于0时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param glyphs 字形索引存储首地址。
 * @param count 字形索引的数量。
 * @param widths 字形宽度存储首地址，用于存储得到的字形宽度。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontGetWidths(const OH_Drawing_Font*, const uint16_t* glyphs, int count, float* widths);

/**
 * @brief 用于测量单个字符的宽度。当前字型中的字体不支持待测量字符时，退化到使用系统字体测量字符宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param str 待测量的单个字符。可以传入字符串，但只会以UTF-8编码解析并测量字符串中的首个字符。
 * @param textWidth 用于存储得到的字符宽度。
 * @return 函数返回执行错误码。\n
 * 返回OH_DRAWING_SUCCESS，表示执行成功。\n
 * 返回OH_DRAWING_ERROR_INVALID_PARAMETER，表示参数font、str、textWidth任意一个为NULL或者str的长度为0。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_FontMeasureSingleCharacter(const OH_Drawing_Font* font, const char* str,
    float* textWidth);

/**
 * @brief 用于获取文本的宽度和边界框。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param text 指向文本的指针。
 * @param byteLength 表示以字节为单位的文本长度。
 * @param encoding 文本编码类型。
 * @param bounds 用于承载获取的边界框，可以为NULL。
 * @param textWidth 表示文本宽度。
 * @return 函数返回执行错误码。\n
 * 返回OH_DRAWING_SUCCESS，表示执行成功。\n
 * 返回OH_DRAWING_ERROR_INVALID_PARAMETER，表示参数font，text，textWidth至少有一个为空，或者byteLength为0。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_FontMeasureText(const OH_Drawing_Font* font, const void* text, size_t byteLength,
    OH_Drawing_TextEncoding encoding, OH_Drawing_Rect* bounds, float* textWidth);

/**
 * @brief 用于设置线性可缩放字型。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象的指针。
 * @param isLinearText 真为使能线性可缩放字型，假为不使能。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetLinearText(OH_Drawing_Font*, bool isLinearText);

/**
 * @brief 获取字型对象是否使用线性缩放。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回字型对象是否使用线性缩放，true为使用，false为不使用。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsLinearText(const OH_Drawing_Font*);

/**
 * @brief 用于给字型设置文本倾斜。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象的指针。
 * @param skewX X轴相对于Y轴的倾斜度。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSkewX(OH_Drawing_Font*, float skewX);

/**
 * @brief 获取字型文本在x轴上的倾斜度。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回一个浮点数，表示x轴上的文本倾斜度。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetTextSkewX(const OH_Drawing_Font*);

/**
 * @brief 用于设置增加描边宽度以近似粗体字体效果。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象的指针。
 * @param isFakeBoldText 真为使能增加描边宽度，假为不使能。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetFakeBoldText(OH_Drawing_Font*, bool isFakeBoldText);

/**
 * @brief 获取是否增加笔画宽度以接近粗体字体。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回是否增加笔画宽度以接近粗体字体。true为增加，false为不增加。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsFakeBoldText(const OH_Drawing_Font*);

/**
 * @brief 用于设置字型在x轴上的缩放比例。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param scaleX 文本在x轴上的缩放比例。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetScaleX(OH_Drawing_Font*, float scaleX);

/**
 * @brief 获取字型在x轴上的缩放比例。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回文本在x轴上的缩放比例。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetScaleX(const OH_Drawing_Font*);

/**
 * @brief 用于设置字型轮廓效果。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_FontHinting不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param OH_Drawing_FontHinting 字型轮廓枚举类型{@link OH_Drawing_FontHinting}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetHinting(OH_Drawing_Font*, OH_Drawing_FontHinting);

/**
 * @brief 获取字型轮廓效果枚举类型。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return OH_Drawing_FontHinting 返回字型轮廓效果枚举类型{@link OH_Drawing_FontHinting}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontHinting OH_Drawing_FontGetHinting(const OH_Drawing_Font*);

/**
 * @brief 用于设置字型是否转换成位图处理。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param isEmbeddedBitmaps 设置字型是否转换成位图处理，true表示转换成位图处理，false表示不转换成位图处理。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetEmbeddedBitmaps(OH_Drawing_Font*, bool isEmbeddedBitmaps);

/**
 * @brief 获取字型是否转换成位图处理。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @return 返回字型是否转换成位图处理，true表示转换成位图处理，false表示不转换成位图处理。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsEmbeddedBitmaps(const OH_Drawing_Font*);

/**
 * @brief 用于销毁字型对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontDestroy(OH_Drawing_Font*);

/**
 * @brief 定义字体度量信息的结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_Font_Metrics {
    /** 指示哪些度量是有效的。 */
    uint32_t fFlags;
    /** 字符最高点到基线的最大距离。 */
    float top;
    /** 字符最高点到基线的推荐距离。 */
    float ascent;
    /** 字符最低点到基线的推荐距离。 */
    float descent;
    /** 字符最低点到基线的最大距离。 */
    float bottom;
    /** 行间距。 */
    float leading;
    /** 平均字符宽度，如果未知则为零。 */
    float avgCharWidth;
    /** 最大字符宽度，如果未知则为零。 */
    float maxCharWidth;
    /** 任何字形边界框原点左侧的最大范围，通常为负值；不推荐使用可变字体。 */
    float xMin;
    /** 任何字形边界框原点右侧的最大范围，通常为负值；不推荐使用可变字体。 */
    float xMax;
    /** 小写字母的高度，如果未知则为零，通常为负数。 */
    float xHeight;
    /** 大写字母的高度，如果未知则为零，通常为负数。 */
    float capHeight;
    /** 下划线粗细。 */
    float underlineThickness;
    /** 表示下划线的位置，即从基线到文字下方笔画顶部的垂直距离，通常为正值。 */
    float underlinePosition;
    /** 删除线粗细。 */
    float strikeoutThickness;
    /** 表示删除线的位置，即从基线到文字上方笔画底部的垂直距离，通常为负值。 */
    float strikeoutPosition;
} OH_Drawing_Font_Metrics;

/**
 * @brief 获取字体度量信息。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Font、OH_Drawing_Font_Metrics任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param OH_Drawing_Font_Metrics 指向字体度量信息对象{@link OH_Drawing_Font_Metrics}的指针。
 * @return 函数返回一个浮点数变量，表示建议的行间距。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetMetrics(OH_Drawing_Font*, OH_Drawing_Font_Metrics*);

/**
 * @brief 获取字型指定字形索引的矩形边界。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param glyphs 字形索引数组。
 * @param count 字形数组的长度。
 * @param bounds 矩形边界数组。
 * @return 函数返回执行错误码。\n
 * 返回OH_DRAWING_SUCCESS，表示执行成功。\n
 * 返回OH_DRAWING_ERROR_INVALID_PARAMETER，表示参数font、glyphs或bounds为空，或者count为零。
 * @since 18
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_FontGetBounds(const OH_Drawing_Font* font, const uint16_t* glyphs, uint32_t count,
    OH_Drawing_Array* bounds);

/**
 * @brief 获取字型指定字形索引的轮廓。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param font 指向字型对象{@link OH_Drawing_Font}的指针。
 * @param glyph 指定的字形索引。
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针, 用于存储得到的字形路径。
 * @return 函数返回执行错误码。\n
 * 返回OH_DRAWING_SUCCESS，表示执行成功。\n
 * 返回OH_DRAWING_ERROR_INVALID_PARAMETER，表示参数font或者path为空， 或者指定glyph不存在。
 * @since 18
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_FontGetPathForGlyph(const OH_Drawing_Font* font, uint16_t glyph,
    OH_Drawing_Path* path);

/**
 * @brief 获取文字轮廓路径。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param font 指示字型对象{@link OH_Drawing_Font}的指针。
 * @param text 指示要获取轮廓路径的文本字符串。
 * @param byteLength 指示要获取对应文本路径的字节长度，如果此字节长度大于text字符串的字节长度，会发生未定义行为。
 * @param encoding 指示文本编码格式，支持 UTF-8、UTF-16、UTF-32，以及字形索引，具体类型格式可见{@link OH_DRAWING_TextEncoding}。
 * @param x 指示文本在绘图区域内以原点为起始位置的X坐标。
 * @param y 指示文本在绘图区域内以原点为起始位置的Y坐标。
 * @param path 返回获取到的文字轮廓路径对象，作为出参使用。
 * @return 返回错误代码。\n
 * 如果操作成功，则返回 {@link OH_DRAWING_SUCCESS}。\n
 * 如果 font、text 或 path 中的任何一个为空指针，则返回 {@link OH_DRAWING_ERROR_INVALID_PARAMETER}。
 * @since 18
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_FontGetTextPath(const OH_Drawing_Font* font, const void* text, size_t byteLength,
    OH_Drawing_TextEncoding encoding, float x, float y, OH_Drawing_Path* path);

/**
 * @brief 设置字型中的字体是否跟随主题字体。设置跟随主题字体后，若系统启用主题字体并且字型未被设置字体，字型会使用该主题字体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param font 指示字型对象{@link OH_Drawing_Font}的指针。
 * @param followed 字型中的字体是否跟随主题字体，true表示跟随主题字体，false表示不跟随主题字体。
 * @return 函数返回执行错误码。\n
 * 返回OH_DRAWING_SUCCESS，表示执行成功。\n
 * 返回OH_DRAWING_ERROR_INVALID_PARAMETER，表示参数font为空。
 * @since 15
 */
OH_Drawing_ErrorCode OH_Drawing_FontSetThemeFontFollowed(OH_Drawing_Font* font, bool followed);

/**
 * @brief 获取字型中的字体是否跟随主题字体。默认不跟随主题字体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param font 指示字型对象{@link OH_Drawing_Font}的指针。
 * @param followed 返回字型中的字体是否跟随主题字体的结果，true表示跟随主题字体，false表示不跟随主题字体。作为出参使用。
 * @return 函数返回执行错误码。\n
 * 返回OH_DRAWING_SUCCESS，表示执行成功。\n
 * 返回OH_DRAWING_ERROR_INVALID_PARAMETER，表示参数font或者followed其中一个为空。
 * @since 15
 */
OH_Drawing_ErrorCode OH_Drawing_FontIsThemeFontFollowed(const OH_Drawing_Font* font, bool* followed);
#ifdef __cplusplus
}
#endif
/** @} */
#endif
