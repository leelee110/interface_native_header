/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_BITMAP_H
#define C_INCLUDE_DRAWING_BITMAP_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_bitmap.h
 *
 * @brief 文件中定义了与位图相关的功能函数。
 *
 * @include native_drawing/drawing_bitmap.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 结构体用于描述位图像素的格式，包括颜色类型和透明度类型。
 * 
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_BitmapFormat {
    /** 描述位图像素的存储格式。 */
    OH_Drawing_ColorFormat colorFormat;
    /** 描述位图像素的透明度分量。 */
    OH_Drawing_AlphaFormat alphaFormat;
} OH_Drawing_BitmapFormat;

/**
 * @brief 用于创建一个位图对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的位图对象。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Bitmap* OH_Drawing_BitmapCreate(void);

/**
 * @brief 用于销毁位图对象并回收该对象占有内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_BitmapDestroy(OH_Drawing_Bitmap*);

/**
 * @brief 用于创建一个位图对象，并将位图像素存储内存地址设置为开发者申请内存的地址。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Image_Info、pixels任意一个为NULL或者rowBytes等于0时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image_Info 指向图片信息对象{@link OH_Drawing_Image_Info}的指针。
 * @param pixels 指向像素存储的内存首地址，内存由开发者申请，保证有效性。
 * @param rowBytes 每行像素的大小，小于等于0时无效。
 * @return 函数返回一个指针，指针指向创建的位图对象{@link OH_Drawing_Bitmap}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Bitmap* OH_Drawing_BitmapCreateFromPixels(OH_Drawing_Image_Info*, void* pixels, uint32_t rowBytes);

/**
 * @brief 用于初始化位图对象的宽度和高度，并且为该位图设置像素格式。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap、OH_Drawing_BitmapFormat任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @param width 位图要初始化设置的宽度。
 * @param height 位图要初始化设置的高度。
 * @param OH_Drawing_BitmapFormat 位图要初始化设置的像素格式，包括像素的颜色类型和透明度类型。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_BitmapBuild(
    OH_Drawing_Bitmap*, const uint32_t width, const uint32_t height, const OH_Drawing_BitmapFormat*);

/**
 * @brief 用于获取指定位图的宽度。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @return 函数返回位图的宽度。
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_BitmapGetWidth(OH_Drawing_Bitmap*);

/**
 * @brief 用于获取指定位图的高度。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @return 函数返回位图的高度。
 * @since 8
 * @version 1.0
 */
uint32_t OH_Drawing_BitmapGetHeight(OH_Drawing_Bitmap*);

/**
 * @brief 用于获取指定位图的像素存储格式。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @return 函数返回位图的像素存储格式，支持格式参考{@link OH_Drawing_ColorFormat}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_ColorFormat OH_Drawing_BitmapGetColorFormat(OH_Drawing_Bitmap*);

/**
 * @brief 用于获取指定位图的像素透明度分量。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @return 函数返回位图的像素透明度分量，支持格式参考{@link OH_Drawing_AlphaFormat}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_AlphaFormat OH_Drawing_BitmapGetAlphaFormat(OH_Drawing_Bitmap*);

/**
 * @brief 用于获取指定位图的像素地址，可以通过像素地址获取到位图的像素数据。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @return 函数返回位图的像素地址。
 * @since 8
 * @version 1.0
 */
void* OH_Drawing_BitmapGetPixels(OH_Drawing_Bitmap*);

/**
 * @brief 用于获取指定位图的信息。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap、OH_Drawing_Image_Info任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象{@link OH_Drawing_Bitmap}的指针。
 * @param OH_Drawing_Image_Info 指向图片信息对象{@link OH_Drawing_Image_Info}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_BitmapGetImageInfo(OH_Drawing_Bitmap*, OH_Drawing_Image_Info*);

/**
 * @brief 将位图中的矩形区域像素数据读取到指定的内存缓冲区中。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Bitmap、dstInfo、dstPixels任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Bitmap 指向位图对象{@link OH_Drawing_Bitmap}的指针。
 * @param OH_Drawing_Image_Info 指向图片信息对象{@link OH_Drawing_Image_Info}的指针。
 * @param dstPixels 目标像素存储区域。
 * @param dstRowBytes 目标像素数据每行的字节数，应大于或等于图片信息对象中的最小每行字节数。
 * @param srcX 源位图中读取像素数据的起始x轴坐标，应小于源位图的宽度。
 * @param srcY 源位图中读取像素数据的起始y轴坐标，应小于源位图的高度。
 * @return 返回接口调用成功与否的结果。true表示复制成功，false表示复制失败。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_BitmapReadPixels(OH_Drawing_Bitmap*, const OH_Drawing_Image_Info* dstInfo,
    void* dstPixels, size_t dstRowBytes, int32_t srcX, int32_t srcY);

#ifdef __cplusplus
}
#endif
/** @} */
#endif