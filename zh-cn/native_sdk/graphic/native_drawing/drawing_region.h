/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_REGION_H
#define C_INCLUDE_DRAWING_REGION_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_region.h
 *
 * @brief 定义了与区域相关的功能函数，包括区域的创建，边界设置和销毁等。
 *
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 区域操作类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_RegionOpMode {
    /**
     * 差集操作。
     */
    REGION_OP_MODE_DIFFERENCE,
    /**
     * 交集操作。
     */
    REGION_OP_MODE_INTERSECT,
    /**
     * 并集操作。
     */
    REGION_OP_MODE_UNION,
    /**
     * 异或操作。
     */
    REGION_OP_MODE_XOR,
    /**
     * 反向差集操作。
     */
    REGION_OP_MODE_REVERSE_DIFFERENCE,
    /**
     * 替换操作。
     */
    REGION_OP_MODE_REPLACE,
} OH_Drawing_RegionOpMode;

/**
 * @brief 用于创建一个区域对象，实现更精确的图形控制。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的区域对象{@link OH_Drawing_Region}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Region* OH_Drawing_RegionCreate(void);

/**
 * @brief 判断区域是否包含指定坐标点。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * region为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region 指向区域对象{@link OH_Drawing_Region}的指针。
 * @param int32_t 表示指定坐标点的x轴坐标。
 * @param int32_t 表示指定坐标点的y轴坐标。
 * @return 返回区域是否包含指定坐标点。true表示区域包含该坐标点，false表示区域不包含该坐标点。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionContains(OH_Drawing_Region* region, int32_t x, int32_t y);

/**
 * @brief 将两个区域按照指定的区域操作类型合并。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * region、dst任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * op不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region 指向区域对象{@link OH_Drawing_Region}的指针，操作完成后的区域结果将会保存在此区域对象中。
 * @param other 指向区域对象{@link OH_Drawing_Region}的指针。
 * @param op 区域操作枚举类型，支持可选的具体模式可见{@link OH_Drawing_RegionOpMode}枚举。
 * @return 返回操作后的区域是否为空。true表示区域不为空，false表示区域为空。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionOp(OH_Drawing_Region* region, const OH_Drawing_Region* other, OH_Drawing_RegionOpMode op);

/**
 * @brief 用于尝试给区域对象设置矩形边界。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * region、rect任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region 指向区域对象{@link OH_Drawing_Region}的指针。
 * @param rect 指向矩形对象的指针。
 * @return 返回区域对象设置矩形边界是否成功的结果。true表示设置矩形边界成功，false表示设置矩形边界失败。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionSetRect(OH_Drawing_Region* region, const OH_Drawing_Rect* rect);

/**
 * @brief 给区域对象设置为指定区域内路径表示的范围。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * region、path、clip任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param region 指向区域对象{@link OH_Drawing_Region}的指针。
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param clip 指向区域对象{@link OH_Drawing_Region}的指针。
 * @return 返回操作后的区域是否为空。true表示区域不为空，false表示区域为空。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RegionSetPath(OH_Drawing_Region* region, const OH_Drawing_Path* path, const OH_Drawing_Region* clip);

/**
 * @brief 用于销毁区域对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Region 指向区域对象{@link OH_Drawing_Region}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RegionDestroy(OH_Drawing_Region*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
