/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_SAMPLING_OPTIONS_H
#define C_INCLUDE_DRAWING_SAMPLING_OPTIONS_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_sampling_options.h
 *
 * @brief 文件中定义了与采样相关的功能函数。用于图片或者纹理等图像的采样。
 *
 * @include native_drawing/drawing_sampling_options.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 过滤模式枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FilterMode {
    /** 邻近过滤模式。 */
    FILTER_MODE_NEAREST,
    /** 线性过滤模式。 */
    FILTER_MODE_LINEAR,
} OH_Drawing_FilterMode;

/**
 * @brief 多级渐远纹理模式枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_MipmapMode {
    /** 忽略多级渐远纹理级别。 */
    MIPMAP_MODE_NONE,
    /** 邻近多级渐远级别采样。 */
    MIPMAP_MODE_NEAREST,
    /** 两个邻近多级渐远纹理之间，线性插值采样。 */
    MIPMAP_MODE_LINEAR,
} OH_Drawing_MipmapMode;

/**
 * @brief 创建一个采样选项对象。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_MipmapMode不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FilterMode 过滤采样模式{@link OH_Drawing_FilterMode}。
 * @param OH_Drawing_MipmapMode 多级渐远纹理采样模式{@link OH_Drawing_MipmapMode}。
 * @return 函数会返回一个指针，指针指向创建的采样选项对象{@link OH_Drawing_SamplingOptions}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_SamplingOptions* OH_Drawing_SamplingOptionsCreate(OH_Drawing_FilterMode, OH_Drawing_MipmapMode);

/**
 * @brief 销毁采样选项对象并回收该对象占有内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_SamplingOptions 指向采样选项对象{@link OH_Drawing_SamplingOptions}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SamplingOptionsDestroy(OH_Drawing_SamplingOptions*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
