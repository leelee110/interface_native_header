/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PATH_H
#define C_INCLUDE_DRAWING_PATH_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_path.h
 *
 * @brief 文件中定义了与自定义路径相关的功能函数。
 *
 * @include native_drawing/drawing_path.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 添加闭合轮廓方向枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathDirection {
    /** 顺时针方向添加闭合轮廓。 */
    PATH_DIRECTION_CW,
    /** 逆时针方向添加闭合轮廓。 */
    PATH_DIRECTION_CCW,
} OH_Drawing_PathDirection;

/**
 * @brief 定义路径的填充类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathFillType {
    /** 绘制区域中的任意一点，向任意方向射出一条射线，对于射线和路径的所有交点，初始计数为0，\n
     *  遇到每个顺时针的交点（路径从射线的左边向右穿过），计数加1，遇到每个逆时针的交点（路径从射线的右边向左穿过），\n
     *  计数减1，若最终的计数结果为0，则认为这个点在路径内部，需要被涂色；若计数为0则不被涂色。 */
    PATH_FILL_TYPE_WINDING,
    /** 绘制区域中的任意一点，向任意方向射出一条射线，若这条射线和路径相交的次数是奇数，则这个点被认为在路径内部，需要被涂色；若是偶数则不被涂色。 */
    PATH_FILL_TYPE_EVEN_ODD,
    /** PATH_FILL_TYPE_WINDING 涂色规则取反。 */
    PATH_FILL_TYPE_INVERSE_WINDING,
    /** PATH_FILL_TYPE_EVEN_ODD 涂色规则取反。 */
    PATH_FILL_TYPE_INVERSE_EVEN_ODD,
} OH_Drawing_PathFillType;

/**
 * @brief 用于指定路径添加模式的枚举类型。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathAddMode {
    /** 以追加的方式添加路径。 */
    PATH_ADD_MODE_APPEND,
    /** 如果之前的路径未闭合，则添加直线闭合路径。 */
    PATH_ADD_MODE_EXTEND,
} OH_Drawing_PathAddMode;

/**
 * @brief 路径操作类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathOpMode {
    /**
     * 差集操作。
     */
    PATH_OP_MODE_DIFFERENCE,
    /**
     * 交集操作。
     */
    PATH_OP_MODE_INTERSECT,
    /**
     * 并集操作。
     */
    PATH_OP_MODE_UNION,
    /**
     * 异或操作。
     */
    PATH_OP_MODE_XOR,
    /**
     * 反向差集操作。
     */
    PATH_OP_MODE_REVERSE_DIFFERENCE,
} OH_Drawing_PathOpMode;

/**
 * @brief 路径测量获取相应矩阵信息维度枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathMeasureMatrixFlags {
    /**
     * 获取位置信息对应的矩阵。
     */
    GET_POSITION_MATRIX,
    /**
     * 获取切线信息对应的矩阵。
     */
    GET_TANGENT_MATRIX,
    /**
     * 获取位置和切线信息对应的矩阵。
     */
    GET_POSITION_AND_TANGENT_MATRIX,
} OH_Drawing_PathMeasureMatrixFlags;

/**
 * @brief 用于创建一个路径对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的路径对象。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCreate(void);

/**
 * @brief 创建一个路径对象副本{@link OH_Drawing_Path}，用于拷贝一个已有路径对象。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @return 函数返回一个指针，指针指向创建的路径对象副本{@link OH_Drawing_Path}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCopy(OH_Drawing_Path*);

/**
 * @brief 用于销毁路径对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathDestroy(OH_Drawing_Path*);

/**
 * @brief 用于设置自定义路径的起始点位置。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param x 起始点的横坐标。
 * @param y 起始点的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathMoveTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 用于添加一条从路径的最后点位置（若路径没有内容则默认为 (0, 0)）到目标点位置的线段。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param x 目标点的横坐标。
 * @param y 目标点的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathLineTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 用于给路径添加一段弧线，绘制弧线的方式为角度弧，该方式首先会指定一个矩形边框，\n
 * 矩形边框的内切椭圆将会被用来截取弧线，然后会指定一个起始角度和扫描度数，\n
 * 从起始角度扫描截取的椭圆周长一部分即为绘制的弧线。若路径有内容则会默认添加一条从路径的最后点位置到弧线起始点位置的线段。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param x1 包围椭圆的矩形左上角点位置的横坐标。
 * @param y1 包围椭圆的矩形左上角点位置的纵坐标。
 * @param x2 包围椭圆的矩形右下角点位置的横坐标。
 * @param y2 包围椭圆的矩形右下角点位置的纵坐标。
 * @param startDeg 起始的角度。角度的起始方向（0°）为x轴正方向。
 * @param sweepDeg 扫描的度数，为正数时顺时针扫描，为负数时逆时针扫描。实际扫描的度数为该入参对360取模的结果。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathArcTo(OH_Drawing_Path*, float x1, float y1, float x2, float y2, float startDeg, float sweepDeg);

/**
 * @brief 用于添加一条从路径最后点位置（若路径没有内容则默认为 (0, 0)）到目标点位置的二阶贝塞尔曲线。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param ctrlX 控制点位置的横坐标。
 * @param ctrlY 控制点位置的纵坐标。
 * @param endX 目标点位置的横坐标。
 * @param endY 目标点位置的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathQuadTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY);

/**
 * @brief 在当前路径上添加一条路径终点（若路径没有内容则默认为 (0, 0)）到目标点位置的圆锥曲线段，其控制点为 (ctrlX, ctrlY)，结束点为 (endX, endY)。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param ctrlX 控制点位置的横坐标。
 * @param ctrlY 控制点位置的纵坐标。
 * @param endX 结束点位置的横坐标。
 * @param endY 结束点位置的纵坐标。
 * @param weight 表示曲线的权重，决定了曲线的形状，越大越接近控制点。\n
 * 若小于等于0则等同于使用{@link OH_Drawing_PathLineTo}添加一条到结束点的线段，\n
 * 若为1则等同于{@link OH_Drawing_PathQuadTo}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathConicTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY, float weight);

/**
 * @brief 用于添加一条从路径最后点位置（若路径没有内容则默认为 (0, 0)）到目标点位置的三阶贝塞尔圆滑曲线。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param ctrlX1 第一个控制点位置的横坐标。
 * @param ctrlY1 第一个控制点位置的纵坐标。
 * @param ctrlX2 第二个控制点位置的横坐标。
 * @param ctrlY2 第二个控制点位置的纵坐标。
 * @param endX 目标点位置的横坐标。
 * @param endY 目标点位置的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathCubicTo(
    OH_Drawing_Path*, float ctrlX1, float ctrlY1, float ctrlX2, float ctrlY2, float endX, float endY);

/**
 * @brief 用于设置一个相对于当前路径终点（若路径没有内容则默认为 (0, 0)）的路径起始点位置。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param x 相对于当前路径终点的x轴偏移量，正数往x轴正方向偏移，负数往x轴负方向偏移。
 * @param y 相对于当前路径终点的y轴偏移量，正数往y轴正方向偏移，负数往y轴负方向偏移。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRMoveTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 使用相对位置在当前路径上添加一条当前路径终点（若路径没有内容则默认为 (0, 0)）到目标点位置的线段。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param x 相对于当前路径终点的x轴偏移量，用于指定目标点的横坐标。
 * @param y 相对于当前路径终点的y轴偏移量，用于指定目标点的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRLineTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief 使用相对位置在当前路径上添加一条当前路径终点（若路径没有内容则默认为 (0, 0)）到目标点位置的二阶贝塞尔曲线。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param ctrlX 相对于路径终点的x轴偏移量，用于指定控制点的横坐标。
 * @param ctrlY 相对于路径终点的y轴偏移量，用于指定控制点的纵坐标。
 * @param endX 相对于路径终点的x轴偏移量，用于指定目标点的横坐标。
 * @param endY 相对于路径终点的y轴偏移量，用于指定目标点的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRQuadTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY);

/**
 * @brief 使用相对位置在当前路径上添加一条路径终点（若路径没有内容则默认为 (0, 0)）到目标点位置的圆锥曲线段。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param ctrlX 相对于路径终点的x轴偏移量，用于指定控制点的横坐标。
 * @param ctrlY 相对于路径终点的y轴偏移量，用于指定控制点的纵坐标。
 * @param endX 相对于路径终点的x轴偏移量，用于指定目标点的横坐标。
 * @param endY 相对于路径终点的y轴偏移量，用于指定目标点的纵坐标。
 * @param weight 表示曲线的权重，决定了曲线的形状，越大越接近控制点。\n
 * 若小于等于0则等同于使用{@link OH_Drawing_PathRLineTo}添加一条到结束点的线段，\n
 * 若为1则等同于{@link OH_Drawing_PathRQuadTo}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRConicTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY, float weight);

/**
 * @brief 使用相对位置在当前路径上添加一条当前路径终点（若路径没有内容则默认为 (0, 0)）到目标点位置的三阶贝塞尔圆滑曲线。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param ctrlX1 相对于路径终点的x轴偏移量，用于指定第一个控制点的横坐标。
 * @param ctrlY1 相对于路径终点的y轴偏移量，用于指定第一个控制点的纵坐标。
 * @param ctrlX2 相对于路径终点的x轴偏移量，用于指定第二个控制点的横坐标。
 * @param ctrlY2 相对于路径终点的y轴偏移量，用于指定第二个控制点的纵坐标。
 * @param endX 相对于路径终点的x轴偏移量，用于指定目标点的横坐标。
 * @param endY 相对于路径终点的y轴偏移量，用于指定目标点的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRCubicTo(
    OH_Drawing_Path*, float ctrlX1, float ctrlY1, float ctrlX2, float ctrlY2, float endX, float endY);

/**
 * @brief 按指定方向，将矩形添加到路径中，添加的路径的起始点为矩形左上角。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathDirection不在枚举范围内返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param left 矩形左上角的x轴坐标。
 * @param top 矩形左上角的y轴坐标。
 * @param right 矩形右下角的x轴坐标。
 * @param bottom 矩形右下角的y轴坐标。
 * @param OH_Drawing_PathDirection 路径方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRect(OH_Drawing_Path*, float left, float top,
    float right, float bottom, OH_Drawing_PathDirection);

/**
 * @brief 按指定方向，向路径添加矩形轮廓。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、OH_Drawing_Rect任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathDirection不在枚举范围内返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param OH_Drawing_PathDirection 表示绘制方向{@link OH_Drawing_PathDirection}。
 * @param start 起始点的位置，表示从矩形的哪个角开始绘制路径。0：左上角，1：右上角，2：右下角，3：左下角。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRectWithInitialCorner(OH_Drawing_Path*, const OH_Drawing_Rect*,
    OH_Drawing_PathDirection, uint32_t start);

/**
 * @brief 按指定方向，向路径添加圆角矩形轮廓。路径添加方向为顺时针时，起始点位于圆角矩形左下方圆角与左边界的交点；路径添加方向为逆时针时，起始点位于圆角矩形左上方圆角与左边界的交点。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、roundRect任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathDirection不在枚举范围内返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param roundRect 指向圆角矩形对象{@link OH_Drawing_RoundRect}的指针。
 * @param OH_Drawing_PathDirection 路径方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRoundRect(OH_Drawing_Path*, const OH_Drawing_RoundRect* roundRect, OH_Drawing_PathDirection);

/**
 * @brief 将椭圆添加到路径中，其中矩形对象作为椭圆的外切矩形区域，绘制方向用来指定绘制时是顺时针或者逆时针方向。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、OH_Drawing_Rect任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathDirection不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param start 表示椭圆初始点的索引。
 * @param OH_Drawing_PathDirection 表示绘制方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddOvalWithInitialPoint(OH_Drawing_Path*, const OH_Drawing_Rect*,
    uint32_t start, OH_Drawing_PathDirection);

/**
 * @brief 将圆弧添加到路径中，作为新轮廓的起点。从起始角度到扫描角度添加弧，添加的弧是矩形内切椭圆的一部分，如果扫描角度<= -360°，或>= 360°，并且起始角度对90取模接近于0，则添加椭圆而不是弧。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、OH_Drawing_Rect任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param startAngle 弧的起始角度，单位为度。
 * @param sweepAngle 扫描的度数，为正数时顺时针扫描，为负数时逆时针扫描。实际扫描的度数为该入参对360取模的结果。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddArc(OH_Drawing_Path*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief 将源路径矩阵变换后，添加到当前路径中。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、src任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向当前路径对象{@link OH_Drawing_Path}的指针。
 * @param src 指向源路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针，为NULL时表示单位矩阵。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPath(OH_Drawing_Path*, const OH_Drawing_Path* src, const OH_Drawing_Matrix*);

/**
 * @brief 将源路径矩阵变换后，以规定模式添加到当前路径中。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、src任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathAddMode不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向当前路径对象{@link OH_Drawing_Path}的指针。
 * @param src 指向源路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针，为NULL表示单位矩阵。
 * @param OH_Drawing_PathAddMode 路径添加模式{@link OH_Drawing_PathAddMode}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPathWithMatrixAndMode(OH_Drawing_Path* path, const OH_Drawing_Path* src,
    const OH_Drawing_Matrix*, OH_Drawing_PathAddMode);

/**
 * @brief 将源路径以规定模式添加到当前路径中。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、src任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathAddMode不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向当前路径对象{@link OH_Drawing_Path}的指针。
 * @param src 指向源路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_PathAddMode 路径添加模式{@link OH_Drawing_PathAddMode}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPathWithMode(OH_Drawing_Path* path, const OH_Drawing_Path* src, OH_Drawing_PathAddMode);

/**
 * @brief 将源路径偏移后，以规定模式添加到当前路径中。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、src任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathAddMode不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向当前路径对象{@link OH_Drawing_Path}的指针。
 * @param src 指向源路径对象{@link OH_Drawing_Path}的指针。
 * @param dx 添加到目标路径横坐标的偏移量。
 * @param dy 添加到目标路径纵坐标的偏移量。
 * @param OH_Drawing_PathAddMode 路径添加模式{@link OH_Drawing_PathAddMode}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPathWithOffsetAndMode(OH_Drawing_Path* path, const OH_Drawing_Path* src,
    float dx, float dy, OH_Drawing_PathAddMode);

/**
 * @brief 向路径添加多边形。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、points任意一个为NULL或者count等于0时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向当前路径对象{@link OH_Drawing_Path}的指针。
 * @param points 表示多边形的顶点坐标数组。
 * @param count 表示多边形顶点坐标数组的大小。
 * @param isClosed 是否添加连接起始点和终止点的线，true表示添加，false表示不添加。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPolygon(OH_Drawing_Path* path, const OH_Drawing_Point2D* points, uint32_t count, bool isClosed);

/**
 * @brief 按指定方向，向路径添加圆形。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * radius小于等于0时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE；\n
 * OH_Drawing_PathDirection不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param x 表示圆心的x轴坐标。
 * @param y 表示圆心的y轴坐标。
 * @param radius 表示圆形的半径。
 * @param OH_Drawing_PathDirection 路径方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddCircle(OH_Drawing_Path* path, float x, float y, float radius, OH_Drawing_PathDirection);

/**
 * @brief 按指定方向，向路径添加椭圆。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、OH_Drawing_Rect任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathDirection不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param OH_Drawing_PathDirection 路径方向{@link OH_Drawing_PathDirection}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddOval(OH_Drawing_Path*, const OH_Drawing_Rect*, OH_Drawing_PathDirection);

/**
 * @brief 解析SVG字符串表示的路径。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、str任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param str 表示SVG字符串。
 * @return 函数返回解析SVG字符串是否成功。true表示成功，false表示不成功。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathBuildFromSvgString(OH_Drawing_Path* path, const char* str);

/**
 * @brief 判断指定坐标点是否被路径包含，判定是否被路径包含的规则参考{@link OH_Drawing_PathFillType}。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param x x轴上坐标点。
 * @param y y轴上坐标点。
 * @return 函数返回true表示点在路径内，函数返回false表示点在路径外。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathContains(OH_Drawing_Path*, float x, float y);

/**
 * @brief 对路径进行矩阵变换。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、OH_Drawing_Matrix任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。 
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathTransform(OH_Drawing_Path*, const OH_Drawing_Matrix*);

/**
 * @brief 对路径进行矩阵变换。用转换后的路径替换目标路径，如果目标路径为NULL，则替换源路径。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * src、OH_Drawing_Matrix任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param src 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param dst 指向目标路径对象{@link OH_Drawing_Path}的指针。
 * @param applyPerspectiveClip 表示变换路径是否应用透视裁剪。true表示应用透视裁剪，false表示不用透视裁剪。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathTransformWithPerspectiveClip(OH_Drawing_Path* src, const OH_Drawing_Matrix*,
    OH_Drawing_Path* dst, bool applyPerspectiveClip);

/**
 * @brief 设置路径的填充类型，这个决定了路径内部区域的定义方式。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * OH_Drawing_PathFillType不在枚举范围内返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_PathFillType 路径填充规则{@link OH_Drawing_PathFillType}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathSetFillType(OH_Drawing_Path*, OH_Drawing_PathFillType);

/**
 * @brief 获取当前路径的长度。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param forceClosed 表示是否按照闭合路径测量。true表示测量时路径会被强制视为已闭合，false表示会根据路径的实际闭合状态测量。
 * @return 函数返回当前路径的长度。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_PathGetLength(OH_Drawing_Path*, bool forceClosed);

/**
 * @brief 获取包含路径的最小边界框。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path、OH_Drawing_Rect任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathGetBounds(OH_Drawing_Path*, OH_Drawing_Rect*);

/**
 * @brief 用于闭合路径，会添加一条从路径起点位置到最后点位置的线段。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathClose(OH_Drawing_Path*);

/**
 * @brief 获取路径是否闭合。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param forceClosed 表示是否按照闭合路径测量，true表示测量时路径会被强制视为已闭合，false表示会根据路径的实际闭合状态测量。
 * @return 返回路径是否闭合。true表示路径的测量结果为已闭合，false表示路径的测量结果为未闭合。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathIsClosed(OH_Drawing_Path* path, bool forceClosed);

/**
 * @brief 获取距路径起始点指定距离的坐标点和切线值。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、position、tangent任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param forceClosed 表示是否按照闭合路径测量，true表示测量时路径会被强制视为已闭合，false表示会根据路径的实际闭合状态测量。
 * @param distance 表示距离起始点的距离，小于0时会被视为0处理，大于路径长度时会被视为路径长度处理。
 * @param position 表示距路径起始点指定距离的坐标点。
 * @param tangent 表示距路径起始点指定距离的切线值，tangent.x表示该点切线的余弦值，tangent.y表示该点切线的正弦值。
 * @return 返回测量是否成功。true表示成功，false表示失败。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathGetPositionTangent(OH_Drawing_Path* path, bool forceClosed,
    float distance, OH_Drawing_Point2D* position, OH_Drawing_Point2D* tangent);

/**
 * @brief 将两个路径按照指定的路径操作类型合并。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、srcPath任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * op不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针，操作完成后的路径结果将会保存在此路径对象中。
 * @param other 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param op 路径操作枚举类型，支持可选的具体模式可见{@link OH_Drawing_PathOpMode}枚举。
 * @return 返回操作后的路径是否为空。true表示路径不为空，false表示路径为空。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathOp(OH_Drawing_Path* path, const OH_Drawing_Path* other, OH_Drawing_PathOpMode op);

/**
 * @brief 获取距路径起始点指定距离的相应变换矩阵。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * path、matrix任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER；\n
 * flag不在枚举范围内时返回OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向路径对象{@link OH_Drawing_Path}的指针。
 * @param forceClosed 表示是否按照闭合路径测量，true表示测量时路径会被强制视为已闭合，false表示会根据路径的实际闭合状态测量。
 * @param distance 表示距离起始点的距离，小于0时会被视为0处理，大于路径长度时会被视为路径长度处理。
 * @param matrix 表示要获取的变换矩阵。
 * @param flag 矩阵信息维度枚举，支持可选的具体模式可见{@link OH_Drawing_PathMeasureMatrixFlags}枚举。
 * @return 返回获取变换矩阵是否成功。true表示获取成功，false表示获取失败，失败的原因可能是path为NULL或者长度为0。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathGetMatrix(OH_Drawing_Path* path, bool forceClosed,
    float distance, OH_Drawing_Matrix* matrix, OH_Drawing_PathMeasureMatrixFlags flag);

/**
 * @brief 将路径中的所有点沿着x轴和y轴方向偏移一定距离，并将结果存储到目标路径对象中。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path 指向当前路径对象{@link OH_Drawing_Path}的指针。
 * @param dst 指向目标路径对象{@link OH_Drawing_Path}的指针，为NULL时会将结果存储到当前路径对象中。
 * @param dx x轴方向的偏移量。
 * @param dy y轴方向的偏移量。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathOffset(OH_Drawing_Path* path, OH_Drawing_Path* dst, float dx, float dy);

/**
 * @brief 用于重置自定义路径数据。
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。
 * OH_Drawing_Path为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathReset(OH_Drawing_Path*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif