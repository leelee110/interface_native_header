/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DRAWING_TEXT_FONT_DESCRIPTOR_H
#define DRAWING_TEXT_FONT_DESCRIPTOR_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_font_descriptor.h
 *
 * @brief 定义了字体信息的相关接口，比如获取字体信息，查找指定字体等。
 *
 * @include native_drawing/drawing_text_font_descriptor.h
 * @library libnative_drawing.so
 * @since 14
 * @version 1.0
 */

#include "drawing_text_typography.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 字体类型的枚举。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @since 14
 */
typedef enum OH_Drawing_SystemFontType {
    /** 所有字体类型。 */
    ALL = 1 << 0,
    /** 系统字体类型。 */
    GENERIC = 1 << 1,
    /** 风格字体类型 */
    STYLISH = 1 << 2,
    /** 用户已安装字体类型。 */
    INSTALLED = 1 << 3,
    /**
     * 自定义字体类型。
     * @since 18
     * */
    CUSTOMIZED = 1 << 4,
} OH_Drawing_SystemFontType;

/**
 * @brief 获取与指定字体描述符匹配的所有系统字体描述符，其中{@link OH_Drawing_FontDescriptor}的path字段不作为有效的匹配字段，其余字段不是默认值时生效。\n
 * 如果参数{@link OH_Drawing_FontDescriptor}的所有字段都是默认值，则获取所有系统字体描述符。\n
 * 如果匹配失败，返回NULL。不再需要{@link OH_Drawing_FontDescriptor}时，请使用{@link OH_Drawing_DestroyFontDescriptors}接口释放该对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontDescriptor {@link OH_Drawing_FontDescriptor}指针。建议使用{@link OH_Drawing_CreateFontDescriptor}获得有效的{@link OH_Drawing_FontDescriptor}实例，如果自己创建{@link OH_Drawing_FontDescriptor}实例，请确保不用于匹配的字段是默认值。
 * @param size_t 表示返回值数组的成员个数。
 * @return {@link OH_Drawing_FontDescriptor}数组，释放时请使用{@link OH_Drawing_DestroyFontDescriptors}。
 * @since 18
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_MatchFontDescriptors(OH_Drawing_FontDescriptor*, size_t*);

/**
 * @brief 释放字体描述符{@link OH_Drawing_FontDescriptor}数组。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontDescriptor {@link OH_Drawing_FontDescriptor}数组。
 * @param size_t {@link OH_Drawing_FontDescriptor}数组的成员个数。
 * @since 18
 * @version 1.0
 */
void OH_Drawing_DestroyFontDescriptors(OH_Drawing_FontDescriptor*, size_t);

/**
 * @brief 根据字体名称和字体类型获取指定的字体描述符，支持系统字体、风格字体和用户已安装字体。\n
 * 字体描述符是描述字体特征的一种数据结构，它包含了定义字体外观和属性的详细信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_String 表示指向字体名称字符串{@link OH_Drawing_String}的指针。
 * @param OH_Drawing_SystemFontType 表示字体类型的枚举值{@link OH_Drawing_SystemFontType}。
 * @return 指向字体描述符对象{@link OH_Drawing_FontDescriptor}的指针，不再需要{@link OH_Drawing_FontDescriptor}时，请使用{@link OH_Drawing_DestroyFontDescriptor}接口释放该对象的指针。
 * @since 14
 */
OH_Drawing_FontDescriptor* OH_Drawing_GetFontDescriptorByFullName(const OH_Drawing_String*, OH_Drawing_SystemFontType);

/**
 * @brief 根据字体类型获取对应字体的字体名称数组。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_SystemFontType 表示字体类型的枚举值{@link OH_Drawing_SystemFontType}。
 * @return 返回对应字体类型的字体名称数组{@link OH_Drawing_Array}的指针，不再需要{@link OH_Drawing_Array}时，请使用{@link OH_Drawing_DestroySystemFontFullNames}接口释放该对象的指针。
 * @since 14
 */
OH_Drawing_Array* OH_Drawing_GetSystemFontFullNamesByType(OH_Drawing_SystemFontType);

/**
 * @brief 在字体名称数组中通过索引获取对应位置的字体名称。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Array 表示字体名称数组{@link OH_Drawing_Array}的指针。
 * @param size_t 数组的索引。
 * @return 返回对应索引的字体名称{@link OH_Drawing_String}的指针。
 * @since 14
 */
const OH_Drawing_String* OH_Drawing_GetSystemFontFullNameByIndex(OH_Drawing_Array*, size_t);

/**
 * @brief 释放通过字体类型获取的对应字体的字体名称数组占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Array 表示字体名称数组{@link OH_Drawing_Array}的指针。
 * @since 14
 */
void OH_Drawing_DestroySystemFontFullNames(OH_Drawing_Array*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif