/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FILTER_H
#define C_INCLUDE_DRAWING_FILTER_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_filter.h
 *
 * @brief 声明与绘图模块中的滤波器对象相关的函数。
 *
 * @include native_drawing/drawing_filter.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 创建一个滤波器对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 返回创建的滤波器对象的指针。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Filter* OH_Drawing_FilterCreate(void);

/**
 * @brief 为滤波器对象设置图像滤波器对象。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Filter为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter 指示指向滤波器对象{@link OH_Drawing_Filter}的指针。
 * @param OH_Drawing_ImageFilter 指示指向图像滤波器{@link OH_Drawing_ImageFilter}对象的指针，为NULL表示清空滤波器对象中的图像滤波器效果。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FilterSetImageFilter(OH_Drawing_Filter*, OH_Drawing_ImageFilter*);

/**
 * @brief 为滤波器对象设置蒙版滤波器对象。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Filter为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter 指示指向滤波器对象{@link OH_Drawing_Filter}的指针。
 * @param OH_Drawing_MaskFilter 指示指向蒙版滤波器对象{@link OH_Drawing_ColorFilter}的指针，为NULL表示清空滤波器对象中的蒙版滤波器效果。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FilterSetMaskFilter(OH_Drawing_Filter*, OH_Drawing_MaskFilter*);

/**
 * @brief 为滤波器对象设置颜色滤波器对象。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Filter为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter 指示指向滤波器对象{@link OH_Drawing_Filter}的指针。
 * @param OH_Drawing_ColorFilter 指示指向颜色滤波器对象{@link OH_Drawing_ColorFilter}的指针，为NULL表示清空滤波器对象中的颜色滤波器效果。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FilterSetColorFilter(OH_Drawing_Filter*, OH_Drawing_ColorFilter*);

/**
 * @brief 从滤波器对象获取颜色滤波器对象。\n
 *
 * 本接口会产生错误码，可以通过{@link OH_Drawing_ErrorCodeGet}查看错误码的取值。\n
 * OH_Drawing_Filter、OH_Drawing_ColorFilter任意一个为NULL时返回OH_DRAWING_ERROR_INVALID_PARAMETER。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter 指示指向滤波器对象{@link OH_Drawing_Filter}的指针。
 * @param OH_Drawing_ColorFilter 指示指向颜色滤波器对象{@link OH_Drawing_ColorFilter}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FilterGetColorFilter(OH_Drawing_Filter*, OH_Drawing_ColorFilter*);

/**
 * @brief 销毁滤波器对象，并收回该对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter 指示指向滤波器对象{@link OH_Drawing_Filter}的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FilterDestroy(OH_Drawing_Filter*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif