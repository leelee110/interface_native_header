/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_GPU_CONTEXT_H
#define C_INCLUDE_DRAWING_GPU_CONTEXT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。\n
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_gpu_context.h
 *
 * @brief 声明与绘图模块中的图形处理器上下文对象相关的函数。
 *
 * @include native_drawing/drawing_gpu_context.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义有关图形处理器上下文的选项。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_GpuContextOptions {
    /** 用于控制是否启用路径蒙版缓存，如果为true，则允许缓存路径蒙版纹理，如果为false，则不允许。*/
    bool allowPathMaskCaching;
} OH_Drawing_GpuContextOptions;

/**
 * @brief 用于创建一个使用OpenGL作为后端接口的图形处理器上下文对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_GpuContextOptions 图形处理器上下文选项{@link OH_Drawing_GpuContextOptions}。
 * @return 返回一个指针，指针指向创建的图形处理器上下文对象{@link OH_Drawing_GpuContext}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_GpuContext* OH_Drawing_GpuContextCreateFromGL(OH_Drawing_GpuContextOptions);

/**
 * @brief 用于创建一个图形处理器上下文对象, 使用的后端类型取决于运行设备。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 返回一个指针，指针指向创建的图形处理器上下文对象{@link OH_Drawing_GpuContext}。
 * @since 16
 * @version 1.0
 */
OH_Drawing_GpuContext* OH_Drawing_GpuContextCreate();

/**
 * @brief 用于销毁图形处理器上下文对象并回收该对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_GpuContext 指向图形处理器上下文对象的指针{@link OH_Drawing_GpuContext}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_GpuContextDestroy(OH_Drawing_GpuContext*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif