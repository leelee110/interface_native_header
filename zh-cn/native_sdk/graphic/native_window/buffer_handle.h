/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDE_BUFFER_HANDLE_H
#define INCLUDE_BUFFER_HANDLE_H

/**
 * @addtogroup NativeWindow
 * @{
 *
 * @brief 提供NativeWindow功能，作为数据生产者，可用来和egl对接。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @since 8
 * @version 1.0
 */

/**
 * @file buffer_handle.h
 *
 * @brief 定义NativeWindow模块使用的的BufferHandle的结构体。
 *
 * @include native_window/buffer_handle.h
 * @library libnative_window.so
 * @since 8
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 缓冲区句柄，用于对缓冲区的信息传递和获取。句柄包含了缓冲区的文件描述符、尺寸、格式、用途、虚拟地址、共享内存键、物理地址、自定义数据。
 * @since 8
 */
typedef struct {
    /** 缓冲区文件描述符，若不支持则为-1。 */
    int32_t fd;
    /** 缓冲区内存的宽度，单位为像素。 */
    int32_t width;
    /** 缓冲区内存的步幅，单位为字节。 */
    int32_t stride;
    /** 缓冲区内存的高度，单位为像素。 */
    int32_t height;
    /** 缓冲区内存的大小，单位为字节。 */
    int32_t size;
    /** 缓冲区内存的格式，取值具体可见{@link OH_NativeBuffer_Format}枚举值。 */
    int32_t format;
    /** 缓冲区内存的用途，按位标志位，取值具体可见{@link OH_NativeBuffer_Usage}枚举值。 */
    uint64_t usage;
    /** 缓冲区内存的虚拟地址。 */
    void *virAddr;
    /** 缓冲区共享内存键值。 */
    int32_t key;
    /** 缓冲区内存的物理地址。 */
    uint64_t phyAddr;
    /** 额外数据的文件描述符数量。 */
    uint32_t reserveFds;
    /** 额外数据的整型值数量。 */
    uint32_t reserveInts;
    /** 额外数据。 */
    int32_t reserve[0];
} BufferHandle;
#ifdef __cplusplus
}

#endif

/** @} */
#endif // INCLUDE_BUFFER_HANDLE_H
