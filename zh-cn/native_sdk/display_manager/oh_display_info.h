/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_NATIVE_DISPLAY_INFO_H
#define OH_NATIVE_DISPLAY_INFO_H

/**
 * @addtogroup OH_DisplayManager
 * @{
 *
 * @brief 提供屏幕管理的能力。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

/**
 * @file oh_display_info.h
 *
 * @brief 提供屏幕的公共枚举、公共定义等。
 *
 * @kit ArkUI
 * @include window_manager/oh_display_info.h
 * @library libnative_display_manager.so
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 屏幕顺时针的旋转角度。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_Rotation {
    /** 代表屏幕顺时针旋转角度0度。 */
    DISPLAY_MANAGER_ROTATION_0 = 0,

    /** 代表屏幕顺时针旋转角度90度。 */
    DISPLAY_MANAGER_ROTATION_90 = 1,

    /** 代表屏幕顺时针旋转角度180度。 */
    DISPLAY_MANAGER_ROTATION_180 = 2,

    /** 代表屏幕顺时针旋转角度270度。 */
    DISPLAY_MANAGER_ROTATION_270 = 3,
} NativeDisplayManager_Rotation;

/**
 * @brief 屏幕的旋转方向。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_Orientation {
    /** 表示设备当前以竖屏方式显示。 */
    DISPLAY_MANAGER_PORTRAIT = 0,

    /** 表示设备当前以横屏方式显示。 */
    DISPLAY_MANAGER_LANDSCAPE = 1,

    /** 表示设备当前以反向竖屏方式显示。 */
    DISPLAY_MANAGER_PORTRAIT_INVERTED = 2,

    /** 表示设备当前以反向横屏方式显示。 */
    DISPLAY_MANAGER_LANDSCAPE_INVERTED = 3,

    /** 表示显示未识别屏幕方向。 */
    DISPLAY_MANAGER_UNKNOWN,
} NativeDisplayManager_Orientation;

/**
 * @brief 屏幕管理接口返回状态码枚举。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_ErrorCode {
    /** 成功。 */
    DISPLAY_MANAGER_OK = 0,

    /** 权限校验失败，应用无权限使用该API，需要申请权限。 */
    DISPLAY_MANAGER_ERROR_NO_PERMISSION = 201,

    /** 权限校验失败，非系统应用使用了系统API。 */
    DISPLAY_MANAGER_ERROR_NOT_SYSTEM_APP = 202,

    /** 参数检查失败。 */
    DISPLAY_MANAGER_ERROR_INVALID_PARAM = 401,

    /** 该设备不支持此API。 */
    DISPLAY_MANAGER_ERROR_DEVICE_NOT_SUPPORTED = 801,

    /** 操作的显示设备无效。 */
    DISPLAY_MANAGER_ERROR_INVALID_SCREEN = 1400001,

    /** 当前操作对象无操作权限。 */
    DISPLAY_MANAGER_ERROR_INVALID_CALL = 1400002,

    /** 系统服务工作异常。 */
    DISPLAY_MANAGER_ERROR_SYSTEM_ABNORMAL = 1400003,
} NativeDisplayManager_ErrorCode;

/**
 * @brief 可折叠设备的显示模式枚举。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_FoldDisplayMode {
    /** 表示设备当前折叠显示模式未知。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_UNKNOWN = 0,

    /** 表示设备当前全屏显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_FULL = 1,

    /** 表示设备当前主屏幕显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_MAIN = 2,

    /** 表示设备当前子屏幕显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_SUB = 3,

    /** 表示设备当前双屏协同显示。 */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_COORDINATION = 4,
} NativeDisplayManager_FoldDisplayMode;

/**
 * @brief 矩形区域。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_Rect {
    /** 矩形区域左边界。 */
    int32_t left;

    /** 矩形区域上边界。 */
    int32_t top;

    /** 矩形区域宽度。 */
    uint32_t width;

    /** 矩形区域高度。 */
    uint32_t height;
} NativeDisplayManager_Rect;

/**
 * @brief 瀑布屏曲面部分显示区域。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_WaterfallDisplayAreaRects {
    /** 瀑布屏左侧不可用矩形区域。 */
    NativeDisplayManager_Rect left;

    /** 瀑布屏顶部不可用矩形区域。 */
    NativeDisplayManager_Rect top;

    /** 瀑布屏右侧不可用矩形区域。 */
    NativeDisplayManager_Rect right;

    /** 瀑布屏底部不可用矩形区域。 */
    NativeDisplayManager_Rect bottom;
} NativeDisplayManager_WaterfallDisplayAreaRects;

/**
 * @brief 挖孔屏、刘海屏、瀑布屏等不可用屏幕区域信息。
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_CutoutInfo {
    /** 挖孔屏、刘海屏不可用屏幕区域长度。 */
    int32_t boundingRectsLength;

    /** 挖孔屏、刘海屏等区域的边界矩形。 */
    NativeDisplayManager_Rect *boundingRects;

    /** 瀑布屏曲面部分显示区域。 */
    NativeDisplayManager_WaterfallDisplayAreaRects waterfallDisplayAreaRects;
} NativeDisplayManager_CutoutInfo;

/**
 * @brief 显示设备的状态枚举。
 *
 * @since 14
 * @version 1.0
 */
typedef enum NativeDisplayManager_DisplayState {
    /** 表示显示设备状态未知。 */
    DISPLAY_MANAGER_DISPLAY_STATE_UNKNOWN = 0,

    /** 表示显示设备状态为关闭。 */
    DISPLAY_MANAGER_DISPLAY_STATE_OFF = 1,

    /** 表示显示设备状态为开启。 */
    DISPLAY_MANAGER_DISPLAY_STATE_ON = 2,

    /** 表示显示设备为低电耗模式。 */
    DISPLAY_MANAGER_DISPLAY_STATE_DOZE = 3,

    /** 表示显示设备为睡眠模式，CPU为挂起状态。 */
    DISPLAY_MANAGER_DISPLAY_STATE_DOZE_SUSPEND = 4,

    /** 表示显示设备为VR模式。 */
    DISPLAY_MANAGER_DISPLAY_STATE_VR = 5,

    /** d表示显示设备为开启状态，CPU为挂起状态。 */
    DISPLAY_MANAGER_DISPLAY_STATE_ON_SUSPEND = 6,
} NativeDisplayManager_DisplayState;

/**
 * @brief 显示设备支持的所有HDR格式。
 *
 * @since 14
 * @version 1.0
 */
typedef struct NativeDisplayManager_DisplayHdrFormat {
    /** 显示设备的HDR格式长度。 */
    uint32_t hdrFormatLength;

    /** 显示设备的HDR格式数据。 */
    uint32_t *hdrFormats;
} NativeDisplayManager_DisplayHdrFormat;

/**
 * @brief 显示设备支持的所有色域类型。
 *
 * @since 14
 * @version 1.0
 */
typedef struct NativeDisplayManager_DisplayColorSpace {
    /** 显示设备的色域长度。 */
    uint32_t colorSpaceLength;

    /** 显示设备的色域数据。 */
    uint32_t *colorSpaces;
} NativeDisplayManager_DisplayColorSpace;

/**
 * @brief 显示设备的对象属性。
 *
 * @since 14
 * @version 1.0
 */
typedef struct NativeDisplayManager_DisplayInfo {
    /** 显示设备的屏幕id号，为非负整数。 */
    uint32_t id;

    /** 显示设备的名称。 */
    char name[OH_DISPLAY_NAME_LENGTH + 1];

    /** 显示设备是否启用：true表示设备启动，false表示设备未启用。 */
    bool isAlive;

    /** 显示设备的屏幕宽度，单位为px，该参数应为非负整数。 */
    int32_t width;

    /** 显示设备的屏幕高度，单位为px，该参数应为非负整数。 */
    int32_t height;

    /** 显示设备的物理宽度，单位为px，该参数应为非负整数。 */
    int32_t physicalWidth;

    /** 显示设备的物理高度，单位为px，该参数应为非负整数。 */
    int32_t physicalHeight;

    /** 显示设备的刷新率，单位为Hz，该参数应为非负整数。 */
    uint32_t refreshRate;

    /** 2in1设备上屏幕的可用区域宽度，单位为px，该参数为非负整数。 */
    uint32_t availableWidth;

    /** 2in1设备上屏幕的可用区域高度，单位为px，该参数为大于0的整数。 */
    uint32_t availableHeight;

    /** 显示设备屏幕的物理像素密度，表示每英寸上的像素点数。该参数为大于0的浮点数，单位为px。一般取值160.0、480.0等，实际能取到的值取决于不同设备设置里提供的可选值。 */
    float densityDPI;

    /** 显示设备逻辑像素的密度，代表物理像素与逻辑像素的缩放系数。该参数为大于0的浮点数，受densityDPI范围限制，取值范围在[0.5，4.0]。一般取值1.0、3.0等，实际取值取决于不同设备提供的densityDPI。 */
    float densityPixels;

    /** 显示设备的显示字体的缩放因子。该参数为大于0的浮点数，通常与densityPixels相同。 */
    float scaledDensity;

    /** 显示设备x方向中每英寸屏幕的确切物理像素值，该参数为大于0的浮点数。 */
    float xDPI;

    /** 显示设备y方向中每英寸屏幕的确切物理像素值，该参数为大于0的浮点数。 */
    float yDPI;

    /** 显示设备的屏幕顺时针旋转角度。 */
    NativeDisplayManager_Rotation rotation;

    /** 显示设备的状态。 */
    NativeDisplayManager_DisplayState state;

    /** 表示屏幕当前显示的方向。 */
    NativeDisplayManager_Orientation orientation;

    /** 显示设备支持的所有HDR格式。 */
    NativeDisplayManager_DisplayHdrFormat *hdrFormat;

    /** 显示设备支持的所有色域类型。 */
    NativeDisplayManager_DisplayColorSpace *colorSpace;
} NativeDisplayManager_DisplayInfo;

/**
 * @brief 多显示设备的Display对象。
 *
 * @since 14
 * @version 1.0
 */
typedef struct NativeDisplayManager_DisplaysInfo {
    /** 多显示设备Display对象的长度。 */
    uint32_t displaysLength;

    /** 多显示设备Display对象的属性。 */
    NativeDisplayManager_DisplayInfo *displaysInfo;
} NativeDisplayManager_DisplaysInfo;

#ifdef __cplusplus
}
#endif
/** @} */
#endif // OH_NATIVE_DISPLAY_INFO_H