/*
 * Copyright (c) 2025 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup RDB
 * @{
 *
 * @brief 关系型数据库（Relational Database，RDB）是一种基于关系模型来管理数据的数据库。\n
 * 关系型数据库基于SQLite组件提供了一套完整的对本地数据库进行管理的机制，对外提供了一系列的增、删、改、查等接口，也可以直接运行用户输入的SQL语句来满足复杂的场景需要。
 *
 * @since 10
 */

/**
 * @file oh_rdb_types.h
 *
 * @brief 提供与数据值相关的类型定义。
 *
 * @kit ArkData
 * @include <database/rdb/oh_rdb_types.h>
 * @library libnative_rdb_ndk.z.so
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 *
 * @since 18
 */

 #ifndef OH_RDB_TYPES_H
 #define OH_RDB_TYPES_H
 
 #ifdef __cplusplus
 extern "C" {
 #endif
 
 /**
  * @brief 表示冲突解决策略的枚举。
  *
  * @since 18
  */
 typedef enum Rdb_ConflictResolution {
     /**
      * @brief 发生冲突时不执行任何操作。
      */
     RDB_CONFLICT_NONE = 1,
     /**
      * @brief 发生冲突时抛错误码，同时回滚本次事务。
      */
     RDB_CONFLICT_ROLLBACK,
     /**
      * @brief 发生冲突时抛错误码，同时回滚本次修改。
      */
     RDB_CONFLICT_ABORT,
     /**
      * @brief 发生冲突时抛错误码，不回滚冲突前的修改同时终止本次修改。
      */
     RDB_CONFLICT_FAIL,
     /**
      * @brief 发生冲突时忽略冲突的数据，继续执行后续修改。
      */
     RDB_CONFLICT_IGNORE,
     /**
      * @brief 发生冲突时，尝试删除后插入，如果还是冲突则等同于RDB_CONFLICT_ABORT。
      */
     RDB_CONFLICT_REPLACE,
 } Rdb_ConflictResolution;
 
 #ifdef __cplusplus
 };
 #endif
 #endif
 /** @} */