/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Motion
 * @{
 *
 * @brief 手势识别设备驱动对硬件服务提供通用的接口能力。
 *
 * 模块提供硬件服务对手势识别驱动模块访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现使能手势识别/
 * 去使能手势识别、订阅/取消订阅手势识别数据。
 *
 * @since 3.2
 */

/**
 * @file IMotionInterface.idl
 *
 * @brief 定义使能/去使能手势识别、订阅/取消订阅手势识别数据的接口。
 *
 * 在实现拿起、翻转、摇一摇、旋转屏等手势识别功能时，需要调用此处定义的接口。
 *
 * 模块包路径：ohos.hdi.motion.v1_0
 *
 * 引用：
 * - ohos.hdi.motion.v1_0.MotionTypes
 * - ohos.hdi.motion.v1_0.IMotionCallback
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.motion.v1_0;

import ohos.hdi.motion.v1_0.MotionTypes;
import ohos.hdi.motion.v1_0.IMotionCallback;

/**
 * @brief 提供Motion设备基本控制操作接口。
 *
 * 接口提供使能/去使能手势识别、订阅/取消订阅手势识别数据功能。
 *
 * @since 3.2
 * @version 1.0
 */
interface IMotionInterface {
    /**
     * @brief 使能手势识别。
     *
     * @param motionType 手势识别类型，详见{@link HdfMotionTypeTag}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    EnableMotion([in] int motionType);

    /**
     * @brief 去使能手势识别。
     *
     * @param motionType，手势识别类型，详见{@link HdfMotionTypeTag}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    DisableMotion([in] int motionType);

    /**
     * @brief 订阅者注册手势识别数据回调函数，如果注册成功，系统会将获取到的手势识别数据上报给订阅者。
     *
     * @param callbackObj 要注册的回调函数，只需成功订阅一次，无需重复订阅。详见{@link IMotionCallback}。
     *
     * @return 如果注册回调函数成功，则返回0。
     * @return 如果注册回调函数失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    Register([in] IMotionCallback callbackObj);

    /**
     * @brief 订阅者取消注册手势识别数据回调函数。
     *
     * @param callbackObj 要取消注册的回调函数，只需成功取消订阅一次，无需重复取消订阅。详见{@link IMotionCallback}。
     *
     * @return 如果取消注册回调函数成功，则返回0。
     * @return 如果取消注册回调函数失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    Unregister([in] IMotionCallback callbackObj);
}
/** @} */