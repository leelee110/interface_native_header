/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Hostapd
 * @{
 *
 * @brief 定义上层WLAN服务的API接口。
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管，电源管理等。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IHostapdCallback.idl
 *
 * @brief 提供在重新启动hostapd时调用的回调,返回扫描结果,接收Netlink消息.
 *
 * 模块包路径：ohos.hdi.wlan.hostapd.v1_0
 *
 * 引用：ohos.hdi.wlan.hostapd.v1_0.HostapdTypes
 *
 * @since 4.1
 * @version 1.0
 */

package ohos.hdi.wlan.hostapd.v1_0;

import ohos.hdi.wlan.hostapd.v1_0.HostapdTypes;

/**
 * @brief hostapd回调的接口.
 *
 * 当hostapd启动、热点扫描结束，收到Netlink消息时，调用该回调，继续后续处理。
 *
 * @since 4.1
 * @version 1.0
 */
[callback] interface IHostapdCallback {
    /**
     * @brief Wi-Fi Hal回调STA加入AP。
     *
     * @param staJoinParm 表示sta加入内容。
     * @param ifName 表示网卡名称。
     *
     * @since 4.1
     * @version 1.0
     */
    OnEventStaJoin([in] struct HdiApCbParm apCbParm, [in] String ifName);

    /**
     * @brief Wi-Fi Hal回调AP状态。
     *
     * @param apStateParm 表示ap状态内容。
     * @param ifName 表示网卡名称。
     *
     * @since 4.1
     * @version 1.0
     */
    OnEventApState([in] struct HdiApCbParm apCbParm, [in] String ifName);

    /**
    * 用于处理Hostapd回调参数。
    *
    * @param notifyParam 表示Hostapd的参数。
    * @param ifName 表示网卡名称。
    *
    * @since 4.1
    * @version 1.0
    */
    OnEventHostApdNotify([in] String notifyParam, [in] String ifName);
}
/** @} */