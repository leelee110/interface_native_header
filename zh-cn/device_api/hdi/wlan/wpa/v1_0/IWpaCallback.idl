/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Wpa
 * @{
 *
 * @brief 定义上层WLAN服务的API接口。
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点
 * WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IWpaCallback.idl
 *
 * @brief 提供在wpa请求程序重启时调用的回调，返回扫描结果，并且接收到Netlink消息.
 *
 * 模块包路径：ohos.hdi.wlan.wpa.v1_0
 *
 * 引用：ohos.hdi.wlan.wpa.v1_0.WpaTypes
 *
 * @since 3.2
 * @version 1.0
 */


package ohos.hdi.wlan.wpa.v1_0;

import ohos.hdi.wlan.wpa.v1_0.WpaTypes;

/**
 * @brief 定义Wpa模块的回调。
 *
 * 当wpa客户端重启、热点扫描结束时，或者收到Netlink消息，调用回调以继续后续处理。
 *
 * @since 4.1
 * @version 1.0
 */
[callback] interface IWpaCallback {
    /**
     * @brief 用于表示与此接口上当前连接的断开连接。
     *
     * @param disconnectParam  表示Disconnect的参数
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     *
     * @since 4.1
     * @version 1.0
     */
   OnEventDisconnected([in] struct HdiWpaDisconnectParam disconnectParam, [in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前连接的网络的连接。
     *
     * @param connectParam 表示Connect的参数
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
   OnEventConnected([in] struct HdiWpaConnectParam connectParam, [in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前BssidChanged网络的BssidChanged。
     *
     * @param bssidChangedParam 表示BsidChanged的参数
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
    OnEventBssidChanged([in] struct HdiWpaBssidChangedParam bssidChangedParam, [in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前状态已更改的网络的Wpa状态已更。
     *
     * @param statechangedParam 表示BsidChanged的参数
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
   OnEventStateChanged([in] struct HdiWpaStateChangedParam statechangedParam, [in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前Wifi TempDisabled网络的TempDisabled。
     *
     * @param tempDisabledParam 表示TempDisabled的参数
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
   OnEventTempDisabled([in] struct HdiWpaTempDisabledParam tempDisabledParam, [in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前Wifi AssociateReject网络的AssociateReject。
     *
     * @param associateRejectParam 表示AssociateReject的参数
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
    OnEventAssociateReject([in] struct HdiWpaAssociateRejectParam associateRejectParam, [in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前wifi网络的WPS PBC连接尝试在此接口上重叠。
     *
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
    OnEventWpsOverlap([in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前wifi网络尝试进行PBC连接的超时。
     *
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
    OnEventWpsTimeout([in] String ifName);

    /**
     * @brief 用于表示来自此接口上当前wifi网络尝试进行PBC连接的超时。
     *
     * @param recvScanResultParam 表示wifi网络的Recv ScanResult信息
     * @param ifName 表示网卡(NIC)名称
     *
     * @since 4.1
     * @version 1.0
     */
    OnEventScanResult([in] HdiWpaRecvScanResultParam recvScanResultParam, [in] String ifName);
   /**
   * @brief 用于表示已找到P2P设备。
   *
   * @param deviceInfoParam  表示找到的设备的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventDeviceFound([in] struct HdiP2pDeviceInfoParam deviceInfoParam, [in] String ifName);

   /**
   * @brief 用于表示P2P设备已丢失。
   *
   * @param deviceLostParam 表示丢失设备的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventDeviceLost([in] struct HdiP2pDeviceLostParam deviceLostParam, [in] String ifName);

   /**
   * @brief 用于表示接收到P2P Group Owner协商请求。
   *
   * @param goNegotiationRequestParam 启动GO的设备的MAC地址协商请求
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventGoNegotiationRequest([in] struct HdiP2pGoNegotiationRequestParam goNegotiationRequestParam,
       [in] String ifName);

   /**
   * @brief 用于表示P2P Group Owner协商请求完成。
   *
   * @param goNegotiationCompletedParam GO协商状态
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventGoNegotiationCompleted([in] struct HdiP2pGoNegotiationCompletedParam goNegotiationCompletedParam,
       [in] String ifName);

  /**
   * @brief 用于表示接收到P2P邀请。
   *
   * @param invitationReceivedParam 表示邀请ReceivedParam的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventInvitationReceived([in] struct HdiP2pInvitationReceivedParam invitationReceivedParam,
       [in] String ifName);

  /**
   * @brief 用于表示P2P邀请请求的结果。
   *
   * @param invitationResultParam 表示邀请结果的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventInvitationResult([in] struct HdiP2pInvitationResultParam invitationResultParam,
       [in] String ifName);

   /**
   * @brief 用于表示成功组建P2P组。
   *
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventGroupFormationSuccess([in] String ifName);

   /**
   * @brief 用于表示组建P2P组失败。
   *
   * @param reason  表示建组失败的原因
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventGroupFormationFailure([in] String reason, [in] String ifName);

   /**
   * @brief 用于表示P2P建组开始。
   *
   * @param groupStartedParam 表示已启动组的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1 
   * @version 1.0
   */
   OnEventGroupStarted([in] struct HdiP2pGroupStartedParam groupStartedParam, [in] String ifName);

  /**
   * @brief 用于表示删除P2P组。
   *
   * @param groupRemovedParam 表示已删除组的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventGroupRemoved([in] struct HdiP2pGroupRemovedParam groupRemovedParam, [in] String ifName);

  /**
   * @brief 用于表示P2P提供发现请求完成。
   *
   * @param provisionDiscoveryCompletedParam 表示配置发现已完成的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventProvisionDiscoveryCompleted([in] struct HdiP2pProvisionDiscoveryCompletedParam
       provisionDiscoveryCompletedParam, [in] String ifName);

   /**
   * @brief 用于表示P2P查找操作的终止。
   *
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventFindStopped([in] String ifName);

  /**
   * @brief 用于表示P2P服务发现的请求。
   *
   * @param servDiscReqInfoParam  表示服务发现请求的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventServDiscReq([in] struct HdiP2pServDiscReqInfoParam servDiscReqInfoParam, [in] String ifName);

  /**
   * @brief 用于表示接收到P2P服务发现响应。
   *
   * @param servDiscRespParam  表示服务发现响应的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventServDiscResp([in] struct HdiP2pServDiscRespParam servDiscRespParam, [in] String ifName);

  /**
   * @brief 用于表示STA设备何时连接或断开与该设备的连接。
   *
   * @param staConnectStateParam  表示Sta连接状态的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventStaConnectState([in] struct HdiP2pStaConnectStateParam staConnectStateParam, [in] String ifName);

  /**
   * @brief 用于表示创建Iface的时间。
   *
   * @param ifaceCreatedParam  表示Iface创建的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventIfaceCreated([in] struct HdiP2pIfaceCreatedParam ifaceCreatedParam, [in] String ifName);

   /**
   * @brief 用于表示STA认证拒绝。
   *
   * @param authRejectParam  表示身份验证拒绝的参数
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventAuthReject([in]  struct HdiWpaAuthRejectParam authRejectParam, [in] String ifName);

  /**
   * @brief 用于处理STA回调参数。
   *
   * @param notifyParam 表示Sta的参数.
   * @param ifName 表示网卡(NIC)名称
   *
   * @since 4.1
   * @version 1.0
   */
   OnEventStaNotify([in] String notifyParam, [in] String ifName);
}
/** @} */