/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 服务获取驱动对象或者代理后，马达服务启动或停止振动。
 * 通过驱动程序对象或代理提供使用功能。
 *
 * @version 1.0
 */

/**
 * @file vibrator_type.h
 *
 * @brief 定义马达数据结构，包括马达模式和效果振动。
 *
 * @since 2.2
 * @version 1.0
 */

#ifndef VIBRATOR_TYPE_H
#define VIBRATOR_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 定义马达模块返回值。
 *
 * @since 2.2
 */

enum VibratorStatus {
    /** 操作成功。 */
    VIBRATOR_SUCCESS            = 0,
    /** 不支持振动周期设置。 */
    VIBRATOR_NOT_PERIOD         = -1,
    /** 不支持振幅设置。 */
    VIBRATOR_NOT_INTENSITY      = -2,
    /** 不支持频率设置。 */
    VIBRATOR_NOT_FREQUENCY      = -3,
};

/**
 * @brief 定义马达振动模式。
 *
 * @since 2.2
 */

enum VibratorMode {
    /** 表示给定持续时间内的一次性振动。 */
    VIBRATOR_MODE_ONCE   = 0,
    /** 表示具有预置效果的周期性振动。 */
    VIBRATOR_MODE_PRESET = 1,
    /**< 表示高清振动。 */
    VIBRATOR_MODE_HDHAPTIC = 2,
    /** 表示效果模式无效。 */
    VIBRATOR_MODE_BUTT
};

/**
 * @brief 枚举复合效果的效果类型。
 *
 * @since 3.2
 */
enum EffectType {
    /**< 表示给定时间序列的时间效果类型。 */
    EFFECT_TYPE_TIME,
    /**< 表示给定基本振动序列的基本振动效果类型。 */
    EFFECT_TYPE_PRIMITIVE,
    /**< 表示效果类型无效。 */
    EFFECT_TYPE_BUTT,
};

/**
 * @brief 枚举事件类型。
 *
 * @since 4.1
 */
enum EVENT_TYPE {
    /**< 表示振动是连续的。 */
    CONTINUOUS = 0,
    /**< 表示振动是瞬时的。 */
    TRANSIENT  = 1,
};

/**
 * @brief 定义马达参数。
 *
 * 参数包括设置马达振幅和频率以及振幅和频率的范围。
 *
 * @since 3.2
 */
struct VibratorInfo {
    /** 设置马达振幅。1表示支持，0表示不支持。 */
    int32_t isSupportIntensity;
    /** 设置马达频率。1表示支持，0表示不支持。 */
    int32_t isSupportFrequency;
    /** 最大振幅。 */
    int32_t intensityMaxValue;
    /** 最小振幅。 */
    int32_t intensityMinValue;
    /** 最大频率。 */
    int32_t frequencyMaxValue;
    /** 最小频率。 */
    int32_t frequencyMinValue;
};

/**
 * @brief 定义时间效果参数。
 *
 * 参数包括振动的延迟、时间、强度和频率。
 *
 * @since 3.2
 */
struct TimeEffect {
    /** 等待时间。 */
    int32_t delay;
    /** 振动时间。 */
    int32_t time;
    /** 振动强度。 */
    uint16_t intensity;
    /** 振动频率（Hz）。 */
    int16_t frequency;
};

/**
 * @brief 定义基本效果参数。
 *
 * 参数包括延迟、效应id和振动强度。
 *
 * @since 3.2
 */
struct PrimitiveEffect {
    /** 等待时间。 */
    int32_t delay;
    /** 效果id。 */
    int32_t effectId;
    /** 振动强度。 */
    uint16_t intensity;
};

/**
 * @brief 自定义复合效果定义两种效果。
 *
 * 参数包括时间效果和预定义效果。
 *
 * @since 3.2
 */
union Effect {
    struct TimeEffect timeEffect;              /** 时间效果，请参阅｛@link TimeEffect｝ */
    struct PrimitiveEffect primitiveEffect;    /** 预定义效果，请参见｛@link PrimitiveEffect｝ */
};

/**
 * @brief 定义复合振动效果参数。
 *
 * 参数包括复合效果的类型和顺序。
 *
 * @since 3.2
 */
struct CompositeEffect {
    /** 复合效果的类型，请参见｛@link union HdfEffectType｝。 */
    int32_t type;
    /** 合成效果的序列，请参见｛@link union Effect｝。 */
    union Effect effects[];
};

/**
 * @brief 定义振动效果信息。
 *
 * 该信息包括设置效果的能力和效果的振动持续时间。
 *
 * @since 3.2
 */
struct EffectInfo {
    /** 效果的振动持续时间，以毫秒为单位。 */
    int32_t duration;
    /** 设置效果能力。1表示支持，0表示不支持。 */
    bool isSupportEffect;
};


/**
 * @brief 表示振动点。
 *
 * 参数包含时间，强度和频率。
 *
 * @since 4.1
 */
struct CurvePoint {
    /** 时间。 */
    int32_t time;
    /** 强度。 */
    int32_t intensity;
    /** 频率。 */
    int32_t frequency;
};

/**
 * @brief 表示振动事件。
 *
 * 参数包含振动时间，强度，频率等等。
 *
 * @since 4.1
 */
struct HapticEvent {
    /** 振动类型。 */
    enum EVENT_TYPE type;
    /** 时间。 */
    int32_t time;
    /** 振动延时。 */
    int32_t duration;
    /** 振动强度。 */
    int32_t intensity;
    /** 振动频率。 */
    int32_t frequency;
    /** 马达id。表示振动的是哪个马达。 */
    int32_t index;
    /** 振动点数量。 */
    int32_t pointNum;
    /** 振动点数组。 */
    struct CurvePoint points[];
};

/**
 * @brief 高清振动数据包。
 *
 * 参数包含具体的高清振动数据。
 *
 * @since 4.1
 */
struct HapticPaket {
    /** 时间。 */
    int32_t time;
    /** 振动事件数量。 */
    int32_t eventNum;
    /** 振动事件数组。 */
    struct HapticEvent events[];
};

/**
 * @brief 振动能力数据包。
 *
 * 信息包括不同类型的振动。
 *
 * @since 4.1
 */
struct HapticCapacity {
    /** 是否支持高清振动。 */
    bool isSupportHdHaptic;
    /** 是否支持预定义振动。 */
    bool isSupportPresetMapping;
    /** 是否支持延时振动。 */
    bool isSupportTimeDelay;
    /** 预留参数. */
    bool reserved0;
    /** 预留参数. */
    int32_t reserved1;
};

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* VIBRATOR_TYPE_H */
/** @} */
