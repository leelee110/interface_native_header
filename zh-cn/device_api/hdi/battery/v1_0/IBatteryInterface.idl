/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Battery
 * @{
 *
 * @brief 提供获取、订阅电池信息的接口。
 *
 * 电池模块为电池服务提供的获取、订阅电池信息的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口获取、订阅电池信息。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file IBatteryInterface.idl
 *
 * @brief 获取、订阅电池信息的接口。
 *
 * 服务获取此模块的对象或代理后，可以调用相关的接口获取、订阅电池信息。
 *
 * 模块包路径：ohos.hdi.battery.v1_0
 *
 * 引用：
 * - ohos.hdi.battery.v1_0.Types
 * - ohos.hdi.battery.v1_0.IBatteryCallback
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.battery.v1_0;

import ohos.hdi.battery.v1_0.Types;
import ohos.hdi.battery.v1_0.IBatteryCallback;

/**
 * @brief 获取、订阅电池信息的接口。
 *
 * 服务获取此对象后，可以调用相关的接口获取、订阅电池信息。
 *
 * @since 3.1
 */
interface IBatteryInterface {
    /**
     * @brief 注册电池信息的回调。
     *
     * @param event 输入参数，服务注册的回调。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    Register([in] IBatteryCallback event);

    /**
     * @brief 取消注册电池信息的回调。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    UnRegister();

    /**
     * @brief 设置电池信息节点的路径。
     *
     * @param path 输入参数，电池信息节点的路径。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    ChangePath([in] String path);

    /**
     * @brief 获取电池的电量百分比。
     *
     * @param capacity 输出参数，表示电量的百分比值。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetCapacity([out] int capacity);

    /**
     * @brief 获取电池的电压，单位微伏。
     *
     * @param voltage 输出参数，表示电池的电压。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetVoltage([out] int voltage);

    /**
     * @brief 获取电池的充电温度，单位0.1摄氏度。
     *
     * @param temperature 输出参数，表示电池温度。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetTemperature([out] int temperature);

    /**
     * @brief 获取电池的健康状态。
     *
     * @param healthState 输出参数，表示电池健康状态。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     * @see BatteryHealthState
     *
     * @since 3.1
     */
    GetHealthState([out] enum BatteryHealthState healthState);

    /**
     * @brief 获取充电设备类型。
     *
     * @param pluggedType 输出参数，表示充电设备类型。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     * @see BatteryPluggedType
     *
     * @since 3.1
     */
    GetPluggedType([out] enum BatteryPluggedType pluggedType);

    /**
     * @brief 获取充电状态。
     *
     * @param chargeState 输出参数，表示充电状态。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     * @see BatteryChargeState
     *
     * @since 3.1
     */
    GetChargeState([out] enum BatteryChargeState chargeState);

    /**
     * @brief 获取是否支持电池或者电池是否在位。
     *
     * @param present 输出参数，表示是否支持电池或者电池是否在位。true表示支持或在位，false表示不支持或不在位。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetPresent([out] boolean present);

    /**
     * @brief 获取电池的技术型号。
     *
     * @param technology 输出参数，当前电池技术型号。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetTechnology([out] String technology);

    /**
     * @brief 获取电池的总容量。
     *
     * @param totalEnergy 输出参数，表示电池的总容量，单位毫安时。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetTotalEnergy([out] int totalEnergy);

    /**
     * @brief 获取电池的平均电流。
     *
     * @param curAverage 输出参数，表示电池的平均电流，单位毫安。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetCurrentAverage([out] int curAverage);

    /**
     * @brief 获取电池的电流。
     *
     * @param curNow 输出参数，表示电池的实时电流，单位毫安。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetCurrentNow([out] int curNow);

    /**
     * @brief 获取电池的剩余容量。
     *
     * @param remainEnergy 输出参数，表示电池的剩余容量，单位毫安时。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     *
     * @since 3.1
     */
    GetRemainEnergy([out] int remainEnergy);

    /**
     * @brief 获取电池的全部信息。
     *
     * @param info 输出参数，电池的全部信息。
     *
     * @return HDF_SUCCESS 表示操作成功。
     * @return HDF_FAILED 表示操作失败。
     * @see BatteryInfo
     *
     * @since 3.1
     */
    GetBatteryInfo([out] struct BatteryInfo info);
}
/** @} */
