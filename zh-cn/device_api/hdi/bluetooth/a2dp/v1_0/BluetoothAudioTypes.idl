/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiA2dp
 * @{
 *
 * @brief HdiA2dp为A2DP服务提供统一接口。
 *
 * 主机可以通过该模块提供的接口创建音频通话，与音频子系统交换数据。
 *
 * @since 4.0
 */

/**
 * @file BluetoothAudioTypes.idl
 *
 * @brief 声明数据结构。
 *
 * 模块包路径：ohos.hdi.bluetooth.a2dp.v1_0
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.bluetooth.a2dp.v1_0;

/**
 * @brief 声明操作行为。
 *
 * @since 4.0
 */
enum Operation {
    /** 暂停渲染。*/
    SUSPEND_RENDER = 0,
    /** 开启渲染。*/
    START_RENDER = 1,
};

/**
 * @brief 声明接口调用的操作结果。
 *
 * @since 4.0
 */
enum Status {
    /** 调用成功。*/
    SUCCESS = 0,
    /** 调用失败。*/
    FAILURE = 1,
};

/**
 * @brief 声明音频会话的类型。
 *
 * @since 4.0
 */
enum SessionType {
    /** 未知类型。*/
    UNKNOWN_TYPE,
    /** 软件编码类型。*/
    SOFTWARE_ENCODING,
    /** 硬件编码类型。*/
    HARDWARE_ENCODING,
};
/** @} */