/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiSecureElement
 * @{
 *
 * @brief 为SecureElement服务提供统一的访问安全单元的接口。
 * 
 * SecureElement服务通过获取SecureElementInterface对象提供的API接口访问安全单元，包括初始化、
 * 判断安全单元状态、创建关闭逻辑通道、与SE进行APDU指令交互等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file ISecureElementInterface.idl
 *
 * @brief 定义SecureElement初始化、操作通道、利用通道与安全单元进行APDU指令交互的接口文件
 *
 * 模块包路径：ohos.hdi.secure_element.v1_0
 *
 * 引用：
 * - ohos.hdi.secure_element.v1_0.ISecureElementCallback
 * - ohos.hdi.secure_element.v1_0.SecureElementTypes
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.secure_element.v1_0;

import ohos.hdi.secure_element.v1_0.ISecureElementCallback;
import ohos.hdi.secure_element.v1_0.SecureElementTypes;

/**
 * @brief 声明由SecureElement模块提供的用于获取SecureElement操作的API，
 * 请参阅“Open Mobile API 规范”。
 *
 * @since 4.0
 * @version 1.0
 */

interface ISecureElementInterface {

    /**
     * @brief 初始化安全单元。
     *
     * @param callback 用于通知SE状态更改的回调。
     * @param status 初始化SE的状态。
     * @since 4.0
     * @version 1.0
     */
    init([in] ISecureElementCallback clientCallback, [out] enum SecureElementStatus status);

    /**
     * @brief 获取此SE的ATR。
     *
     * @param response 返回SE的ATR，SE的ATR不可用时，返回空的数组。
     * @since 4.0
     * @version 1.0
     */
    getAtr([out] List<unsigned char> response);

    /**
     * @brief 检查当前的安全单元是否可用。
     *
     * @param present 如果安全单元可用，则present等于True，否则为false。
     *
     * @since 4.0
     * @version 1.0
     */
    isSecureElementPresent([out] boolean present);

    /**
     * @brief 使用SE打开一个逻辑通道，选择由给定AID代表的应用（当AID不为Null且AID的长度不为0时）。
     *
     * @param aid 要在此通道上选择的应用的AID的byte数组。
     * @param p2 在该通道上执行的SELECT APDU。
     * @param response 对SELECT指令的响应，如果失败则为空。
     * @param channelNumber 新逻辑通道的通道编号。
     * @param status 打开逻辑通道的状态。
     *
     * @since 4.0
     * @version 1.0
     */
    openLogicalChannel([in] List<unsigned char> aid, [in] unsigned char p2, [out] List<unsigned char> response,
        [out] unsigned char channelNumber, [out] enum SecureElementStatus status);

    /**
     * @brief 访问[ISO 7816-4]中定义的基本通道（编号为0的通道）。所获得的对象是Channel类的一个实例。
     *
     * @param aid 要在此通道上选择的应用的AID的byte数组。
     * @param p2 在该通道上执行的SELECT APDU。
     * @param response SELECT指令的响应，如果失败则为空。
     * @param status 打开基本通道的状态。
     *
     * @since 4.0
     * @version 1.0
     */
    openBasicChannel([in] List<unsigned char> aid, [in] unsigned char p2, [out] List<unsigned char> response,
        [out] enum SecureElementStatus status);

    /**
     * @brief 关闭此SE的逻辑通道。关闭基本通道必须返回SecureElementStatus:：FAILED。
     *
     * @param channelNumber 要关闭的逻辑通道编号。
     * @param status 需要关闭的逻辑通道的状态。
     *
     * @since 4.0
     * @version 1.0
     */
    closeChannel([in] unsigned char channelNumber, [out] enum SecureElementStatus status);

    /**
     * @brief 向SE发送APDU指令（根据协议ISO/IEC 7816）。
     *
     * @param command 要发送的byte数组格式的APDU指令。
     * @param response 以byte数组接收到的响应。
     * @param status 传输指令的状态。
     *
     * @since 4.0
     * @version 1.0
     */
     transmit([in] List<unsigned char> command, [out] List<unsigned char> response, [out] enum SecureElementStatus status);

    /**
     * @brief 向SE发送APDU指令（根据协议ISO/IEC 7816）。
     *
     * @param status 重置安全单元的状态。
     * @since 4.0
     * @version 1.0
     */
     reset([out] enum SecureElementStatus status);
}
/** @} */
