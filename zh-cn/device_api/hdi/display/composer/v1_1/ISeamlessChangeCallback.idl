/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 4.1
 * @version 1.1
 */

 /**
 * @file ISeamlessChangeCallback.idl
 *
 * @brief 显示合成接口声明。
 *
 * 模块包路径：ohos.hdi.display.composer.v1_1
 *
 * @since 4.1
 * @version 1.1
 */


package ohos.hdi.display.composer.v1_1;

/**
 * @brief 更改帧速率需要使用的接口。
 *
 *
 * @since 4.1
 * @version 1.1
 */
[callback] interface ISeamlessChangeCallback {
    /**
     * @brief 准备好更改帧速率时要调用的回调
     *
     * @param devId 表示需要操作的设备ID。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 4.1
     * @version 1.1
     */
    OnSeamlessChange([in] unsigned int devId);
}
/** @} */