/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFingerprintAuth
 * @{
 *
 * @brief 提供指纹认证驱动的API接口。
 *
 * 指纹认证驱动程序为指纹认证服务提供统一的接口，用于访问指纹认证驱动程序。获取指纹认证驱动代理后，服务可以调用相关API获取执行器。
 * 获取指纹认证执行器后，服务可以调用相关API获取执行器信息，获取凭据模板信息、注册指纹特征模板、进行用户指纹认证、删除指纹特征模板等。
 *
 * @since 3.2
 */

/**
 * @file FingerprintAuthTypes.idl
 *
 * @brief 定义指纹认证驱动枚举和数据结构，包括认证类型、执行器角色、 执行器安全等级、命令ID、指纹提示信息编码、执行器信息和模板信息。
 *
 * 模块包路径：ohos.hdi.fingerprint_auth.v1_0
 *
 * @since 3.2
 */

package ohos.hdi.fingerprint_auth.v1_0;

/**
 * @brief 枚举用户认证的凭据类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum AuthType : int {
    PIN = 1, /**< 认证凭据类型为口令。 */
    FACE = 2, /**< 认证凭据类型为人脸。 */
    FINGERPRINT = 4, /**< 认证凭据类型为指纹。 */
};

/**
 * @brief 枚举执行器角色。
 *
 * @since 3.2
 * @version 1.0
 */
enum ExecutorRole : int {
    COLLECTOR = 1, /**< 表示执行器角色是采集器，提供用户认证时的数据采集能力，需要和认证器配合完成用户认证。 */
    VERIFIER = 2, /**< 表示执行器角色是认证器，提供用户认证时数据处理能力，读取存储凭据模板信息并完成比对。 */
    ALL_IN_ONE = 3, /**< 表示执行器角色是全功能执行器，可提供用户认证数据采集、处理、储存及比对能力。 */
};

/**
 * @brief 枚举执行器安全等级。
 *
 * @since 3.2
 * @version 1.0
 */
enum ExecutorSecureLevel : int {
    ESL0 = 0, /**< 表示执行器安全等级是0，关键操作在无访问控制执行环境中完成。 */
    ESL1 = 1, /**< 表示执行器安全等级是1，关键操作在有访问控制的执行环境中完成。 */
    ESL2 = 2, /**< 表示执行器安全等级是2，关键操作在可信执行环境中完成。 */
    ESL3 = 3, /**< 表示执行器安全等级是3，关键操作在高安环境如独立安全芯片中完成。 */
};

/**
 * @brief 枚举指纹认证功能相关操作命令。
 *
 * @since 3.2
 * @version 1.0
 */
enum CommandId : int {
    LOCK_TEMPLATE = 1, /**< 锁定指纹模板。 */
    UNLOCK_TEMPLATE = 2, /**< 解锁指纹模板。 */
    VENDOR_COMMAND_BEGIN = 10000 /**< 用于厂商自定义操作指令。 */
};

/**
 * @brief 枚举提示信息编码。
 *
 * @since 3.2
 * @version 1.0
 *
 * @deprecated 从4.0版本开始废弃，使用{@link FingerprintTipsCode}接口替代。
 */
enum FingerprintTipsCode : int {
    FINGERPRINT_AUTH_TIP_GOOD = 0, /**< 获取的指纹图像是完整的。 */
    FINGERPRINT_AUTH_TIP_DIRTY = 1, /**< 指纹图像非常模糊，原因是传感器上存在可疑或检测到的污垢。 */
    FINGERPRINT_AUTH_TIP_INSUFFICIENT = 2, /**< 仅检测到部分指纹图像。 */
    FINGERPRINT_AUTH_TIP_PARTIAL = 3, /**< 仅检测到部分指纹图像。 */
    FINGERPRINT_AUTH_TIP_TOO_FAST = 4, /**< 指纹图像由于快速移动而不完整。 */
    FINGERPRINT_AUTH_TIP_TOO_SLOW = 5, /**< 指纹图像由于没有移动而无法读取。 */
    VENDOR_FINGERPRINT_AUTH_TIP_BEGIN = 10000 /**< 用于厂商自定义提示信息。 */
};

/**
 * @brief 执行器信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct ExecutorInfo {
    unsigned short sensorId; /**< 传感器ID，不同传感器在人脸认证驱动内的唯一标识。 */
    unsigned int executorType; /**< 执行器类型，根据执行器支持的能力进行分类。 */
    enum ExecutorRole executorRole; /**< 执行器角色{@link ExecutorRole}。 */
    enum AuthType authType; /**< 用户认证凭据类型{@link AuthType}。 */
    enum ExecutorSecureLevel esl; /**< 执行器安全等级{@link ExecutorSecureLevel}。 */
    unsigned char[] publicKey; /**< 执行器公钥，用于校验该执行器私钥签名的信息。 */
    unsigned char[] extraInfo; /**< 其他相关信息，用于支持信息扩展。 */
};

/**
 * @brief 凭据模板信息。
 *
 * @since 3.2
 * @version 1.0
 *
 * @deprecated 从4.0版本开始废弃，使用{@link Property}接口替代。
 */
struct TemplateInfo {
    unsigned int executorType; /**< 执行器类型，根据执行器支持的能力进行分类。 */
    int lockoutDuration; /**< 认证方式被冻结的时间。 */
    int remainAttempts; /**< 认证方式距离被冻结的可处理认证请求次数。 */
    unsigned char[] extraInfo; /**< 其他相关信息，用于支持信息扩展。 */
};
/** @} */