/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiDrm
 * @{
 * @brief DRM模块接口定义。
 *
 * DRM是数字版权管理，用于对多媒体内容加密，以便保护价值内容不被非法获取，
 * DRM模块接口定义了DRM实例管理、DRM会话管理、DRM内容解密的接口。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IMediaKeySystem.idl
 *
 * @brief 定义了DRM实例的功能接口。
 *
 * 模块包路径：ohos.hdi.drm.v1_0
 *
 * 引用：
 * - ohos.hdi.drm.v1_0.MediaKeySystemTypes
 * - ohos.hdi.drm.v1_0.IMediaKeySession
 * - ohos.hdi.drm.v1_0.IMediaKeySystemCallback
 * - ohos.hdi.drm.v1_0.IOemCertificate
 *
 * @since 4.1
 * @version 1.0
 */

package ohos.hdi.drm.v1_0;

import ohos.hdi.drm.v1_0.MediaKeySystemTypes;
import ohos.hdi.drm.v1_0.IMediaKeySession;
import ohos.hdi.drm.v1_0.IMediaKeySystemCallback;
import ohos.hdi.drm.v1_0.IOemCertificate;

/**
 * @brief DRM实例功能接口，判断是否支持特定DRM方案，创建DRM实例。
 *
 * @since 4.1
 * @version 1.0
*/
interface IMediaKeySystem {
    /**
     * @brief 获取特定名称属性的字符串值（字符串）。
     *
     * @param name 属性名。
     * @param value 返回值。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetConfigurationString([in] String name, [out] String value);

    /**
     * @brief 设置特定名称属性的值（字符串）。
     *
     * @param name 属性名。
     * @param value 待设置字符串。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    SetConfigurationString([in] String name, [in] String value);

    /**
     * @brief 获取特定名称属性的值（字节数组）。
     *
     * @param name 属性名。
     * @param value 返回值。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetConfigurationByteArray([in] String name, [out] unsigned char[] value);

    /**
     * @brief 设置特定名称属性的值（字节数组）。
     *
     * @param name 属性名。
     * @param value 待设置字节数组。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    SetConfigurationByteArray([in] String name, [in] unsigned char[] value);

    /**
     * @brief 获取度量统计数据。
     *
     * @param statistics DRM驱动自定义的度量统计数据，以字符串对形式表达。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetStatistics([out] Map<String, String> statistics);

    /**
     * @brief 获取DRM方案支持的最大内容保护级别。
     *
     * @param level 内容保护级别。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetMaxContentProtectionLevel([out] enum ContentProtectionLevel level);

    /**
     * @brief 产生证书下载请求。
     *
     * @param defaultUrl 默认的证书服务器URL地址。
     * @param request 证书下载请求报文，以字节数组定义。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GenerateKeySystemRequest([out] String defaultUrl, [out] unsigned char[] request);

    /**
     * @brief 处理下载的证书。
     *
     * @param response 下载的证书报文。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    ProcessKeySystemResponse([in] unsigned char[] response);

    /**
     * @brief 获取证书状态。
     *
     * @param status 证书状态。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetOemCertificateStatus([out] enum CertificateStatus status);

    /**
     * @brief 设置DRM实例事件通知接口。
     *
     * @param systemCallback DRM实例事件通知接口。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    SetCallback([in] IMediaKeySystemCallback systemCallback);

    /**
     * @brief 创建DRM会话。
     *
     * @param level 待创建会话的内容保护等级。
     * @param keySession 创建的DRM会话。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    CreateMediaKeySession([in] enum ContentProtectionLevel level, [out] IMediaKeySession keySession);

    /**
     * @brief 获取离线许可证索引（数组）。
     *
     * @param mediaKeyIds 离线许可证索引数组。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetOfflineMediaKeyIds([out] unsigned char[][] mediaKeyIds);

    /**
     * @brief 获取离线许可证状态。
     *
     * @param mediaKeyId 离线许可证索引。
     * @param mediaKeyStatus 离线许可证状态。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetOfflineMediaKeyStatus([in] unsigned char[] mediaKeyId, [out] enum OfflineMediaKeyStatus mediaKeyStatus);

    /**
     * @brief 删除离线许可证。
     *
     * @param mediaKeyId 离线许可证索引。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    ClearOfflineMediaKeys([in] unsigned char[] mediaKeyId);

    /**
     * @brief 获取证书下载接口。
     *
     * @param oemCert 证书下载接口，参见{@link IOemCertificate}。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GetOemCertificate([out] IOemCertificate oemCert);

    /**
     * @brief 销毁DRM实例。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    Destroy();
};
/** @} */
