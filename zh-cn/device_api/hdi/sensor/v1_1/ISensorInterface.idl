/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiSensor
 * @{
 *
 * @brief 传感器设备驱动对传感器服务提供通用的接口能力。
 *
 * 模块提供传感器服务对传感器驱动访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现获取传感器设备信息、订阅/取消订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度、量程等可选配置等。
 *
 * @since 4.0
 */

/**
 * @file ISensorInterface.idl
 *
 * @brief Sensor模块对外通用的接口声明文件，提供获取传感器设备信息、订阅/取消订阅传感器数据、
 * 使能/去使能传感器、设置传感器模式、设置传感器精度，量程等可选配置接口定义。
 *
 * 模块包路径：ohos.hdi.sensor.v1_1
 *
 * 引用：
 * - ohos.hdi.sensor.v1_1.SensorTypes
 * - ohos.hdi.sensor.v1_1.ISensorCallback
 *
 * @since 4.0
 * @version 1.1
 */


package ohos.hdi.sensor.v1_1;

import ohos.hdi.sensor.v1_1.SensorTypes;
import ohos.hdi.sensor.v1_1.ISensorCallback;

/**
 * @brief 提供Sensor设备基本控制操作接口。
 *
 * 操作包括获取传感器设备信息、订阅/取消订阅传感器数据、使能/去使能传感器、设置传感器模式、设置传感器精度、量程等可选配置接口定义。
 *
 * @since 4.0
 * @version 1.1
 */
interface ISensorInterface {
    /**
     * @brief 获取当前系统中所有类型的传感器信息。
     *
     * @param info 输出系统中注册的所有传感器信息，一种类型传感器信息包括传感器名字、设备厂商、
     * 固件版本号、硬件版本号、传感器类型编号、传感器标识、最大量程、精度、功耗，详见{@link HdfSensorInformation}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    GetAllSensorInfo([out] struct HdfSensorInformation[] info);

    /**
     * @brief 根据传感器设备类型标识使能传感器信息列表里存在的设备，只有数据订阅者使能传感器后，才能获取订阅的传感器数据。
     *
     * @param sensorId 唯一标识一个传感器设备类型，详见{@link HdfSensorTypeTag}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    Enable([in] int sensorId);

    /**
     * @brief 根据传感器设备类型标识去使能传感器信息列表里存在的设备。
     *
     * @param sensorId 唯一标识一个传感器设备类型，详见{@link HdfSensorTypeTag}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    Disable([in] int sensorId);

    /**
     * @brief 设置指定传感器的数据上报模式，不同的工作模式，上报数据的方式不同。
     *
     * @param sensorId 唯一标识一个传感器设备类型，详见{@link HdfSensorTypeTag}。
     * @param samplingInterval 设置指定传感器的数据采样间隔，单位纳秒。
     * @param reportInterval 表示传感器数据上报间隔，单位纳秒。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 2.2
     * @version 1.0
     */
    SetBatch([in] int sensorId,[in] long samplingInterval, [in] long reportInterval);

    /**
     * @brief 设置指定传感器数据上报模式。
     *
     * @param sensorId 唯一标识一个传感器设备类型，详见{@link HdfSensorTypeTag}。
     * @param mode 传感器的数据上报模式，详见{@link HdfSensorModeType}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    SetMode([in] int sensorId, [in] int mode);

    /**
     * @brief 设置指定传感器量程、精度等可选配置。
     *
     * @param sensorId 唯一标识一个传感器设备类型，详见{@link HdfSensorTypeTag}。
     * @param option 表示要设置的选项，如测量范围和精度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    SetOption([in] int sensorId, [in] unsigned int option);

    /**
     * @brief 订阅者注册传感器数据回调函数，系统会将获取到的传感器数据上报给订阅者。
     *
     * @param groupId 传感器组ID。
     * groupId枚举值范围为128-160，表示已订阅医疗传感器服务，只需成功订阅一次，无需重复订阅。 
     * groupId枚举值范围不在128-160之间，这意味着传统传感器已订阅，只需成功订阅一次，无需重复订阅。
     * @param callbackObj 要注册的回调函数，详见{@link ISensorCallback}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    Register([in] int groupId, [in] ISensorCallback callbackObj);

    /**
     * @brief 订阅者取消注册传感器数据回调函数。
     *
     * @param groupId 传感器组ID。
     * groupId枚举值范围为128-160，表示已订阅医疗传感器服务。只需成功取消订阅一次，无需重复取消订阅。
     * groupId枚举值范围不在128-160之间，这意味着传统传感器已订阅。并且成功取消订阅。
     * @param callbackObj 要取消注册的回调函数，详见{@link ISensorCallback}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负数。
     *
     * @since 2.2
     * @version 1.0
     */
    Unregister([in] int groupId, [in] ISensorCallback callbackObj);

    /**
     * @brief 获取小系统中的传感器事件数据。
     *
     * @param sensorId 表示传感器ID。有关详细信息，请参阅｛@link SensorTypeTag｝。
     * @param event 表示系统中传感器事件数据的矢量。
     * 传感器事件数据包括传感器ID、传感器算法版本、数据生成时间等，数据选项（如测量范围和精度）、数据报
     * 告模式、数据地址和数据长度。有关详细信息，请参阅｛@link HdfSensorEvents｝。
     * 
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负数。
     * 
     * @since 4.0
     * @version 1.1
     */
    ReadData([in] int sensorId, [out] struct HdfSensorEvents[] event);
}
/** @} */