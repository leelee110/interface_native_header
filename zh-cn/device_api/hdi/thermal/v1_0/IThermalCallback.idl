/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Thermal
 * @{
 *
 * @brief 提供设备温度管理、控制及订阅接口。
 *
 * 热模块为热服务提供的设备温度管理、控制及订阅接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口管理、控制和订阅设备温度。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file IThermalCallback.idl
 *
 * @brief 设备发热状态的回调。
 *
 * 热模块为热服务提供的设备发热状态的回调。
 *
 * 模块包路径：ohos.hdi.thermal.v1_0
 *
 * 引用：ohos.hdi.thermal.v1_0.ThermalTypes
 *
 * @since 3.1
 * @version 1.0
 */

package ohos.hdi.thermal.v1_0;

import ohos.hdi.thermal.v1_0.ThermalTypes;

/**
 * @brief 订阅设备发热状态的回调。
 *
 * 服务创建此回调对象后，可以调用{@link IThermalInterface}的接口注册回调，从而订阅设备发热状态的变化。
 *
 * @since 3.1
 */
[callback] interface IThermalCallback {
    /**
     * @brief 设备发热状态变化的回调方法。
     *
     * 当设备发热状态发生变化时，将通过此方法的参数返回给服务。
     *
     * @param event 输入参数，设备发热信息，包括器件类型、器件温度。
     * @see HdfThermalCallbackInfo
     *
     * @since 3.1
     */
    OnThermalDataEvent([in] struct HdfThermalCallbackInfo event);
}
/** @} */
